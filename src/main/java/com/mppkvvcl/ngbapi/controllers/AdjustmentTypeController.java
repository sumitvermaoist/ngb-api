package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.AdjustmentTypeService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.AdjustmentTypeInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Vikas on 8/3/2017.
 */


@RestController
@RequestMapping(value = "/adjustment-type")
public class AdjustmentTypeController {

    private static final Logger logger = GlobalResources.getLogger(AdjustmentTypeController.class);

    @Autowired
    AdjustmentTypeService adjustmentTypeService;
    /**
     * URI : /adjustment-type<br><br>
     * This getAllAdjustmentTypes method is used for getting Adjustment Type objects with above URI.<br><br>
     * Response  : 200(Ok) when Adjustment object found successfully.<br>
     * Response  : 204(NO_CONTENT) when no Adjustment object found.<br>
     * return
     */
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getAllAdjustmentTypes(){
        String methodName = "getAllAdjustments() :";
        ResponseEntity<?> response = null;
        List<? extends AdjustmentTypeInterface> adjustmentTypes = null;
        logger.info(methodName + "Got request in controller to get All type Adjustment list");
        adjustmentTypes =  adjustmentTypeService.getAllTypeAdjustment();
        if(adjustmentTypes != null && adjustmentTypes.size() > 0){
            logger.info(methodName + "fetched Adjustment Type object");
            response = new ResponseEntity<>(adjustmentTypes, HttpStatus.OK);
        }else {
            logger.error( methodName + "unable to fetch all type adjustment list");
            response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        logger.info( methodName + "return from Adjustment type Controller");
        return response;
    }

    /**
     * URI : /adjustment-type/manual<br><br>
     * This getManualAdjustmentTypes method is used for getting Adjustment Type objects with above URI.<br><br>
     * This method provides you list of only those adjustment types which are allowed to be provided for manual adjustments by end user   <br><br>
     * Response  : 200(Ok) when Adjustment object found successfully.<br>
     * Response  : 204(NO_CONTENT) when no Adjustment object found.<br>
     * return
     */
    @RequestMapping(method = RequestMethod.GET, value="/manual", produces = "application/json")
    public ResponseEntity<?> getManualAdjustmentTypes(){
        String methodName = "getManualAdjustmentTypes() :";
        ResponseEntity<?> response = null;
        List<? extends AdjustmentTypeInterface> adjustmentTypes = null;
        logger.info(methodName + "Got request in controller to get All manual Adjustment Type list");
        adjustmentTypes =  adjustmentTypeService.getManualTypeAdjustment();
        if(adjustmentTypes != null && adjustmentTypes.size() > 0){
            logger.info(methodName + "fetched Adjustment Type object");
            response = new ResponseEntity<>(adjustmentTypes, HttpStatus.OK);
        }else {
            logger.error( methodName + "unable to fetch all type adjustment list");
            response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        logger.info( methodName + "return from Adjustment type Controller");
        return response;
    }
}
