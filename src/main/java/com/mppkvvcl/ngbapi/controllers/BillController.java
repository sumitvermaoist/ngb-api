package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.custombeans.CC4Bill;
import com.mppkvvcl.ngbapi.custombeans.CustomBill;
import com.mppkvvcl.ngbapi.custombeans.CustomReadMaster;
import com.mppkvvcl.ngbapi.exceptionhandlers.RuntimeExceptionHandler;
import com.mppkvvcl.ngbapi.services.BillService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbinterface.interfaces.BillInterface;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

/**
 * Created by ANKIT on 14-09-2017.
 */
@RestController
@RequestMapping(value = "bill")
public class BillController {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(BillController.class);

    /*
    Asking spring to inject BillService object so that we can use
    its methods for performing various operations through repo.
     */
    @Autowired
    private BillService billService;

    /**
     * URI : bill/latest/consumer-no/?<br><br>
     * This getLatestBillByConsumerNo method takes consumerNo to fetch Bill with URI. <br><br>
     * Response: 200(Ok) For fetch successful Bill<br>
     * Response: 417(Expectation_failed): For fetch null Bill<br>
     * Response: 400(Bad_Request): For received consumerNo in parameter is null.<br><br>
     * param consumerNo<br>
     * return response
     */

    @RequestMapping(method = RequestMethod.GET, value = "/latest/consumer-no/{consumerNo}", produces = "application/json")
    public ResponseEntity getLatestBillByConsumerNo(@PathVariable("consumerNo") String consumerNo) {
        String methodName = "getLatestBillByConsumerNo() : ";
        logger.info(methodName + " Got requests to fetch Latest Bill by consumer no , " + consumerNo);
        ErrorMessage errorMessage = null;
        BillInterface bill = null;
        ResponseEntity<?> response = null;
        if (consumerNo == null) {
            errorMessage = new ErrorMessage("Invalid Consumer No. Please Check.");
            logger.error(methodName + " Consumer No Null. Returning bad request");
            response = new ResponseEntity<>(errorMessage,HttpStatus.BAD_REQUEST);
            return response;
        }
        try{
            bill = billService.getLatestBillByConsumerNo(consumerNo);
        }catch(RuntimeException e){
            errorMessage = new ErrorMessage("Internal Error Occurred.");
            logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            return response;
        }catch (Exception exception){
            errorMessage = new ErrorMessage("Unable to fetch the Bill. Something Went Wrong.");
            logger.error(methodName + "Received Runtime Exception in controller with error message as :" + exception.getMessage());
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            return response;
        }
        if (bill == null) {
            errorMessage = new ErrorMessage("No Bill Available For Consumer.");
            logger.error(methodName + " No Bill Retrieved for Consumer no "+consumerNo);
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            return response;
        }
        logger.info(methodName + " Got Bill in controller.");
        response = new ResponseEntity<>(bill, HttpStatus.OK);
        logger.info(" Returning Bill from "+methodName);
        return response;
    }


    /**
     * URI : /consumer-no/?/month/?<br><br>
     * This getBillByConsumerNoAndBillMonth method takes consumerNo and bill Month to fetch Bill with URI. <br><br>
     * Response: 200(Ok) For fetch successful Bill<br>
     * Response: 417(Expectation_failed): For fetch null Bill<br>
     * Response: 400(Bad_Request): For received consumerNo in parameter is null.<br><br>
     * param consumerNo<br>
     * param billMonth<br>
     * return response
     */

    @RequestMapping(method = RequestMethod.GET, value = "/consumer-no/{consumerNo}/month/{billMonth}", produces = "application/json")
    public ResponseEntity getBillByConsumerNoAndBillMonth(@PathVariable("consumerNo") String consumerNo,@PathVariable("billMonth") String billMonth) {
        String methodName = "getBillByConsumerNoAndBillMonth() : ";
        logger.info(methodName + " Got requests to fetch Bill by consumer no " + consumerNo +" for bill month "+billMonth);
        ErrorMessage errorMessage = null;
        BillInterface bill = null;
        ResponseEntity<?> response = null;
        if (consumerNo == null || billMonth == null) {
            errorMessage = new ErrorMessage("Invalid Consumer No. or month Please Check.");
            logger.error(methodName + " Consumer No or Bill Month is Null. Returning bad request");
            response = new ResponseEntity<>(errorMessage,HttpStatus.BAD_REQUEST);
            return response;
        }
        try{
            bill = billService.getBillByConsumerNoAndBillMonth(consumerNo,billMonth);
        }catch(RuntimeException e){
            errorMessage = new ErrorMessage("Unable to fetch the Bill. Something Went Wrong.");
            logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            return response;
        }catch (Exception exception){
            errorMessage = new ErrorMessage("Unable to fetch the Bill. Something Went Wrong.");
            logger.error(methodName + "Received Runtime Exception in controller with error message as :" + exception.getMessage());
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            return response;
        }
        if (bill == null) {
            errorMessage = new ErrorMessage("No Bill Found.");
            logger.error(methodName + " No Bill Retrieved for bill month "+billMonth+" And Consumer no "+consumerNo);
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            return response;
        }
        logger.info(methodName + " Got Bill in controller.");
        response = new ResponseEntity<>(bill, HttpStatus.OK);
        logger.info(" Returning Bill from "+methodName);
        return response;
    }

    /**
     * URI : bill/bill-months/consumer-no/?<br><br>
     * This getAllBillMonthsByConsumerNo method takes consumerNo to fetch All Bill Months with URI. <br><br>
     * Response: 200(Ok) For fetch successful Bill Months<br>
     * Response: 417(Expectation_failed): For fetch null Bill Months<br>
     * Response: 400(Bad_Request): For received consumerNo in parameter is null.<br><br>
     * param consumerNo<br>
     * return response
     */

    @RequestMapping(method = RequestMethod.GET, value = "/bill-month/consumer-no/{consumerNo}", produces = "application/json")
    public ResponseEntity getAllBillMonthsByConsumerNo(@PathVariable("consumerNo") String consumerNo) {
        String methodName = "getAllBillMonthsByConsumerNo() : ";
        logger.info(methodName + " Got requests to fetch All Bill Months by consumer no , " + consumerNo);
        ErrorMessage errorMessage = null;
        List<String> billMonths = null;
        ResponseEntity<?> response = null;
        if (consumerNo == null) {
            errorMessage = new ErrorMessage("Invalid Consumer No. Please Check.");
            logger.error(methodName + " Consumer No Null. Returning bad request");
            response = new ResponseEntity<>(errorMessage,HttpStatus.BAD_REQUEST);
            return response;
        }
        try{
            billMonths = billService.getAllBillMonthByConsumerNo(consumerNo);
        }catch(RuntimeException e){
            errorMessage = new ErrorMessage("Internal Error Occurred.");
            logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            return response;
        }catch (Exception exception){
            errorMessage = new ErrorMessage("Unable to fetch the Bill. Something Went Wrong.");
            logger.error(methodName + "Exception in controller with error message as :" + exception.getMessage());
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            return response;
        }
        if (billMonths == null || billMonths.size() == 0) {
            errorMessage = new ErrorMessage("No Bill Available For Consumer.");
            logger.error(methodName + " No Bill Months Retrieved for Consumer no "+consumerNo);
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            return response;
        }
        logger.info(methodName + " Got Bill in controller.");
        response = new ResponseEntity<List<String>>(billMonths, HttpStatus.OK);
        logger.info(" Returning All Bill Months from "+methodName);
        return response;
    }


    /**
     * Added By Preetesh Dated: 31 July 2017
     * @param consumerNo
     * @param paymentDate
     * @param instrumentFlag
     * @param partPaymentFlag
     * @return
     */
    @RequestMapping(value = "custom/consumer-no/{consumerNo}",
            method = RequestMethod.GET , produces = "application/json")
    public ResponseEntity<?> getCustomBill(@PathVariable("consumerNo") String consumerNo,
                                           @RequestParam("paymentDate") String paymentDate,
                                           @RequestParam("agricultureFlag") boolean agricultureFlag,
                                           @RequestParam("instrumentFlag") boolean instrumentFlag,
                                           @RequestParam("partPaymentFlag") boolean partPaymentFlag) {
        String methodName = "getCustomBillAmount() : ";
        CustomBill customBill = null;
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        logger.info(methodName + "Got request to get latest bill for consumer "
                +consumerNo+" date: "+paymentDate+" instrumentFlag: "+instrumentFlag+" part-payment: "+partPaymentFlag+" AgricultureFlag "+agricultureFlag);
        if (consumerNo != null && paymentDate != null ) {
            try{
                customBill = billService.getCustomBillByInstrumentFlagAndAgricultureFlagAndPartPaymentFlagAndPaymentDateAndConsumerNo(agricultureFlag,instrumentFlag, partPaymentFlag, paymentDate, consumerNo);
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                customBill = null;
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                customBill = null;
            }

            if (customBill != null) {
                logger.info(methodName + "found  bill for consumer " + consumerNo);
                response = new ResponseEntity<>(customBill, HttpStatus.OK);
            } else {
                logger.error(methodName + "some error in creating custom bean");
                response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName + "some input is null");
            errorMessage = new ErrorMessage("some input is null");
            response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);

        }
        return response;
    }

    /**
     * code by nitish
     * Below method returns count of readings for consumerNo
     * @param consumerNo
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/count/consumer-no/{consumerNo}", produces = "application/json")
    public ResponseEntity getCountByConsumerNo(@PathVariable("consumerNo") String consumerNo) {
        String methodName = "getCountByConsumerNo() : ";
        logger.info(methodName + "called " + consumerNo);
        ResponseEntity<?> response = null;
        if (consumerNo != null) {
            long count = billService.getCountByConsumerNo(consumerNo);
            if (count >= 0) {
                response = new ResponseEntity<>(count, HttpStatus.OK);
            } else {
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/consumer-no/{consumerNo}/paged",  produces = "application/json" )
    public ResponseEntity<?> getByConsumerNoWithPagination(@PathVariable("consumerNo") String consumerNo,
                                                           @PathParam("sortBy") String sortBy,@PathParam("sortOrder") String sortOrder,
                                                           @PathParam("pageNumber") int pageNumber, @PathParam("pageSize") int pageSize){
        String methodName = "getByConsumerNoWithPagination() : ";
        logger.info(methodName + "called " + consumerNo + " pageNumber " + pageNumber + " pageSize " + pageSize + " sort " + sortOrder + " sortBy " + sortBy);
        ResponseEntity<?> response = null;
        if(consumerNo != null && sortBy != null){
            List<BillInterface> billInterfaces = billService.getByConsumerNoWithPagination(consumerNo,sortBy,sortOrder,pageNumber,pageSize);
            if(billInterfaces != null){
                response = new ResponseEntity<>(billInterfaces, HttpStatus.OK);
            }else{
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName + "Input param consumerNo :" + consumerNo + "found null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * code by nitish
     * Below method returns result in paginated way based on passed
     * pageSize and pagedNumber
     * @param consumerNo
     * @param pageNumber
     * @param pageSize
     * @param sortOrder
     * @return
     *//*
    @RequestMapping(method = RequestMethod.GET, value = "/consumer-no/{consumerNo}/paged", produces = "application/json")
    public ResponseEntity getByConsumerNoOrderByBillMonthAndId(@PathVariable("consumerNo") String consumerNo,
                                                                       @PathParam("pageNumber") int pageNumber, @PathParam("pageSize") int pageSize,
                                                                       @PathParam("sortOrder") String sortOrder) {
        String methodName = "getByConsumerNoOrderByBillMonthAndId() : ";
        logger.info(methodName + "called " + consumerNo + " pageNumber " + pageNumber + " pageSize " + pageSize + " sort " + sortOrder);
        ResponseEntity<?> response = null;
        if (consumerNo != null) {
            List<BillInterface> billInterfaces = billService.getByConsumerNoOrderByBillMonthWithPagination(consumerNo,pageNumber,pageSize,sortOrder);
            if (billInterfaces != null) {
                response = new ResponseEntity<>(billInterfaces, HttpStatus.OK);
            } else {
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }*/

    /**
     * URI : bill/consumer-no/?<br><br>
     * This getBillsByConsumerNo method takes consumerNo to fetch Bill with URI. <br><br>
     * Response: 200(Ok) For fetch successful Bill<br>
     * Response: 417(Expectation_failed): For fetch null Bill<br>
     * Response: 400(Bad_Request): For received consumerNo in parameter is null.<br><br>
     * param consumerNo<br>
     * return response
     */

    @RequestMapping(method = RequestMethod.GET, value = "/consumer-no/{consumerNo}", produces = "application/json")
    public ResponseEntity getBillsByConsumerNo(@PathVariable("consumerNo") String consumerNo) {
        String methodName = "getBillsByConsumerNo() : ";
        logger.info(methodName + " called by consumer no , " + consumerNo);
        ErrorMessage errorMessage = null;
        List<BillInterface> bills = null;
        ResponseEntity<?> response = null;
        if (consumerNo != null) {
            try{
                bills = billService.getBillsByConsumerNo(consumerNo);
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage("Internal Error Occurred.");
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }catch (Exception exception){
                errorMessage = new ErrorMessage("Unable to fetch the Bills. Something Went Wrong.");
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + exception.getMessage());
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
                return response;
            }
            if (bills != null && bills.size() > 0) {
                logger.info(methodName + " Got Bills in controller.");
                response = new ResponseEntity<>(bills, HttpStatus.OK);
                logger.info(" Returning Bill from "+methodName);
            }else{
                errorMessage = new ErrorMessage("No Bill Available For Consumer.");
                logger.error(methodName + " No Bill Retrieved for Consumer no "+consumerNo);
                response = new ResponseEntity<>(errorMessage,HttpStatus.NO_CONTENT);
                }

        }else{
            errorMessage = new ErrorMessage("Invalid Consumer No. Please Check.");
            logger.error(methodName + " Consumer No Null. Returning bad request");
            response = new ResponseEntity<>(errorMessage,HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(method = RequestMethod.POST,value = "/consumer-no/{consumerNo}/re-bill", consumes = "application/json", produces = "application/json")
    public ResponseEntity reBill(@PathVariable("consumerNo") String consumerNo,@RequestBody CustomReadMaster customReadMasterToInsert) {
        String methodName = "reBill() : ";
        logger.info(methodName + "called");
        ResponseEntity<?> response = null;
        ReadMasterInterface  readingToUpdate = null;
        CC4Bill updatedBill = null;
        ErrorMessage errorMessage = new ErrorMessage();
        if (consumerNo != null && customReadMasterToInsert != null) {
            readingToUpdate = GlobalResources.convertCustomReadMasterToReadMaster(customReadMasterToInsert);
            updatedBill = billService.reBill(consumerNo ,readingToUpdate,errorMessage);
        }else{
            errorMessage.setErrorMessage("input param is null");
        }
        if (updatedBill != null && updatedBill.getBillInterface() != null) {
            logger.info(methodName + " Re-bill successfully done for consumer no :"+updatedBill.getBillInterface().getConsumerNo());
            response = new ResponseEntity<>(updatedBill, HttpStatus.CREATED);
        } else {
            logger.error(methodName + " some error in insertion. "+errorMessage);
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }


    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity insertCC4Bill(@RequestBody CC4Bill cc4Bill) {
        String methodName = "insertCC4Bill() : ";
        logger.info(methodName + "called");
        ResponseEntity<?> response = null;
        CC4Bill updatedBill = null;
        ErrorMessage errorMessage = null;
        if (cc4Bill != null) {
            try{
                updatedBill = billService.insertCC4Bill(cc4Bill);
                if (updatedBill != null) {
                    logger.info(methodName + "bill updated successfully for consumer no :"+updatedBill.getBillInterface().getConsumerNo());
                    response = new ResponseEntity<>(updatedBill, HttpStatus.CREATED);
                } else {
                    logger.error(methodName + " some error in insertion. "+errorMessage);
                    response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
                }
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }
        return response;
    }
}

