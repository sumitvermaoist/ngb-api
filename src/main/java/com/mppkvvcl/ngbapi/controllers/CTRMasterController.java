package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.CTRMasterService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.CTRMasterInterface;
import com.mppkvvcl.ngbentity.beans.CTRMaster;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by ANSHIKA on 04-07-2017.
 */
@RestController
@RequestMapping(value = "/ctr")
public class CTRMasterController {
    /**
     * Requesting spring to get singleton ctrMasterService object.
     */
    @Autowired
    CTRMasterService ctrMasterService;

    /**
     * Getting application logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(CTRMasterController.class);

    /**
     * URI - /ctr<br><br>
     * This insert method takes ctrMaster object and insert it into CTRMaster table in backend database when user hit the URI.<br><br>
     * Returns 201 for CREATED status and insertedCTRMaster if successful.<br>
     * Returns 417 for EXPECTATION_FAILED status when insertion fails.<br>
     * Returns 400 for BAD_REQUEST  when input ctrMaster is null.<br><br>
     * param ctrMaster<br>
     * return insertedCTRMaster
     */
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> insert(@RequestBody CTRMaster ctrMaster) {
        String methodName = "insert() : ";
        ResponseEntity<?> response = null;
        CTRMasterInterface insertedCTRMaster = null;
        logger.info(methodName + "Got request to insert ctrMaster");
        if (ctrMaster != null) {
            logger.info(methodName + "Calling CTRService method to insert ctrMaster : " + ctrMaster);
            insertedCTRMaster = ctrMasterService.insert(ctrMaster);
            if (insertedCTRMaster != null) {
                logger.info(methodName + "ctrMaster inserted succesfully");
                response = new ResponseEntity<>(insertedCTRMaster, HttpStatus.CREATED);
            } else {
                logger.error(methodName + "Unable to insert ctrMaster");
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            logger.error(methodName + "Input param ctrMaster found null : " + ctrMaster);
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * URI - /ctr/make/{make}/serial-no/{serialNo}<br><br>
     * This method takes make and serialNo and fetch a CTR object against it when user hit the URI.<br><br>
     * Returns 200 for OK status and CTRMaster object if successful.<br>
     * Returns 204 for NO_CONTENT status when no content in CTR object  .<br>
     * Returns 400 for BAD_REQUEST  when input param is null.<br><br>
     * param make<br>
     * param serialNo<br>
     * return response
     */

    @RequestMapping(method = RequestMethod.GET, value = "/make/{make}/serial-no/{serialNo}", produces = "application/json")
    public ResponseEntity getCTRByMakeAndSerialNo(@PathVariable("make") String make, @PathVariable("serialNo") String serialNo) {
        String methodName = "getCTRByMakeAndSerialNo() : ";
        CTRMasterInterface ctrMaster = null;
        ResponseEntity<?> response = null;
        logger.info(methodName + "Got request to get CTR Details by make and Serial No :  " + make + " and serial no: " + serialNo);
        if (serialNo != null && make != null) {
            ctrMaster = ctrMasterService.getByMakeAndSerialNo(make, serialNo);
            if (ctrMaster != null) {
                logger.info(methodName + "Sucessfully fetched CTRMaster details");
                response = new ResponseEntity<>(ctrMaster, HttpStatus.OK);
            } else {
                logger.info(methodName + "Got zero records for CTRMaster");
                response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } else {
            logger.error(methodName + "Input paremeter passed Make or Serial no is null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return  response;

    }
}