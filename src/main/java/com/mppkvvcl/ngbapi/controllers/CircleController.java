package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.CircleService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.CircleInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by MITHLESH on 18-07-2017.
 */
@RestController
@RequestMapping(value = "circle")
public class CircleController {
    /**
     * Asking Spring to get Singleton Object of CircleService
     */
    @Autowired
    CircleService circleService;
    /**
     * Getting logger for logging in current class
     */
    Logger logger = GlobalResources.getLogger(CircleController.class);

    /**
     * URI - /circle<br><br>
     * This getAll() method is used to get circles.<br><br>
     * Response : 200(OK) : to successfully fetch circles.<br>
     * Response : 417(EXPECTATION_FAILED) : circles not received successfully.<br><br>
     * return response.
     */
    @RequestMapping(method = RequestMethod.GET , produces = "application/json")
    public ResponseEntity<?> getAll(){
        String methodName = "getAll() : ";
        ResponseEntity<?> response = null;
        logger.info(methodName + "Got request to fetch All Circle objects");
        List<? extends CircleInterface> circles = circleService.getAll();
        if(circles != null){
            if(circles.size() > 0){
                logger.info(methodName + "Successfully got List<Circle> with size: "+circles.size());
                response = new ResponseEntity<Object>(circles , HttpStatus.FOUND);
            }else{
                logger.error(methodName + "Successfully got List<Circle> with size: "+circles.size());
                response = new ResponseEntity<Object>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName + "Fetched List<Circle> is null");
            response = new ResponseEntity<Object>(HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }
}
