package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.custombeans.CustomMeterReplacement;
import com.mppkvvcl.ngbapi.custombeans.PostMeterReplacement;
import com.mppkvvcl.ngbapi.exceptionhandlers.RuntimeExceptionHandler;
import com.mppkvvcl.ngbapi.services.ConsumerMeterMappingService;
import com.mppkvvcl.ngbapi.services.MeterMasterService;
import com.mppkvvcl.ngbapi.services.NSCStagingStatusService;
import com.mppkvvcl.ngbapi.services.ReadReplacementService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.MeterMasterInterface;
import com.mppkvvcl.ngbinterface.interfaces.NSCStagingStatusInterface;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerMeterMappingInterface;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterInterface;
import com.mppkvvcl.ngbentity.beans.ConsumerMeterMapping;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by PREETESH on 6/22/2017.
 */

@RestController
@RequestMapping(value = "/consumer/meter/mapping")
public class ConsumerMeterMappingController {
    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ConsumerMeterMappingController.class);
    /**
     * Asking spring to inject MeterMasterService in the below variable
     * so that various methods of it can be used to interact and perform business
     * logics on meter_master table data at the backend database.
     */
    @Autowired
    private MeterMasterService meterMasterService;
    /**
     * Asking spring to inject ConsumerMeterMappingService in the below variable
     * so that various methods of it can be used to interact and perform business
     * logics on consumer_meter_mapping table data at the backend database.
     */
    @Autowired
    private ConsumerMeterMappingService consumerMeterMappingService;
    /**
     * Asking spring to inject NSCStagingStatusService in the below variable
     * so that various methods of it can be used to interact and perform business
     * logics on nsc_staging_status table data at the backend database.
     */
    @Autowired
    private ReadReplacementService readReplacementService;

    @Autowired
    private NSCStagingStatusService nscStagingStatusService;

    /**
     * URI consumer/meter/identifier/{identifier}<br><br>
     * This method getByIdentifier  at controller with URI
     * provides consumerMeterMappings details with passed identifier parameter.<br><br>
     * Response 200 for OK status and list of consumerMeterMappings is sent if successful.<br>
     * Response 204 NO_CONTENT status is sent if no content in list.<br>
     * Response 400 for BAD_REQUEST status is sent if input param are null.<br><br>
     * param identifier<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET, value = "/identifier/{identifier}", produces = "application/json")
    public ResponseEntity getByIdentifier(@PathVariable("identifier") String identifier) {
        String methodName = " getByIdentifier() :  ";
        List<ConsumerMeterMappingInterface> consumerMeterMappings = null;
        ResponseEntity<?> response = null;
        logger.info(methodName + "Got request to get MeterMapping Details by meterIdentifier :  " + identifier );
        if (identifier != null) {
            consumerMeterMappings = consumerMeterMappingService.getByIdentifier(identifier.trim());
            if (consumerMeterMappings.size() > 0) {
                logger.info(methodName + "Got rows " + consumerMeterMappings);
                response = new ResponseEntity<List<ConsumerMeterMappingInterface>> (consumerMeterMappings, HttpStatus.OK);
            } else {
                logger.error(methodName + "Got zero rows");
                response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } else {
            logger.error(methodName + "Input paremeter passed does not meet requirement of being NOT NULL, hence returning BAD REQUEST as response");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return response;
    }

    /**
     * URI- /consumer/meter/mapping/identifier/{identifier}/status/{status}<br><br>
     * This api check for meter which are mapped to any consumer or not.It also checks for mapping of meter
     * on new pending nsc applications<br><br>
     * Response 302 FOUND status is sent if successful.<br>
     * Response 204 NO_CONTENT status is sent if no content in list.<br>
     * Response 400 for BAD_REQUEST status is sent if input param identifier are null.<br>
     * Response 400 for BAD_REQUEST status is sent if input param meterMaster are null.<br><br>
     * param identifier meterIdentifier to check for
     * param status status of meter on consumer whether ACTIVE or INACTIVE<br>
     * return Response
     */
    @RequestMapping(method = RequestMethod.GET, value = "/identifier/{identifier}/status/{status}", produces = "application/json")
    public ResponseEntity getByIdentifierAndStatus(@PathVariable("identifier") String identifier, @PathVariable("status") String status) {
        String methodName = " getByIdentifierAndStatus() :  ";
        MeterMasterInterface meterMaster = null;
        ConsumerMeterMapping consumerMeterMapping = null;
        List<ConsumerMeterMappingInterface> consumerMeterMappings = null;
        ResponseEntity<?> response = null;
        logger.info(methodName + "Got request to get MeterMapping  Details by meterIdentifier and Status :  " + identifier );
        if (identifier != null) {
            meterMaster = meterMasterService.getByIdentifier(identifier.trim());
            if (meterMaster != null) {
                consumerMeterMappings = consumerMeterMappingService.getByIdentifierAndMappingStatus(identifier, status);
                if (consumerMeterMappings.size() > 0) {
                    logger.info(methodName + "Got rows " + consumerMeterMappings);
                    response = new ResponseEntity <>(consumerMeterMappings, HttpStatus.OK);
                } else {
                    logger.info(methodName + "Got zero rows proceeding");
                    if(status.trim().equals("ACTIVE")){
                        logger.info(methodName + "Got zero rows proceeding with nsc_staging check");
                        logger.info(methodName + " status is " + status + " checking nscstagingstatus with status PENDING");
                        NSCStagingStatusInterface nscStagingStatus = nscStagingStatusService.getByPendingStatusAndMeterIdentifier("PENDING",identifier);
                        if(nscStagingStatus != null){
                            logger.info(methodName + identifier +" identifier not found to be mapped to any consumer but found to be in pending nsc");
                            response = new ResponseEntity<>(nscStagingStatus,HttpStatus.OK);
                            return response;
                        }
                    }
                    response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
                }
            }
            else{
                logger.error(methodName + "Meter Not Found, hence returning BAD REQUEST as response");
                response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } else {
            logger.error(methodName + "Input paremeter passed does not meet requirement of being NOT NULL, hence returning BAD REQUEST as response");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * Added By: Preetesh,  Dated : 10 July 2017<br><br>
     * URI - /consumer/meter/mapping/consumer-no/{consumerNo}/replacement<br><br>
     * This URI method is dedicated to meter-replacement form , whenever end user open meter replacement form,
     * he will required meter details, last reading of meter, So this method will return CustomMeterReplacement
     * bean that contains meter replacement related data.<br><br>
     * Returns 200 for OK status and customMeterReplacement object if successful.<br>
     * Returns 204 for NO_CONTENT status when no content in customMeterReplacement object  .<br>
     * Returns 400 for BAD_REQUEST  when input param is null.<br><br>
     * param consumerNo<br>
     * return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/consumer-no/{consumerNo}/replacement", produces = "application/json")
    public ResponseEntity getCustomMeterReplacementForConsumerNo(@PathVariable("consumerNo") String consumerNo) throws Exception {
        String methodName = "getCustomMeterReplacementForConsumerNo() : ";
        CustomMeterReplacement customMeterReplacement = null;
        logger.info(methodName + " Got requests to fetch meter and reading data for consumer no , " + consumerNo);
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        if (consumerNo != null) {
            consumerNo = consumerNo.trim();
            try{
                customMeterReplacement = readReplacementService.getCustomMeterReplacementByConsumerNo(consumerNo);
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                customMeterReplacement = null;
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                customMeterReplacement = null;
            }
            if(customMeterReplacement != null){
                logger.info(methodName + "CustomMeterReplacement set successfully "+ customMeterReplacement);
                response =  new ResponseEntity<CustomMeterReplacement>(customMeterReplacement, HttpStatus.OK);
            }else{
                logger.error(methodName + "some error in fetching CustomMeterReplacement data ");
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            logger.error(methodName + " inputs parameters is null.Returning bad request");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        logger.info(methodName + " Returning from fetch Read Master");
        return response;
    }

    /**
     * Added By: Vikas Patel date: 11 July 2017<br><br>
     * URI - /consumer/meter/mapping/consumer-no/{consumerNo}<br><br>
     * Response 200 for OK and insertedReadMaster is sent if successful.<br>
     * Response 417 for EXPECTATION_FAILED is sent if insertion fails.<br>
     * Response 400 for BAD_REQUEST is sent if input param are null.<br><br>
     * param consumerNo<br>
     * param postMeterReplacement<br>
     * return
     */
    @RequestMapping(method = RequestMethod.POST, value = "/consumer-no/{consumerNo}",consumes = "application/json",produces = "application/json")
    public ResponseEntity insertMeterReplacement(@PathVariable("consumerNo") String consumerNo, @RequestBody PostMeterReplacement postMeterReplacement) throws Exception {
        String methodName = "insertMeterReplacement() :";
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        ReadMasterInterface insertedReadMaster = null;
        logger.info(methodName + "Got request to replace meter or CTR");
        if (consumerNo != null && postMeterReplacement != null) {
            consumerNo = consumerNo.trim();
            try {
                insertedReadMaster = consumerMeterMappingService.insertMeterReplacement(consumerNo, postMeterReplacement);
            } catch (RuntimeException e) {
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as" + errorMessage);
                insertedReadMaster = null;
            } catch (Exception ee) {
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in Controller with error message as : " + errorMessage);
                insertedReadMaster = null;
            }
            if (insertedReadMaster != null) {
                response = new ResponseEntity<>(insertedReadMaster, HttpStatus.OK);
            } else {
                logger.error(methodName + "Unable to replace Meter OR CTR");
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            logger.error(methodName + "inputs parameters is null.Returning bad request");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/consumer-no/{consumerNo}/status/{status}",produces = "application/json")
    public ResponseEntity getByConsumerNoAndMappingStatus(@PathVariable ("consumerNo") String consumerNo, @PathVariable ("status") String status ) throws Exception{
        String methodName = "getByConsumerNoAndMappingStatus() : ";
        List<ConsumerMeterMappingInterface> consumerMeterMappings = null;
        ResponseEntity<?> response = null;
        logger.info(methodName + "Got request to get Consumer Meter Mapping by Consumer No :"+ consumerNo+"And Mapping Status"+status);
        if(consumerNo != null){
            consumerMeterMappings = consumerMeterMappingService.getByConsumerNoAndMappingStatus(consumerNo,status);
            if(consumerMeterMappings != null && consumerMeterMappings.size() > 0){
                logger.info(methodName + "Fetched Consumer Meter Mapping :" + consumerMeterMappings);
                response = new ResponseEntity<>(consumerMeterMappings, HttpStatus.OK);
            }else{
                logger.error(methodName + "unable to fetch Consumer Meter Mapping by given consumerNo :" + consumerNo);
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName + "given input parameter is null ");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(method = RequestMethod.GET, value = "mf/consumer-no/{consumerNo}", produces = "application/json")
    public ResponseEntity getMFByConsumerNo(@PathVariable("consumerNo") String consumerNo) {
        String methodName = "getMFByConsumerNo() : ";
        logger.info(methodName + "called");
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        BigDecimal mf = null;
        if (consumerNo != null) {
            try {
                mf  = consumerMeterMappingService.getMFByConsumerNoForActiveMeter(consumerNo);
            } catch (RuntimeException e) {
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as" + errorMessage);
                mf = null;
            } catch (Exception ee) {
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in Controller with error message as : " + errorMessage);
                mf = null;
            }
            if (mf != null) {
                response = new ResponseEntity<>(mf, HttpStatus.OK);
            } else {
                logger.error(methodName + "Unable to get MF");
                response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            logger.error(methodName + " inputs parameters is null.Returning bad request");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }
}