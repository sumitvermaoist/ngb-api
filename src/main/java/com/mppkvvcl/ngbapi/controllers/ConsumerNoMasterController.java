package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.custombeans.GroupConsumerData;
import com.mppkvvcl.ngbapi.custombeans.Read;
import com.mppkvvcl.ngbapi.custombeans.ReadingDiaryConsumerData;
import com.mppkvvcl.ngbapi.custombeans.ZoneConsumerData;
import com.mppkvvcl.ngbapi.exceptionhandlers.RuntimeExceptionHandler;
import com.mppkvvcl.ngbapi.services.ConsumerNoMasterService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerNoMasterInterface;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbentity.beans.ReadMaster;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by SUMIT on 20-06-2017.
 */
@RestController
@RequestMapping(value = "consumer/master")
public class ConsumerNoMasterController {
    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ConsumerNoMasterController.class);

    /**
     * Asking spring to inject NSCStagingService in the below variable
     * so that various methods of it can be used to interact and perform business
     * logics on nsc_staging table data at the backend database.
     */
    @Autowired
    private ConsumerNoMasterService consumerNoMasterService;

    /**
     * URI consumer-no-master/old-service-no-one/{oldServiceNoOne}<br><br>
     * This method getByOldServiceNoOne  at controller with URI provides consumerNoMaster details with passed identifier parameter.<br><br>
     * Response 202 for CREATED status is sent if successful.<br>
     * Response 417 for EXPECTATION_FAILED status is sent if list is empty.<br>
     * Response 400 for BAD_REQUEST status is sent if input param are null.<br><br>
     * param oldServiceNoOne<br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET,value = "/old-service-no-one/{oldServiceNoOne}",produces = "application/json")
    public ResponseEntity getByOldServiceNoOne(@PathVariable("oldServiceNoOne") String oldServiceNoOne){
        String methodName=" getByOldServiceNoOne  : ";
        logger.info(methodName +": Got old service no one as "+oldServiceNoOne+"  for getting data of ConsumerNoMaster .");
        ConsumerNoMasterInterface consumerNoMaster = null;
        ResponseEntity<?> response = null;
        if(oldServiceNoOne != null){
            logger.info(methodName+": getting data for ConsumerNoMaster ");
            consumerNoMaster = consumerNoMasterService.getByOldServiceNoOne(oldServiceNoOne);
            if(consumerNoMaster != null){
                logger.info(methodName+": Got Consumer No Master  : "+ consumerNoMaster);
                response = new ResponseEntity<>(consumerNoMaster, HttpStatus.CREATED);
            }else{
                logger.error(methodName+": ConsumerNo error . ConsumerNoService Data may be NULL ");
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName+": oldServiceNoOne is NULL");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * URI - /pdc/consumer-no/{consumerNo}<br>
     * This method takes consumerNo and fetch a list of payments.<br>
     * Response 200 for OK status and custom read bean is sent if successful.<br>
     * Response 400 for BAD_REQUEST status is sent if input param are null<br>
     * Response 417 for EXPECTATION_FAILED status is sent if PDC not allowed on consumer<br>
     * param consumerNo<br>
     * return
     */
    @RequestMapping(method = RequestMethod.GET,value = "/consumer-no/{consumerNo}", produces = "application/json" )
    public ResponseEntity<?> getByConsumerNo(@PathVariable("consumerNo") String consumerNo){
        String methodName = "getByConsumerNo() : ";
        ResponseEntity<?> response = null;
        ConsumerNoMasterInterface consumerMaster = null;
        ErrorMessage errorMessage = null;
        logger.info(methodName + "Got request to get Consumer Data for consumer : " + consumerNo);
        if(consumerNo != null){
            logger.info(methodName + "Calling consumerNoMasterService.");
            consumerMaster = consumerNoMasterService.getByConsumerNo(consumerNo);
            if (consumerMaster != null) {
                logger.info(methodName + "found ConsumerMaster "+consumerMaster);
                response = new ResponseEntity<>(consumerMaster, HttpStatus.OK);
            } else {
                errorMessage = new ErrorMessage("Consumer Number Entered is Not Valid.");
                logger.error(methodName + "some error Fetching Consumer Master");
                response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            errorMessage = new ErrorMessage("Please Enter Valid Consumer No.");
            logger.error(methodName + "Consumer No got as null.");
            response = new ResponseEntity<>(errorMessage,HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * URI - /pdc/consumer-no/{consumerNo}<br>
     * This method takes consumerNo and fetch a list of payments.<br>
     * Response 200 for OK status and custom read bean is sent if successful.<br>
     * Response 400 for BAD_REQUEST status is sent if input param are null<br>
     * Response 417 for EXPECTATION_FAILED status is sent if PDC not allowed on consumer<br>
     * param consumerNo<br>
     * return
     */
    @RequestMapping(method = RequestMethod.GET,value = "/pdc/consumer-no/{consumerNo}", produces = "application/json" )
    public ResponseEntity<?> getInfoForPDC(@PathVariable("consumerNo") String consumerNo){
        String methodName = "getInfoForPDC() : ";
        ResponseEntity<?> response = null;
        Read read = null;
        logger.info(methodName + "Got request to get data for PDC consumer : " + consumerNo);
        if(consumerNo != null){
            logger.info(methodName + "Calling consumerNoMasterService to fetch validity and read data");
            ErrorMessage errorMessage = null;
            try{
                read = consumerNoMasterService.getPDCDataForConsumerNo(consumerNo);
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
            }

            if (read != null) {
                logger.info(methodName + "found read "+read);
                response = new ResponseEntity<>(read, HttpStatus.OK);
            } else {
                logger.error(methodName + "some error in creating PDC data");
                response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName + "Input param consumerNo :" + consumerNo + "found null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }


    /**
     * URI - /pdc/consumer-no/{consumerNo}<br>
     * This method takes consumerNo and final read , calls service method to process PDC process<br>
     * Response 200 for OK status and custom read bean is sent if successful.<br>
     * Response 400 for BAD_REQUEST status is sent if input param are null<br>
     * Response 417 for EXPECTATION_FAILED status is sent if PDC not allowed on consumer<br>
     * param consumerNo<br>
     * return
     */
    @RequestMapping(method = RequestMethod.PUT,value = "/pdc/consumer-no/{consumerNo}", produces = "application/json",consumes = "application/json" )
    public ResponseEntity<?> pdcConsumerAndInsertFinalRead(@PathVariable("consumerNo") String consumerNo,
                                                           @RequestParam("pdcDate") String pdcDateString,
                                                           @RequestParam("pdcRemark") String pdcRemark,
                                                           @RequestBody ReadMaster finalRead){
        String methodName = "pdcConsumerAndInsertFinalRead() : ";
        ResponseEntity<?> response = null;
        ConsumerNoMasterInterface consumerNoMaster = null;
        logger.info(methodName + "Got request to PDC consumer and saving final read : " + consumerNo+
                " pdcdate "+pdcDateString+" pdcremark "+pdcRemark);

        if(consumerNo != null && pdcDateString != null && pdcRemark != null){
            logger.info(methodName + "Calling consumerNoMasterService to check agreement completion and read data");
            ErrorMessage errorMessage = null;
            try{
                consumerNoMaster = consumerNoMasterService.pdcConsumerAndInsertFinalRead(consumerNo,finalRead,pdcDateString,pdcRemark);
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
            }

            if ( consumerNoMaster!= null) {
                logger.info(methodName + "found read "+consumerNoMaster);
                response = new ResponseEntity<>(consumerNoMaster, HttpStatus.OK);
            } else {
                logger.error(methodName + "some error in creating PDC data");
                response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName + "Input param consumerNo :" + consumerNo + "found null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * coded by Preetesh
     * @param locationCode
     * @return
     */
    @RequestMapping(method = RequestMethod.GET,value = "count/location-code/{locationCode}",produces = "application/json")
    public ResponseEntity<?> getZoneConsumerDataByLocationCode(@PathVariable("locationCode") String locationCode){
        final String methodName = "getZoneConsumerDataByLocationCode() : ";
        logger.info(methodName + "called ");
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = new ErrorMessage();
        ZoneConsumerData zoneConsumerData = null;
        if(locationCode != null){
            zoneConsumerData = consumerNoMasterService.getZoneConsumerDataByLocationCode(locationCode,errorMessage);
        }else{
            errorMessage.setErrorMessage("location code param is null");
        }

        if(zoneConsumerData != null){
            response = new ResponseEntity<>(zoneConsumerData,HttpStatus.OK);
        }else{
            logger.error(methodName + "error occurred: " + errorMessage);
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }

    /**
     * Added By Preetesh
     * @param groupNo
     * @return
     */
    @RequestMapping(method = RequestMethod.GET,value = "count/group-no/{groupNo}",produces = "application/json")
    public ResponseEntity<?> getGroupConsumerDataByGroupNo(@PathVariable("groupNo") String groupNo){
        final String methodName = "getGroupConsumerDataByGroupNo() : ";
        logger.info(methodName + "called ");
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = new ErrorMessage();
        GroupConsumerData groupConsumerData = null;
        if(groupNo != null){
            groupConsumerData = consumerNoMasterService.getGroupConsumerDataByGroupNo(groupNo,errorMessage);
        }else{
            errorMessage.setErrorMessage("group no parameter is null");
        }
        if(groupConsumerData != null){
            response = new ResponseEntity<>(groupConsumerData,HttpStatus.OK);
        }else{
            logger.error(methodName + "error occurred: " + errorMessage);
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }

    /**
     * Added By Preetesh
     * @param groupNo
     * @return
     */
    @RequestMapping(method = RequestMethod.GET,value = "count/group-no/{groupNo}/reading-diary-no/{readingDiaryNo}",produces = "application/json")
    public ResponseEntity<?> getReadingDiaryNoDataByGroupNoAndReadingDiaryNo(@PathVariable("groupNo") String groupNo,
                                                                             @PathVariable("readingDiaryNo") String readingDiaryNo){
        final String methodName = "getReadingDiaryNoDataByGroupNoAndReadingDiaryNo() : ";
        logger.info(methodName + "called ");
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = new ErrorMessage();
        ReadingDiaryConsumerData readingDiaryConsumerData = null;
        if(groupNo != null && readingDiaryNo != null){
            readingDiaryConsumerData = consumerNoMasterService.getReadingDiaryConsumerDataByGroupNoAndReadingDiaryNo(groupNo,readingDiaryNo,errorMessage);
        }else{
            errorMessage.setErrorMessage("group no or reading diary no parameter is null");
        }
        if(readingDiaryConsumerData != null){
            response = new ResponseEntity<>(readingDiaryConsumerData,HttpStatus.OK);
        }else{
            logger.error(methodName + "error occurred: " + errorMessage);
            response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }
}
