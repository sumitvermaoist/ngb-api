package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.MeterRentService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.MeterRentInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by ANSHIKA on 23-08-2017.
 */
@RestController
@RequestMapping(value = "meter/rent")
public class MeterRentController {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private static Logger logger = GlobalResources.getLogger(MeterRentController.class);

    /**
     * Requesting spring to get singleton MeterRent Service object.
     */
    @Autowired
    private MeterRentService meterRentService;

    /**
     * URI - meter/rent/code/{code}
     * This method takes meter code and fetches the list of meter rent from backend database when user hit the URI.
     * Returns 200 for OK status and meterRent list if successful.<br>
     * Returns 204 for NO_CONTENT status when no content in list  .<br>
     * Returns 404 for NOT_FOUND  when list not found.<br>
     * Returns 400 for BAD_REQUEST  when input param is null.<br><br>
     * param meterCode
     * return response
     */
    @RequestMapping(method = RequestMethod.GET, value = "/code/{meterCode}", produces = "application/json")
    public ResponseEntity<?> getByMeterCode(@PathVariable("meterCode") String meterCode){
        String methodName = "getByMeterCode() : ";
        logger.info(methodName + "called for meterCode " + meterCode);
        List<MeterRentInterface> meterRents = null;
        ResponseEntity<?> response = null;
        if(meterCode != null){
            logger.info(methodName + "Calling meterRentService method to get list of meterRent");
            meterRents = meterRentService.getByMeterCode(meterCode);
            if(meterRents != null) {
                if (meterRents.size() > 0) {
                    logger.info(methodName + "List of MeterRent against meterCode : " + meterCode + "received successfully");
                    response = new ResponseEntity<>(meterRents, HttpStatus.OK);
                } else {
                    logger.error(methodName + "No content found against meterCode : " + meterCode);
                    response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
                }
            }else{
                logger.error(methodName + "List of MeterRent not found against meterCode : " + meterCode);
                response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }else{
            logger.error(methodName + "It was a BadRequest : input param meterCode : " + meterCode + "found null ");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }



}
