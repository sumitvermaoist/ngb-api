package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.NSCStagingStatusService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbinterface.interfaces.NSCStagingStatusInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping(value = "/nsc/staging/status")
public class NSCStagingStatusController {
    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(NSCStagingController.class);

    @Autowired
    private NSCStagingStatusService nscStagingStatusService;

    /**
     * code by nitish
     * @param locationCode
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/count/location-code/{locationCode}", produces = "application/json")
    public ResponseEntity getCountByLocationCode(@PathVariable("locationCode")String locationCode) {
        String methodName = "getCountByLocationCode() : ";
        logger.info(methodName + "called " + locationCode);
        ResponseEntity<?> response = null;
        if (locationCode != null) {
            long count = nscStagingStatusService.getCountByLocationCode(locationCode);
            if (count >= 0) {
                response = new ResponseEntity<>(count, HttpStatus.OK);
            } else {
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * code by nitish
     * @param locationCode
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/count/location-code/{locationCode}/status/{status}", produces = "application/json")
    public ResponseEntity getCountByLocationCodeAndStatus(@PathVariable("locationCode")String locationCode,@PathVariable("status")String status) {
        String methodName = "getCountByLocationCodeAndStatus() : ";
        logger.info(methodName + "called " + locationCode + " " + status);
        ResponseEntity<?> response = null;
        if (locationCode != null && status != null) {
            long count = nscStagingStatusService.getCountByLocationCodeAndStatus(locationCode,status);
            if (count >= 0) {
                response = new ResponseEntity<>(count, HttpStatus.OK);
            } else {
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * code by nitish
     * @param groupNo
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/count/group-no/{groupNo}", produces = "application/json")
    public ResponseEntity getCountByGroupNo(@PathVariable("groupNo")String groupNo) {
        String methodName = "getCountByGroupNo() : ";
        logger.info(methodName + "called " + groupNo);
        ResponseEntity<?> response = null;
        if (groupNo != null) {
            long count = nscStagingStatusService.getCountByGroupNo(groupNo);
            if (count >= 0) {
                response = new ResponseEntity<>(count, HttpStatus.OK);
            } else {
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * code by nitish
     * @param groupNo
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/count/group-no/{groupNo}/status/{status}", produces = "application/json")
    public ResponseEntity getCountByGroupNoAndStatus(@PathVariable("groupNo")String groupNo,@PathVariable("status")String status) {
        String methodName = "getCountByGroupNoAndStatus() : ";
        logger.info(methodName + "called " + groupNo + " " + status);
        ResponseEntity<?> response = null;
        if (groupNo != null && status != null) {
            long count = nscStagingStatusService.getCountByGroupNoAndStatus(groupNo,status);
            if (count >= 0) {
                response = new ResponseEntity<>(count, HttpStatus.OK);
            } else {
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        } else {
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * This controller can be shifted to dedicated class
     * @param locationCode
     * @return
     */
    @RequestMapping(method = RequestMethod.GET,value = "/location-code/{locationCode}/paged",produces = "application/json")
    public ResponseEntity getByLocationCodeWithPagination(@PathVariable("locationCode")String locationCode,
                                            @PathParam("sortBy") String sortBy, @PathParam("sortOrder") String sortOrder,
                                            @PathParam("pageNumber") int pageNumber, @PathParam("pageSize") int pageSize){
        final String methodName = "getByLocationCodeWithPagination() : ";
        logger.info(methodName + "called location " + locationCode);
        ResponseEntity<?> response = null;
        if(locationCode != null){
            List<NSCStagingStatusInterface> nscStagingStatusInterfaces = nscStagingStatusService.getByLocationCodeWithPagination(locationCode,sortBy,sortOrder,pageNumber,pageSize);
            if(nscStagingStatusInterfaces != null){
                response = new ResponseEntity<>(nscStagingStatusInterfaces, HttpStatus.OK);
            }else{
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            ErrorMessage errorMessage = new ErrorMessage("LOCATION CODE IS NULL");
            response = new ResponseEntity<>(errorMessage,HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * This controller can be shifted to dedicated class
     * @param locationCode
     * @return
     */
    @RequestMapping(method = RequestMethod.GET,value = "/location-code/{locationCode}/status/{status}/paged",produces = "application/json")
    public ResponseEntity getByLocationCodeAndStatusWithPagination(@PathVariable("locationCode")String locationCode,@PathVariable("status")String status,
                                                          @PathParam("sortBy") String sortBy, @PathParam("sortOrder") String sortOrder,
                                                          @PathParam("pageNumber") int pageNumber, @PathParam("pageSize") int pageSize){
        final String methodName = "getByLocationCodeAndStatusWithPagination() : ";
        logger.info(methodName + "called location " + locationCode + " " + status);
        ResponseEntity<?> response = null;
        if(locationCode != null && status != null){
            List<NSCStagingStatusInterface> nscStagingStatusInterfaces = nscStagingStatusService.getByLocationCodeAndStatusWithPagination(locationCode,status,sortBy,sortOrder,pageNumber,pageSize);
            if(nscStagingStatusInterfaces != null){
                response = new ResponseEntity<>(nscStagingStatusInterfaces, HttpStatus.OK);
            }else{
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            ErrorMessage errorMessage = new ErrorMessage("LOCATION CODE IS NULL");
            response = new ResponseEntity<>(errorMessage,HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(method = RequestMethod.GET,value = "/group-no/{groupNo}/paged",produces = "application/json")
    public ResponseEntity getByGroupNoWithPagination(@PathVariable("groupNo")String groupNo,
                                                              @PathParam("sortBy") String sortBy, @PathParam("sortOrder") String sortOrder,
                                                              @PathParam("pageNumber") int pageNumber, @PathParam("pageSize") int pageSize){
        final String methodName = "getByGroupNoWithPagination() : ";
        logger.info(methodName + "called " + groupNo);
        ResponseEntity<?> response = null;
        if(groupNo != null){
            List<NSCStagingStatusInterface> nscStagingStatusInterfaces = nscStagingStatusService.getByGroupNoWithPagination(groupNo,sortBy,sortOrder,pageNumber,pageSize);
            if(nscStagingStatusInterfaces != null){
                response = new ResponseEntity<>(nscStagingStatusInterfaces, HttpStatus.OK);
            }else{
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            ErrorMessage errorMessage = new ErrorMessage("LOCATION CODE IS NULL");
            response = new ResponseEntity<>(errorMessage,HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(method = RequestMethod.GET,value = "/group-no/{groupNo}/status/{status}/paged",produces = "application/json")
    public ResponseEntity getByGroupNoAndStatusWithPagination(@PathVariable("groupNo")String groupNo,@PathVariable("status")String status,
                                                                   @PathParam("sortBy") String sortBy, @PathParam("sortOrder") String sortOrder,
                                                                   @PathParam("pageNumber") int pageNumber, @PathParam("pageSize") int pageSize){
        final String methodName = "getByGroupNoAndStatusWithPagination() : ";
        logger.info(methodName + "called " + groupNo + " " + status);
        ResponseEntity<?> response = null;
        if(groupNo != null && status != null){
            List<NSCStagingStatusInterface> nscStagingStatusInterfaces = nscStagingStatusService.getByGroupNoAndStatusWithPagination(groupNo,status,sortBy,sortOrder,pageNumber,pageSize);
            if(nscStagingStatusInterfaces != null){
                response = new ResponseEntity<>(nscStagingStatusInterfaces, HttpStatus.OK);
            }else{
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            ErrorMessage errorMessage = new ErrorMessage("LOCATION CODE IS NULL");
            response = new ResponseEntity<>(errorMessage,HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(method = RequestMethod.GET,value = "/location-code/{locationCode}/status/{status}",produces = "application/json")
    public ResponseEntity getByLocationCodeAndStatus(@PathVariable("locationCode")String locationCode,@PathVariable("status") String status){
        final String methodName = "getByLocationCodeAndStatus() : ";
        logger.info(methodName + "called location " + locationCode + " status " + status);
        ResponseEntity<?> response = null;
        if(locationCode != null && status != null){
            List<NSCStagingStatusInterface> nscStagingStatusInterfaces = nscStagingStatusService.getByLocationCodeAndStatus(locationCode,status);
            if(nscStagingStatusInterfaces != null && nscStagingStatusInterfaces.size() > 0){
                response = new ResponseEntity<>(nscStagingStatusInterfaces,HttpStatus.OK);
            }else{
                response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }else{
            ErrorMessage errorMessage = new ErrorMessage("STATUS OR LOCATION CODE IS NULL");
            response = new ResponseEntity<>(errorMessage,HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(method = RequestMethod.GET,value = "/group-no/{groupNo}",produces = "application/json")
    public ResponseEntity getByGroupNo(@PathVariable("groupNo")String groupNo){
        final String methodName = "getByGroupNo() : ";
        logger.info(methodName + "called group " + groupNo);
        ResponseEntity<?> response = null;
        if(groupNo != null){
            List<NSCStagingStatusInterface> nscStagingStatusInterfaces = nscStagingStatusService.getByGroupNo(groupNo);
            if(nscStagingStatusInterfaces != null && nscStagingStatusInterfaces.size() > 0){
                response = new ResponseEntity<>(nscStagingStatusInterfaces,HttpStatus.OK);
            }else{
                response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }else{
            ErrorMessage errorMessage = new ErrorMessage("GROUP NO IS NULL");
            response = new ResponseEntity<>(errorMessage,HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(method = RequestMethod.GET,value = "/group-no/{groupNo}/status/{status}",produces = "application/json")
    public ResponseEntity getByrGroupNoAndStatus(@PathVariable("groupNo")String groupNo,@PathVariable("status")String status){
        final String methodName = "getByrGroupNoAndStatus() : ";
        logger.info(methodName + "called group " + groupNo + " status " + status);
        ResponseEntity<?> response = null;
        if(groupNo != null && status != null){
            List<NSCStagingStatusInterface> nscStagingStatusInterfaces = nscStagingStatusService.getByGroupNoAndStatus(groupNo,status);
            if(nscStagingStatusInterfaces != null && nscStagingStatusInterfaces.size() > 0){
                response = new ResponseEntity<>(nscStagingStatusInterfaces,HttpStatus.OK);
            }else{
                response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }else{
            ErrorMessage errorMessage = new ErrorMessage("GROUP NO OR STATUS IS NULL");
            response = new ResponseEntity<>(errorMessage,HttpStatus.BAD_REQUEST);
        }
        return response;
    }
}
