package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.services.RegionService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.RegionInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by MPWZERP on 18-07-2017.
 */
@RestController
@RequestMapping(value = "region")
public class RegionController {
    /**
     * Asking Spring to get Singleton Object of RegionService
     */
    @Autowired
    RegionService regionService;
    /**
     * Getting logger for logging in current class
     */
    Logger logger = GlobalResources.getLogger(RegionController.class);

    /**
     * URI - /region<br><br>
     * This getAll() method is used to get regions<br><br>
     * Response : 200(OK) : to successfully fetch regions<br>
     * Response : 417(EXPECTATION_FAILED) : regions not received successfully<br><br>
     * return response
     */
    @RequestMapping(method = RequestMethod.GET , produces = "application/json")
    public ResponseEntity<?> getAll(){
        String methodName = "getAll() : ";
        ResponseEntity<?> response = null;
        logger.info(methodName + "Request receive as GET to fetch All Region objects");
        List<? extends RegionInterface> regions = regionService.getAll();
        if(regions != null){
            if(regions.size() > 0){
                logger.info(methodName + "successfully got List<Region> with size : "+regions.size());
                response = new ResponseEntity<Object>(regions , HttpStatus.OK);
            }else{
                logger.error(methodName + "successfully got List<Region> with size : "+regions.size());
                response = new ResponseEntity<Object>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName + "Received List<Region> is null");
            response = new ResponseEntity<Object>(HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }
}
