package com.mppkvvcl.ngbapi.controllers;

import com.mppkvvcl.ngbapi.exceptionhandlers.RuntimeExceptionHandler;
import com.mppkvvcl.ngbapi.services.ScheduleService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ScheduleInterface;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbentity.beans.Schedule;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * Created by ANSHIKA on 04-07-2017.
 */
@RestController
@RequestMapping(value = "/schedule")
public class ScheduleController {
    /**
     * Requesting spring to get a singleton ScheduleService object.
     */
    @Autowired
    ScheduleService scheduleService;
    /**
     * Getting logger object from GlobalResources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(ScheduleController.class);

    /**
     * URI  /schedule/group-no/{groupNo}/bill-status/{billStatus}/submitted/{submitted}<br><br>
     * This method is executed when user hit the URI.<br><br>
     * Response 200 for OK status and list of schedules is sent if successful.<br>
     * Response 204 NO_CONTENT status is sent if no content in list.<br>
     * Response 404 for NOT_FOUND status is sent when list is null.<br>
     * Response 400 for BAD_REQUEST status is sent if input param are null<br><br>
     * param groupNo<br>
     * param billStatus<br>
     * param submitted<br>
     * return response
     */
    @RequestMapping(value = "group-no/{groupNo}/bill-status/{billStatus}/submitted/{submitted}", method = RequestMethod.GET)
    public ResponseEntity<?> getByGroupNoAndBillStatusAndSubmitted(@PathVariable("groupNo") String groupNo, @PathVariable("billStatus") String billStatus, @PathVariable("submitted") String submitted){
        String methodName = "getByGroupNoAndBillStatusAndSubmitted() : ";
        logger.info(methodName + "called for groupNo : " + groupNo + ", billStatus : " + billStatus + "and submitted : " + submitted);
        List<ScheduleInterface> schedules = null;
        ResponseEntity<?> response = null;
        if(groupNo != null && billStatus != null && submitted != null) {
            logger.info(methodName + "Calling schedule service method to get list of schedules");
            schedules = scheduleService.getByGroupNoAndBillStatusAndSubmitted(groupNo, billStatus, submitted);
            if (schedules != null) {
                if (schedules.size() > 0) {
                    logger.info(methodName + "List of schedule received successfully with" + schedules.size() + "schedules");
                    response = new ResponseEntity<>(schedules, HttpStatus.OK);
                } else {
                    logger.error(methodName + "List of schedule received but with no content");
                    response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
                }
            } else {
                logger.error(methodName + "List not found");
                response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }else{
            logger.error(methodName + "Input param found null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return  response;
    }

    /**
     * URI - /schedule/latest/group-no/{groupNo}<br><br>
     * This method takes groupNo and fetch the latest schedule reading against groupNo.<br><br>
     * Response 200 for OK with schedule is sent if successful,<br>
     * Response 404 for NOT_FOUND is sent when schedule not found,<br>
     * Response 400 for BAD_REQUEST is sent when input groupId found null.<br><br>
     * param groupNo<br>
     * return response
     */
    @RequestMapping(value = "/latest/group-no/{groupNo}", method = RequestMethod.GET)
    public ResponseEntity<?> getLatestScheduleByGroupNo(@PathVariable("groupNo") String groupNo){
        String methodName = "getLatestScheduleByGroupNo() : " ;
        logger.info(methodName + "called for groupNo" + groupNo);
        ResponseEntity<?> response = null;
        ScheduleInterface schedule = null;
        ErrorMessage errorMessage = null;
        if(groupNo != null){
            logger.info(methodName + "Calling schedule service to get latest reading ");
            schedule = scheduleService.getLatestScheduleByGroupNo(groupNo);
            if(schedule != null) {
                logger.info(methodName + "Got latest schedule against groupNo "+groupNo);
                response = new ResponseEntity<>(schedule, HttpStatus.OK);
            }else{
                logger.error(methodName + "latest schedule not found against groupNo "+groupNo);
                errorMessage = new ErrorMessage("latest schedule not found against groupNo "+groupNo);
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName + "Input param groupNo found null" + groupNo);
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }


    /**
     * URI - /schedule/group-no/{groupNo}<br><br>
     * This method takes groupNo and fetch all schedules against groupNo.<br><br>
     * Response 200 for OK with schedules are sent if successful,<br>
     * Response 404 for NOT_FOUND is sent when schedules not found,<br>
     * Response 400 for BAD_REQUEST is sent when input groupId found null.<br><br>
     * param groupNo<br>
     * return response
     */
    @RequestMapping(value = "/group-no/{groupNo}", method = RequestMethod.GET)
    public ResponseEntity<?> getAllSchedulesByGroupNo(@PathVariable("groupNo") String groupNo){
        String methodName = "getAllSchedulesByGroupNo() : " ;
        logger.info(methodName + "called for groupNo" + groupNo);
        ResponseEntity<?> response = null;
        List<ScheduleInterface> schedules = null;
        if(groupNo != null){
            schedules = scheduleService.getAllSchedulesByGroupNo(groupNo);
            if(schedules != null && schedules.size() > 0) {
                logger.info(methodName + "Got schedules against groupId");
                response = new ResponseEntity<>(schedules, HttpStatus.OK);
            }else{
                logger.error(methodName + "latest schedule not found against groupId");
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName + "Input param groupNo found null" + groupNo);
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * URI - /schedule<br><br>
     * This insert method takes schedule object and insert it into the backend database when user hit the URI.<br><br>
     * Response 201 for CREATED and schedule object is sent if successful<br>
     * Response 417 for EXPECTATION_FAILED is sent when insertion fail<br>
     * Response 400 for BAD_REQUEST is sent when input param found null.<br><br>
     * param schedule<br>
     * return response
     */
    @RequestMapping(method =  RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public  ResponseEntity<?> insert(@RequestBody Schedule schedule){
        String methodName = "insert() : ";
        logger.info(methodName + "called");
        ResponseEntity<?> response = null;
        ScheduleInterface insertedSchedule = null;
        ErrorMessage errorMessage = null;
        if(schedule != null) {
            try{
                insertedSchedule = scheduleService.insert(schedule);
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                insertedSchedule = null;
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                insertedSchedule = null;
            }
            if (insertedSchedule != null) {
                logger.info(methodName + "schedule object inserted successfully" + insertedSchedule);
                response = new ResponseEntity<>(insertedSchedule, HttpStatus.CREATED);
            } else {
                logger.error(methodName + "Unable to insert schedule object");
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }else {
            logger.error(methodName + "Input param schedule found null : " + schedule);
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * URI - /schedule<br><br>
     * This update method takes schedule object and update it into the backend database when user hit the URI.<br><br>
     * Response 200 for OK and schedule object is sent if successful<br>
     * Response 417 for EXPECTATION_FAILED is sent when insertion fail<br>
     * Response 400 for BAD_REQUEST is sent when input param found null.<br><br>
     * param schedule<br>
     * return response
     */
    @RequestMapping(method =  RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public  ResponseEntity<?> editPendingSchedule(@RequestBody Schedule schedule){
        String methodName = "editPendingSchedule() : ";
        logger.info(methodName + "called");
        ResponseEntity<?> response = null;
        ScheduleInterface updatedSchedule = null;
        ErrorMessage errorMessage = null;
        if(schedule != null) {
            try{
                updatedSchedule = scheduleService.editPendingSchedule(schedule);
            }catch(RuntimeException e){
                errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                updatedSchedule = null;
            }catch (Exception ee){
                errorMessage = new ErrorMessage(ee.getMessage());
                logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                updatedSchedule = null;
            }
            if (updatedSchedule != null) {
                logger.info(methodName + "schedule object updated successfully" + updatedSchedule);
                response = new ResponseEntity<>(updatedSchedule, HttpStatus.OK);
            } else {
                logger.error(methodName + "Unable to update schedule object");
                response = new ResponseEntity<>(errorMessage,HttpStatus.EXPECTATION_FAILED);
            }
        }else {
            logger.error(methodName + "Input param schedule found null : " + schedule);
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * URI - /schedule/group-no/{groupNo}<br><br>
     * This method takes groupNo and fetch all schedules against groupNo.<br><br>
     * Response 200 for OK with schedules are sent if successful,<br>
     * Response 404 for NOT_FOUND is sent when schedules not found,<br>
     * Response 400 for BAD_REQUEST is sent when input groupId found null.<br><br>
     * param groupNo<br>
     * return response
     */
    @RequestMapping(value = "/bill-date/{billDate}", method = RequestMethod.GET)
    public ResponseEntity<?> getDueDatesByBillDate(@PathVariable("billDate") String billDateString)throws Exception{
        String methodName = "getDueDatesByBillDate() : " ;
        logger.info(methodName + "called for " + billDateString);
        ResponseEntity<?> response = null;
        ErrorMessage errorMessage = null;
        List<Date> dates = null;
        if (billDateString != null) {
            billDateString = billDateString.trim();
            Date billDate = GlobalResources.getDateFromString(billDateString);
            logger.info(methodName + "Got request to get due dates against bill date" + billDate);
            if(billDate != null){
                logger.info(methodName + "Calling schedule service to get due dates ");
                try{
                    dates = scheduleService.getDueDates(billDate);
                }catch(RuntimeException e){
                    errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
                    logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
                    dates = null;
                }catch (Exception ee){
                    errorMessage = new ErrorMessage(ee.getMessage());
                    logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
                    dates = null;
                }
                if(dates != null && dates.size() == 2) {
                    logger.info(methodName + "Got due dates against bill date");
                    response = new ResponseEntity<>(dates, HttpStatus.OK);
                }else{
                    logger.error(methodName + "not able to determine due dates");
                    response = new ResponseEntity<>(errorMessage,HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
                }
            }else{
                logger.error(methodName + "Input param bill date found null" + billDateString);
                response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        }else{
            logger.error(methodName + "bill date is null");
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return response;
    }

    @RequestMapping(value = "top2/group-no/{groupNo}", method = RequestMethod.GET)
    public ResponseEntity<?> getTop2SchedulesByGroupNo(@PathVariable("groupNo") String groupNo){
        String methodName = "getTop2SchedulesByGroupNo() : ";
        logger.info(methodName + "called for groupNo" + groupNo);
        ResponseEntity<?> response = null;
        List<ScheduleInterface> schedules = null;
        if(groupNo != null){
            schedules = scheduleService.getTop2ByGroupNo(groupNo);
            if(schedules != null && schedules.size() == 2) {
                logger.info(methodName + "Got schedules against groupNo");
                response = new ResponseEntity<>(schedules, HttpStatus.OK);
            }else{
                logger.error(methodName + "latest schedule not found against groupId");
                response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }else{
            logger.error(methodName + "Input param groupNo found null" + groupNo);
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }


    /**
     * URI - /schedule<br><br>
     * This update method takes schedule object and update it into the backend database when user hit the URI.<br><br>
     * Response 200 for OK and schedule object is sent if successful<br>
     * Response 417 for EXPECTATION_FAILED is sent when insertion fail<br>
     * Response 400 for BAD_REQUEST is sent when input param found null.<br><br>
     * param schedule<br>
     * return response
     */
    @RequestMapping(method =  RequestMethod.PUT,value = "/submit/id/{id}", consumes = "application/json", produces = "application/json")
    public  ResponseEntity<?> submit(@PathVariable long id) {
        String methodName = "submit() : ";
        logger.info(methodName + "called with id " + id);
        ResponseEntity<?> response = null;
        ScheduleInterface updatedSchedule = null;
        ErrorMessage errorMessage = null;
        try {
            updatedSchedule = scheduleService.submit(id);
        } catch (RuntimeException e) {
            errorMessage = new ErrorMessage(RuntimeExceptionHandler.handleException());
            logger.error(methodName + "Received Runtime Exception in controller with error message as :" + e.getMessage());
            updatedSchedule = null;
        } catch (Exception ee) {
            errorMessage = new ErrorMessage(ee.getMessage());
            logger.error(methodName + "Received Exception in controller with error message as :" + ee.getMessage());
            updatedSchedule = null;
        }
        if (updatedSchedule != null) {
            logger.info(methodName + "schedule submitted successfully" + updatedSchedule);
            response = new ResponseEntity<>(updatedSchedule, HttpStatus.OK);
        } else {
            logger.error(methodName + "Unable to submit schedule object");
            response = new ResponseEntity<>(errorMessage, HttpStatus.EXPECTATION_FAILED);
        }
        return response;
    }
}
