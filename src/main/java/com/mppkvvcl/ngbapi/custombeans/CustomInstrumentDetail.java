package com.mppkvvcl.ngbapi.custombeans;

import com.mppkvvcl.ngbinterface.interfaces.InstrumentDetailInterface;
import com.mppkvvcl.ngbinterface.interfaces.InstrumentPaymentMappingInterface;
import com.mppkvvcl.ngbinterface.interfaces.PaymentInterface;

import java.util.List;

/**
 * Created by Vikas on 8/9/2017.
 */
public class CustomInstrumentDetail {

    private List<InstrumentDetailInterface> instrumentDetails;
    private List<InstrumentPaymentMappingInterface> instrumentPaymentMappings;
    private List<PaymentInterface> payments;

    public List<InstrumentDetailInterface> getInstrumentDetails() {
        return instrumentDetails;
    }

    public void setInstrumentDetails(List<InstrumentDetailInterface> instrumentDetails) {
        this.instrumentDetails = instrumentDetails;
    }

    public List<InstrumentPaymentMappingInterface> getInstrumentPaymentMappings() {
        return instrumentPaymentMappings;
    }

    public void setInstrumentPaymentMappings(List<InstrumentPaymentMappingInterface> instrumentPaymentMappings) {
        this.instrumentPaymentMappings = instrumentPaymentMappings;
    }

    public List<PaymentInterface> getPayments() {
        return payments;
    }

    public void setPayments(List<PaymentInterface> payments) {
        this.payments = payments;
    }

    @Override
    public String toString() {
        return "CustomInstrumentDetail{" + "instrumentDetail=" + instrumentDetails + ", instrumentPaymentMappings=" + instrumentPaymentMappings + ", payments=" + payments + "}";
    }
}
