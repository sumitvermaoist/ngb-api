package com.mppkvvcl.ngbapi.custombeans;

import com.mppkvvcl.ngbinterface.interfaces.CTRMasterInterface;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerInformationInterface;
import com.mppkvvcl.ngbinterface.interfaces.MeterMasterInterface;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterInterface;

import java.math.BigDecimal;

/**
 * Created by PREETESH on 7/10/2017.
 */
public class CustomMeterReplacement {

    private ConsumerInformationInterface consumerInformation;
    private BigDecimal mf;
    private ReadMasterInterface readMaster;
    private MeterMasterInterface meterMaster;
    private CTRMasterInterface ctrMaster;

    public ConsumerInformationInterface getConsumerInformation() {
        return consumerInformation;
    }

    public void setConsumerInformation(ConsumerInformationInterface consumerInformation) {
        this.consumerInformation = consumerInformation;
    }

    public BigDecimal getMf() {
        return mf;
    }

    public void setMf(BigDecimal mf) {
        this.mf = mf;
    }

    public ReadMasterInterface getReadMaster() {
        return readMaster;
    }

    public void setReadMaster(ReadMasterInterface readMaster) {
        this.readMaster = readMaster;
    }

    public MeterMasterInterface getMeterMaster() {
        return meterMaster;
    }

    public void setMeterMaster(MeterMasterInterface meterMaster) {
        this.meterMaster = meterMaster;
    }

    public CTRMasterInterface getCtrMaster() {
        return ctrMaster;
    }

    public void setCtrMaster(CTRMasterInterface ctrMaster) {
        this.ctrMaster = ctrMaster;
    }

    @Override
    public String toString() {
        return "CustomMeterReplacement{" +
                "consumerInformation=" + consumerInformation +
                ", mf=" + mf +
                ", readMaster=" + readMaster +
                ", meterMaster=" + meterMaster +
                ", ctrMaster=" + ctrMaster +
                '}';
    }
}
