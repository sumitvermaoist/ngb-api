package com.mppkvvcl.ngbapi.custombeans;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterKWInterface;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterPFInterface;
import com.mppkvvcl.ngbapi.deserializers.CustomReadMasterDeserializer;
import com.mppkvvcl.ngbentity.beans.ReadMaster;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by vikas on 31-Oct-17.
 */
@JsonDeserialize(using = CustomReadMasterDeserializer.class)
public class CustomReadMaster extends ReadMaster {
    
    public CustomReadMaster(){}

    public CustomReadMaster(long id, String billMonth, String groupNo, String readingDiaryNo, String consumerNo, String meterIdentifier, Date readingDate, String readingType,
                      String meterStatus, String replacementFlag, String source, BigDecimal reading, BigDecimal difference, BigDecimal mf, BigDecimal consumption, BigDecimal assessment,
                      BigDecimal propagatedAssessment, BigDecimal totalConsumption, boolean usedOnBill, String createdBy, Date createdOn, String updatedBy, Date updatedOn,
                      String remark, ReadMasterKWInterface readMasterKWInterface, ReadMasterPFInterface readMasterPFInterface){
        this.setId(id);
        this.setBillMonth(billMonth);
        this.setGroupNo(groupNo);
        this.setReadingDiaryNo(readingDiaryNo);
        this.setConsumerNo(consumerNo);
        this.setMeterIdentifier(meterIdentifier);
        this.setReadingDate(readingDate);
        this.setReadingType(readingType);
        this.setMeterStatus(meterStatus);
        this.setReplacementFlag(replacementFlag);
        this.setSource(source);
        this.setReading(reading);
        this.setDifference(difference);
        this.setMf(mf);
        this.setConsumption(consumption);
        this.setAssessment(assessment);
        this.setPropagatedAssessment(propagatedAssessment);
        this.setTotalConsumption(totalConsumption);
        this.setUsedOnBill(usedOnBill);
        this.setRemark(remark);
        this.setReadMasterKW(readMasterKWInterface);
        this.setReadMasterPF(readMasterPFInterface);
        this.setCreatedBy(createdBy);
        this.setCreatedOn(createdOn);
        this.setUpdatedBy(updatedBy);
        this.setUpdatedOn(updatedOn);
    }
}
