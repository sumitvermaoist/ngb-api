package com.mppkvvcl.ngbapi.custombeans;

import com.mppkvvcl.ngbinterface.interfaces.GroupInterface;

import java.util.List;

/**
 * Created by PREETESH on 11/6/2017.
 */
public class GroupConsumerData  {

    private GroupInterface group;
    private long consumers;
    private long activeConsumers;
    private long inactiveConsumers;
    private List<ReadingDiaryConsumerData> readingDiaries;

    public long getConsumers() {
        return consumers;
    }

    public void setConsumers(long consumers) {
        this.consumers = consumers;
    }

    public long getActiveConsumers() {
        return activeConsumers;
    }

    public void setActiveConsumers(long activeConsumers) {
        this.activeConsumers = activeConsumers;
    }

    public long getInactiveConsumers() {
        return inactiveConsumers;
    }

    public void setInactiveConsumers(long inactiveConsumers) {
        this.inactiveConsumers = inactiveConsumers;
    }

    public List<ReadingDiaryConsumerData> getReadingDiaries() {
        return readingDiaries;
    }

    public void setReadingDiaries(List<ReadingDiaryConsumerData> readingDiaries) {
        this.readingDiaries = readingDiaries;
    }

    public GroupInterface getGroup() {
        return group;
    }

    public void setGroup(GroupInterface group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return "GroupConsumerData{" +
                "consumers=" + consumers +
                ", activeConsumers=" + activeConsumers +
                ", inactiveConsumers=" + inactiveConsumers +
                ", readingDiaries=" + readingDiaries +
                '}';
    }
}
