package com.mppkvvcl.ngbapi.factories;

import com.mppkvvcl.ngbentity.beans.BPLConnectionMapping;
import com.mppkvvcl.ngbinterface.interfaces.BPLConnectionMappingInterface;

public class BPLConnectionMappingFactory {
    public static BPLConnectionMappingInterface build(){
        BPLConnectionMappingInterface bplConnectionMappingInterface = new BPLConnectionMapping();
        return bplConnectionMappingInterface;
    }
}
