package com.mppkvvcl.ngbapi.factories;

import com.mppkvvcl.ngbentity.beans.ConsumerInformationHistory;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerInformationHistoryInterface;

public class ConsumerInformationHistoryFactory {
    public static ConsumerInformationHistoryInterface build(){
        ConsumerInformationHistoryInterface consumerInformationHistoryInterface = new ConsumerInformationHistory();
        return consumerInformationHistoryInterface;
    }
}
