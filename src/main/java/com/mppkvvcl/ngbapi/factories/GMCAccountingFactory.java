package com.mppkvvcl.ngbapi.factories;

import com.mppkvvcl.ngbentity.beans.GMCAccounting;
import com.mppkvvcl.ngbinterface.interfaces.GMCAccountingInterface;

public class GMCAccountingFactory {

        public static GMCAccountingInterface build(){
            GMCAccountingInterface gmcAccountingInterface = new GMCAccounting();
            return gmcAccountingInterface;
        }


}
