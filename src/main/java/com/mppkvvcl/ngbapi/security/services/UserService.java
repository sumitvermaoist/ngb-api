package com.mppkvvcl.ngbapi.security.services;

import com.mppkvvcl.ngbapi.security.beans.CustomUserDetails;
import com.mppkvvcl.ngbapi.security.beans.User;
import com.mppkvvcl.ngbapi.security.repositories.UserRepository;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by NITISH on 24-04-2017.
 */
@Service
public class UserService implements UserDetailsService {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(UserService.class);

    /**
    * Asking spring to inject EntityManager for underlying hibernate
    * for creating sessions and transactions;
    */
    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleService roleService;

    public List<User> getAllUsers(){
        System.out.println("Finding all users");
        return userRepository.findAll();
    }

    private User getByUsername(String username){
        String methodName = "getByUsername() : ";
        logger.info(methodName + "Finding user by username: "+username);
        User user = userRepository.findByUsername(username);
        entityManager.detach(user);
        return user;
    }

    /*public User getByUsernameWithoutPassword(String username){
        String methodName = "getByUsernameWithoutPassword() : ";
        logger.info(methodName + "Finding user by username: "+username);
        User user = userRepository.findByUsername(username);
        entityManager.detach(user);
        if(user != null) user.setPassword("UserService" + System.currentTimeMillis());
        System.out.println(methodName + "User without password is " + user);
        return user;
    }*/

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        String methodName = "loadUserByUsername() : ";
        logger.info(methodName +"called with username: " + username);
        User user = getByUsername(username);
        if(user != null){
            logger.info(methodName + "found user with username: " + username);
            logger.info(methodName + "with password as: " + user.getPassword().length());
            List<String> roles = roleService.getAllRoleNames();
            return new CustomUserDetails(user,roles);
        }else{
            logger.error(methodName + "no user found with username: " + username + " throwing exception");
            throw new UsernameNotFoundException("Username not found for username : " + username);
        }
    }
}
