package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbinterface.interfaces.AdjustmentRangeInterface;
import com.mppkvvcl.ngbdao.daos.AdjustmentRangeDAO;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * Created by ANKIT on 18-08-2017.
 */
@Service
public class AdjustmentRangeService {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    Logger logger  = GlobalResources.getLogger(AdjustmentRangeService.class);

    /**
     * Asking spring to inject dependency on AdjustmentRangeRepository.
     * So that we can use it to perform various operation through repo.
     */
    @Autowired
    AdjustmentRangeDAO adjustmentRangeDAO;

    /*
     * This Method will fetch the AdjustmentRange for received code and amount
     * param code<br>
     * param amount<br>
     * return
     */
    public AdjustmentRangeInterface getAdjustmentRangeByCodeAndAmount(int code, BigDecimal amount){
        String methodName = "getAdjustmentRangeByCodeAndAmount() : ";
        logger.info(methodName + "called");
        AdjustmentRangeInterface adjustmentRange = adjustmentRangeDAO.getByCodeAndAmount(code,amount);
        return adjustmentRange;
    }
}
