package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.custombeans.CustomAdjustment;
import com.mppkvvcl.ngbapi.security.beans.Role;
import com.mppkvvcl.ngbapi.security.services.RoleService;
import com.mppkvvcl.ngbapi.security.services.UserDetailService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbinterface.interfaces.AdjustmentHierarchyInterface;
import com.mppkvvcl.ngbinterface.interfaces.AdjustmentInterface;
import com.mppkvvcl.ngbinterface.interfaces.AdjustmentRangeInterface;
import com.mppkvvcl.ngbinterface.interfaces.ZoneInterface;
import com.mppkvvcl.ngbdao.daos.AdjustmentDAO;
import com.mppkvvcl.ngbentity.beans.Adjustment;
import com.mppkvvcl.ngbentity.beans.AdjustmentProfile;
import com.mppkvvcl.ngbentity.beans.UserDetail;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by SHIVANSHU on 14-07-2017.
 */
@Service
public class AdjustmentService {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    Logger logger = GlobalResources.getLogger(AdjustmentService.class);

    /**
     * Asking spring to inject dependency on AdjustmentRepository.
     * So that we can use it to perform various operation through repo.
     */
    /*@Autowired
    AdjustmentRepository adjustmentRepository;*/

    @Autowired
    AdjustmentDAO adjustmentDAO;

    @Autowired
    AdjustmentRangeService adjustmentRangeService;

    @Autowired
    private AdjustmentHierarchyService adjustmentHierarchyService;

    @Autowired
    private AdjustmentProfileService adjustmentProfileService;

    @Autowired
    private UserDetailService userDetailService;

    @Autowired
    private RoleService roleService;

    @Autowired
    AdjustmentTypeService adjustmentTypeService;

    /**
     * Asking spring to get Singleton object of LocationService.
     */
    @Autowired
    private ZoneService zoneService;

    /**
     * This getByConsumerNoAndPostingBillMonth is used to get Adjustment object with
     * consumerNo and postingBillMonth.<br><br>
     * param consumerNo<br>
     * param postingBillMonth<br>
     * return adjustment
     */
    public AdjustmentInterface getByConsumerNoAndPostingBillMonth(String consumerNo, String postingBillMonth) {
        String methodName = "getByConsumerNoAndPostingBillMonth() : ";
        logger.info(methodName + "called");
        AdjustmentInterface adjustment = null;
        if (consumerNo != null && postingBillMonth != null) {
            adjustment = adjustmentDAO.getByConsumerNoAndPostingBillMonth(consumerNo, postingBillMonth);
        }
        return adjustment;
    }

    /**
     * This getByConsumerNoAndPostedAndDeleted method is used for getting list of Adjustment with consumer
     * where posted is false and deleted is also false.<br><br>
     * param consumerNo<br>
     * param posted<br>
     * param deleted<br>
     * return
     */
    public List<AdjustmentInterface> getByConsumerNoAndPostedAndDeleted(String consumerNo, boolean posted, boolean deleted){
        String methodName = "getByConsumerNoAndPostedAndDeleted() : ";
        logger.info(methodName + "called");
        List<AdjustmentInterface> adjustments = null;
        if (consumerNo != null) {
            adjustments = adjustmentDAO.getByConsumerNoAndPostedAndDeleted(consumerNo, posted, deleted);
        }
        return adjustments;
    }

    public List<AdjustmentInterface> getByConsumerNo(String consumerNo){
        String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called");
        List<AdjustmentInterface> adjustments = null;
        if (consumerNo != null) {
            adjustments = adjustmentDAO.getByConsumerNoAndPostedAndDeleted(consumerNo, AdjustmentInterface.POSTED_FALSE, AdjustmentInterface.DELETED_FALSE);
        }
        return adjustments;
    }

    public AdjustmentInterface getById(long id) {
        String methodName = "getById() : ";
        logger.info(methodName + "called for adjustment id: "+id);
        AdjustmentInterface adjustment = adjustmentDAO.getById(id);
        return adjustment;
    }

    public List<AdjustmentInterface> getAllOpenAdjustmentByLocationCodeAndUserRole(String locationCode, String userRole) throws Exception {
        String methodName = "getAllOpenAdjustmentByLocationCodeAndUserRole() : ";
        logger.info(methodName + "called for location code: "+locationCode);
        List<AdjustmentInterface> adjustments = null;
        ZoneInterface location = null;
        List<ZoneInterface> locations = null;
        if (locationCode == null || userRole == null) {
            logger.error(methodName + "Location Code or Role passed is Null");
            return adjustments;
        }
        switch (userRole) {
            case Role.OAG:
                adjustments = getAllOpenAdjustmentByLocationCode(locationCode);
                break;
            case Role.JE:
                adjustments = getAllOpenAdjustmentByLocationCode(locationCode);
                break;
            case Role.OIC:
                adjustments = getAllOpenAdjustmentByLocationCode(locationCode);
                break;
            case Role.EE:
                location = zoneService.getByLocationCode(locationCode);
                locations = zoneService.getByDivisionId(location.getDivision().getId());
                for(ZoneInterface loc : locations){
                    adjustments.addAll(getAllOpenAdjustmentByLocationCode(loc.getCode()));
                }
                break;
            case Role.SE:
                location = zoneService.getByLocationCode(locationCode);
                locations = zoneService.getByCircleId(location.getDivision().getCircle().getId());
                for(ZoneInterface loc : locations){
                    adjustments.addAll(getAllOpenAdjustmentByLocationCode(loc.getCode()));
                }
                break;
            default:
                logger.error("No Matched user role Found for Role: " + userRole);
                throw new Exception("No Matched user role Found");
        }
        return adjustments;
    }

    public List<AdjustmentInterface> getAllOpenAdjustmentByLocationCode(String locationCode) throws Exception{
        String methodName = "getAllOpenAdjustmentByLocationCode() : ";
        logger.info(methodName + "called");
        List<AdjustmentInterface> adjustments = null;
        if (locationCode == null) {
            logger.error(methodName + "Location Code passed is Null");
            throw new Exception("Location Code passed is Null");
        }
        adjustments = adjustmentDAO.getByLocationCodeAndPostedAndDeleted(locationCode, AdjustmentInterface.POSTED_FALSE, AdjustmentInterface.DELETED_FALSE);
        return adjustments;
    }

    @Transactional(rollbackFor = Exception.class)
    public AdjustmentInterface insert(AdjustmentInterface adjustment) throws Exception {
        String methodName = "insert() : ";
        logger.info(methodName + " Called");
        if (adjustment == null) {
            logger.error("Adjustment object received for insertion is null");
            throw new Exception("Something went wrong. Please try again.");
        }
        AdjustmentInterface adjustmentAlreadyAdded = adjustmentDAO.getByConsumerNoAndCodeAndPostedAndDeleted(adjustment.getConsumerNo(),adjustment.getCode(), Adjustment.POSTED_FALSE,Adjustment.DELETED_FALSE);
        if(adjustmentAlreadyAdded != null){
            logger.error("Adjustment Already Added For Adjustment Type "+adjustment.getCode());
            throw new Exception("Adjustment Already Present For provided Adjustment Type for Consumer, Please Check.");
        }
        AdjustmentInterface insertedAdjustment = null;
        String username = GlobalResources.getLoggedInUser();
        UserDetail userDetail = userDetailService.getByUsername(username);
        if (userDetail == null) {
            logger.error("Something wrong happened while getting the userDetail. Received as null");
            throw new Exception("No User Found.");
        }
        String locationCode = userDetail.getLocationCode();
        adjustment.setLocationCode(locationCode);
        Role role = roleService.getByRole(userDetail.getRole());
        if (role == null) {
            logger.error("Could not retrieve role for logged in userDetail");
            throw new Exception("No User Found");
        }
        int userPriority = role.getPriority();
        AdjustmentRangeInterface adjustmentRange = adjustmentRangeService.getAdjustmentRangeByCodeAndAmount(adjustment.getCode(), adjustment.getAmount());
        if (adjustmentRange == null) {
            logger.error("Adjustment Range not found for given adjustment code and amount.");
            throw new Exception("Entered Adjustment Amount is not allowed for this adjustment category.");
        }
        adjustment.setRangeId(adjustmentRange.getId());
        int adjustmentPriority = adjustmentRange.getMaxPriority();
        List<AdjustmentHierarchyInterface> adjustmentHierarchyList = adjustmentHierarchyService.getByUserPriorityAndAdjustmentPriority(userPriority, adjustmentPriority);
        if (adjustmentHierarchyList == null || adjustmentHierarchyList.size() <= 0) {
            logger.error("Adjustment Hierarchy not found.");
            throw new Exception("Unable to add Adjustment.");
        }
        List<AdjustmentProfile> adjustmentProfileList = new ArrayList<AdjustmentProfile>();
        for (AdjustmentHierarchyInterface adjustmentHierarchy : adjustmentHierarchyList) {
            AdjustmentProfile adjustmentProfile = new AdjustmentProfile();
            String roleName = adjustmentHierarchy.getRole();
            if (adjustmentHierarchy.getPriority() == userPriority) {
                adjustmentProfile.setRemark(adjustment.getRemark());
                adjustmentProfile.setApprover(username);
                adjustmentProfile.setStatus(true);
            } else {
                switch (roleName) {
                    case Role.JE:
                        UserDetail jeUserDetail = userDetailService.getByLocationCodeAndRole(locationCode,Role.JE).get(0);
                        adjustmentProfile.setApprover(jeUserDetail.getUsername());
                        break;
                    case Role.OIC:
                        adjustmentProfile.setApprover(userDetailService.getAEByLocationCode(locationCode));
                        break;

                    case Role.EE:
                        adjustmentProfile.setApprover(userDetailService.getEEByLocationCode(locationCode));
                        break;
                    case Role.SE:
                        adjustmentProfile.setApprover(userDetailService.getSEByLocationCode(locationCode));
                        break;
                    default:
                        logger.error("No Matched userDetail role Found for Role: " + roleName);
                        throw new Exception("Incorrect User");
                }
                adjustmentProfile.setStatus(false);
            }
            if (userPriority == adjustmentPriority) {
                adjustment.setApprovalStatus(AdjustmentInterface.APPROVED);
            } else {
                adjustment.setApprovalStatus(AdjustmentInterface.PENDING);
            }
            adjustmentProfile.setLocationCode(locationCode);
            adjustmentProfileList.add(adjustmentProfile);
        }
        setAuditDetails(adjustment);
        insertedAdjustment = adjustmentDAO.add(adjustment);

        if (insertedAdjustment == null) {
            logger.error("Unable to insert adjustment.");
            throw new Exception("Unable to insert adjustment.");
        }
        logger.info("Adjustment inserted Successfully, Proceeding with insert Adjustment Profile.");
        for (AdjustmentProfile profile : adjustmentProfileList) {
            profile.setAdjustmentId(insertedAdjustment.getId());
            adjustmentProfileService.insert(profile);
        }
        return insertedAdjustment;
    }

    private void setAuditDetails(AdjustmentInterface adjustment) {
        String methodName = "setAuditDetails()  : ";
        if (adjustment != null) {
            Date date = GlobalResources.getCurrentDate();
            String user = GlobalResources.getLoggedInUser();
            adjustment.setUpdatedOn(date);
            adjustment.setUpdatedBy(user);
            adjustment.setCreatedOn(date);
            adjustment.setCreatedBy(user);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public AdjustmentInterface update(AdjustmentInterface adjustment) throws Exception{
        String methodName = "update() : ";
        logger.info(methodName + "called");
        AdjustmentInterface updatedAdjustment = null;
        if (adjustment != null) {
            updatedAdjustment = adjustmentDAO.add(adjustment);
        }
        return updatedAdjustment;
    }

    /**
     * This Method maps List of AdjustmentInterface into List of CustomAdjustments
     * @param adjustments
     * @return CustomAdjustments
     */
    public List<CustomAdjustment> prepareCustomAdjustments(List<? extends AdjustmentInterface> adjustments){
        List<CustomAdjustment> customAdjustments = null;
        if(adjustments != null){
            customAdjustments = new ArrayList<CustomAdjustment>();
            for(AdjustmentInterface adjustment : adjustments) {
                CustomAdjustment customAdjustment = new CustomAdjustment(adjustment);
                customAdjustment.setAdjustmentType(adjustmentTypeService.getByAdjustmentCode(adjustment.getCode()));
                customAdjustment.setAdjustmentProfiles(adjustmentProfileService.getByAdjustmentId(adjustment.getId()));
                customAdjustments.add(customAdjustment);
            }
        }
        return customAdjustments;
    }

    /**
     * This Method maps AdjustmentInterface into CustomAdjustments
     * @param adjustment
     * @return CustomAdjustment
     */
    public CustomAdjustment prepareCustomAdjustment(AdjustmentInterface adjustment){
        CustomAdjustment customAdjustment = null;
        if(adjustment != null){
            customAdjustment = new CustomAdjustment(adjustment);
            customAdjustment.setAdjustmentType(adjustmentTypeService.getByAdjustmentCode(adjustment.getCode()));
            customAdjustment.setAdjustmentProfiles(adjustmentProfileService.getByAdjustmentId(adjustment.getId()));
        }
        return customAdjustment;
    }

    public List<CustomAdjustment> getByConsumerNoAndApprovalStatus(String consumerNo, String approvalStatus, ErrorMessage errorMessage) {
        String methodName = "getByConsumerNoAndApprovalStatus() : ";
        logger.info(methodName + "called ");

        if (consumerNo == null || approvalStatus == null) {
            errorMessage.setErrorMessage(methodName + "consumer no or status passed is Null");
            return null;
        }

        List<? extends AdjustmentInterface> adjustments = adjustmentDAO.getByConsumerNoAndApprovalStatus(consumerNo,approvalStatus);
        if(adjustments == null || adjustments.size() <= 0){
            errorMessage.setErrorMessage(methodName + "No adjustment found");
            return null;
        }

       List<CustomAdjustment> customAdjustments = prepareCustomAdjustments(adjustments);
        if(customAdjustments == null || customAdjustments.size() != adjustments.size()){
            errorMessage.setErrorMessage(methodName + "error in retrieving custom adjustment");
            return null;
        }
        return customAdjustments;
    }
}
