package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.AdjustmentInterface;
import com.mppkvvcl.ngbinterface.interfaces.AdjustmentTypeInterface;
import com.mppkvvcl.ngbentity.beans.Adjustment;
import com.mppkvvcl.ngbentity.beans.AdjustmentType;
import com.mppkvvcl.ngbdao.daos.AdjustmentTypeDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by SHIVANSHU on 14-07-2017.
 */
@Service
public class AdjustmentTypeService {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    Logger logger = GlobalResources.getLogger(AdjustmentTypeService.class);

    /**
     * Asking spring to inject dependency on AdjustmentTypeRepository.
     * So that we can use it to perform various operation through repo.
     */
    @Autowired
    AdjustmentTypeDAO adjustmentTypeDAO;


    public List<? extends AdjustmentTypeInterface> getAllTypeAdjustment(){
        String methodName = "getAllTypeAdjustment() : ";
        logger.info( methodName + "called");
        List<? extends AdjustmentTypeInterface> adjustmentTypes = null;
        adjustmentTypes = adjustmentTypeDAO.getAll();
        return adjustmentTypes;
    }

    public List<AdjustmentTypeInterface> getManualTypeAdjustment(){
        String methodName = "getManualTypeAdjustment() : ";
        logger.info( methodName + "called");
        List<AdjustmentTypeInterface> adjustmentTypes = null;
        adjustmentTypes = adjustmentTypeDAO.getAllByView(AdjustmentTypeInterface.MANUAL_ADJUSTMENT_TRUE);
        return adjustmentTypes;
    }

    public AdjustmentTypeInterface getByAdjustmentCode(int code){
        String methodName = "getByAdjustmentCode() : ";
        logger.info(methodName+"called");
        return adjustmentTypeDAO.getByCode(code);
    }
}
