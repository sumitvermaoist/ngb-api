package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbdao.daos.BPLConnectionMappingDAO;
import com.mppkvvcl.ngbinterface.interfaces.BPLConnectionMappingInterface;
import com.mppkvvcl.ngbinterface.interfaces.NSCStagingStatusInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by SUMIT on 14-06-2017.
 */
@Service
public class BPLConnectionMappingService {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(BPLConnectionMappingService.class);
    /**
     * Asking Spring to inject BPLConnectionMappingRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on NSCStaging table
     * at the backend Database
     */
    @Autowired
    private BPLConnectionMappingDAO bplConnectionMappingDAO;

    @Autowired
    private NSCStagingStatusService nscStagingStatusService;

    @Autowired
    private GlobalResources globalResources;

    /**
     * This method takes bplConnectingMapping object and insert it into backend database.
     * Return bplConnectingMapping object if successful else return null.<br><br>
     * param bplConnectionMapping<br>
     * return insertedBplConnectionMapping
     */
    public BPLConnectionMappingInterface insert(BPLConnectionMappingInterface bplConnectionMapping){
        String methodName = "insert : " ;
        BPLConnectionMappingInterface insertedBplConnectionMapping = null;
        logger.info(methodName + "Started insertion for BPLConnectionMapping ");
        if(bplConnectionMapping != null){
            logger.info(methodName + "Calling BPLConnectionMappingRepository for inserting BPLConnectionMapping");
            setAuditDetails(bplConnectionMapping);
            insertedBplConnectionMapping = bplConnectionMappingDAO.add(bplConnectionMapping);
            if (insertedBplConnectionMapping != null) {
                logger.info(methodName + "Successfully inserted BPLConnection Mappping");
            }else{
                logger.error(methodName + "unable to insert bpl connection mapping row. Repository sending null");
            }
        }else{
            logger.error(methodName + "bplConnectionMapping received is null .");
        }
        return insertedBplConnectionMapping ;
    }

    public List<BPLConnectionMappingInterface> getByConsumerNoAndStatus(String consumerNo, String status){
        String methodName = "getByConsumerNoAndStatus() : ";
        logger.info(methodName + "called");
        List<BPLConnectionMappingInterface> bplConnectionMappingInterfaces = null;
        if(consumerNo != null && status != null ){
            bplConnectionMappingInterfaces = bplConnectionMappingDAO.getByConsumerNoAndStatus(consumerNo,status);
        }
        return  bplConnectionMappingInterfaces;
    }

    public List<BPLConnectionMappingInterface> getByBplNoAndStatus(String bplNo, String status){
        String methodName = "getByBplNoAndStatus() : ";
        logger.info(methodName + "called");
        List<BPLConnectionMappingInterface> bplConnectionMappingInterfaces = null;
        if(bplNo != null && status != null ){
            bplConnectionMappingInterfaces = bplConnectionMappingDAO.getByBplNoAndStatus(bplNo,status);
        }
        return  bplConnectionMappingInterfaces;
    }

    public boolean checkBPLNoUsed(String bplNo, String status) {
        String methodName = "checkBPLNoUsed() : ";
        logger.info(methodName + "called " + bplNo + " status " + status);
        boolean isUsed = false;
        if (bplNo != null && status != null) {
            List<BPLConnectionMappingInterface> bplConnectionMappingInterfaces = getByBplNoAndStatus(bplNo, status);
            if (bplConnectionMappingInterfaces != null && bplConnectionMappingInterfaces.size() > 0) {
                isUsed = true;
            }
            NSCStagingStatusInterface nscStagingStatusInterface = nscStagingStatusService.getByPendingStatusAndBPlNo("PENDING", bplNo);
            if (nscStagingStatusInterface != null) {
                isUsed = true;
            }
        }
        return isUsed;
    }

    public BPLConnectionMappingInterface update(BPLConnectionMappingInterface bplConnectionMappingInterface) {
        String methodName = "update() : ";
        logger.info(methodName + "called ");
        BPLConnectionMappingInterface updatedBPLMappingInterface = null;
        if (bplConnectionMappingInterface != null) {
            setUpdateAuditDetails(bplConnectionMappingInterface);
            updatedBPLMappingInterface = bplConnectionMappingDAO.update(bplConnectionMappingInterface);
        }
        return updatedBPLMappingInterface;
    }

    private void setAuditDetails(BPLConnectionMappingInterface bplConnectionMappingInterface) {
        final String methodName = "setAuditDetails() : ";
        logger.info(methodName + "called");
        if (bplConnectionMappingInterface != null) {
            String user = GlobalResources.getLoggedInUser();
            bplConnectionMappingInterface.setCreatedBy(user + " " + globalResources.getProjectDetail());
            bplConnectionMappingInterface.setCreatedOn(GlobalResources.getCurrentDate());
            bplConnectionMappingInterface.setUpdatedBy(user + " " + globalResources.getProjectDetail());
            bplConnectionMappingInterface.setUpdatedOn(GlobalResources.getCurrentDate());
        }
    }

    private void setUpdateAuditDetails(BPLConnectionMappingInterface bplConnectionMappingInterface) {
        final String methodName = "setUpdateAuditDetails() : ";
        logger.info(methodName + "called");
        if (bplConnectionMappingInterface != null) {
            String user = GlobalResources.getLoggedInUser();
            bplConnectionMappingInterface.setUpdatedBy(user + " " + globalResources.getProjectDetail());
            bplConnectionMappingInterface.setUpdatedOn(GlobalResources.getCurrentDate());
        }
    }
}
