package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbdao.daos.BillCalculationLineDAO;
import com.mppkvvcl.ngbinterface.interfaces.BillCalculationLineInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BillCalculationLineService {

    private static final Logger logger = GlobalResources.getLogger(BillCalculationLineService.class);

    @Autowired
    private BillCalculationLineDAO billCalculationLineDAO;

    public List<BillCalculationLineInterface> getByBillId(long billId){
        final String methodName = "getByBillId() : ";
        logger.info(methodName + "called");
        List<BillCalculationLineInterface> billCalculationLineInterfaces = billCalculationLineDAO.getByBillId(billId);
        return billCalculationLineInterfaces;
    }
}
