package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.factories.BillCorrectionFactory;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbdao.daos.BillCorrectionDAO;
import com.mppkvvcl.ngbinterface.interfaces.BillCorrectionInterface;
import com.mppkvvcl.ngbinterface.interfaces.BillInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class BillCorrectionService {

    private static final Logger logger = GlobalResources.getLogger(BillCorrectionService.class);

    @Autowired
    private BillCorrectionDAO billCorrectionDAO;

    @Autowired
    private BillService billService;

    @Autowired
    private GlobalResources globalResources;

    public List<BillCorrectionInterface> getByConsumerNoAndBillMonthAndDeleted(String consumerNo, String billMonth,boolean deletedFlag) {
        String methodName = "getByConsumerNoAndBillMonthAndDeleted () :";
        logger.info(methodName + "called");
        List<BillCorrectionInterface> billCorrectionInterfaces = null;
        if (consumerNo != null && billMonth != null) {
            billCorrectionInterfaces = billCorrectionDAO.getByConsumerNoAndBillMonthAndDeleted(consumerNo, billMonth, deletedFlag);
        }
        return billCorrectionInterfaces;
    }

    public BillCorrectionInterface calculateBillDifferenceComponent(BillInterface originalBillInterface, BillInterface correctedBillInterface) {
        String methodName = "calculateBillDifferenceComponent () : ";
        logger.info(methodName + "called");
        if (originalBillInterface == null || correctedBillInterface == null) {
            logger.error("given input is null");
            return null;
        }
        // declaring all variable used in BillCorrectionInterface
        long billID;
        long referenceBillId;
        String locationCode = null;
        String groupNo = null;
        String readingDiaryNo = null;
        String consumerNo = null;
        String billMonth = null;
        String billTypeCode = null;
        Date billDate = null;
        Date dueDate = null;
        Date chequeDueDate = null;
        Date currentReadDate = null;
        BigDecimal currentRead = null;
        BigDecimal previousRead = null;
        BigDecimal difference = null;
        BigDecimal mf = null;
        BigDecimal assessment = null;
        BigDecimal differenceMeteredUnit = null;
        BigDecimal differenceTotalUnit = null;
        BigDecimal differenceGMCUnit = null;
        BigDecimal differenceBilledUnit = null;
        BigDecimal differenceBilledMD = null;
        BigDecimal differenceBilledPF = null;
        BigDecimal differenceLoadFactor = null;
        BigDecimal differenceFixedCharge = null;
        BigDecimal differenceAdditionalFixedCharges1 = null;
        BigDecimal differenceAdditionalFixedCharges2 = null;
        BigDecimal differenceEnergyCharge = null;
        BigDecimal differenceFCACharge = null;
        BigDecimal differenceElectricityDuty = null;
        BigDecimal differenceMeterRent = null;
        BigDecimal differencePFCharge = null;
        BigDecimal differenceWeldingTransformerSurcharge = null;
        BigDecimal differenceLoadFactorIncentive = null;
        BigDecimal differenceSDInterest = null;
        BigDecimal differenceCCBAdjustment = null;
        BigDecimal differenceLockCredit = null;
        BigDecimal differenceOtherAdjustment = null;
        BigDecimal differenceEmployeeRebate = null;
        BigDecimal differenceOnlinePaymentRebate = null;
        BigDecimal differencePrepaidMeterRebate = null;
        BigDecimal differenceBillPromptPaymentIncentive = null;
        BigDecimal differenceAdvancePaymentIncentive = null;
        BigDecimal differenceSubsidy = null;
        BigDecimal differenceDemandSideIncentive = null;
        BigDecimal differenceCurrentBillAmount = null;
        BigDecimal differenceArrear = null;
        BigDecimal differenceCumulativeSurcharge = null;
        BigDecimal differenceSurchargeDemanded = null;
        BigDecimal differenceNetBillAmount = null;
        BigDecimal differenceAsdBilled = null;
        BigDecimal differenceAsdArrear = null;
        BigDecimal differencePristineElectricityDuty = null;
        BigDecimal differenceAsdInstallment = null;
        BigDecimal differencePristineNetBill = null;
        BigDecimal differenceCurrentSurchargeAmount = null;
        BigDecimal differenceXrayFixedCharge = null;
        Boolean isDeleted = null;
        Boolean usedOnMis = null;
        String createdBy = null;
        Date createdOn = null;
        String updatedBy = null;
        Date updatedOn = null;
        BillCorrectionInterface billCorrectionInterface = null;

        billID = correctedBillInterface.getId();
        referenceBillId = originalBillInterface.getId();


        if (correctedBillInterface.getLocationCode() != null) {
            locationCode = correctedBillInterface.getLocationCode();
        }

        if (correctedBillInterface.getGroupNo() != null) {
            groupNo = correctedBillInterface.getGroupNo();
        }

        if (correctedBillInterface.getReadingDiaryNo() != null) {
            readingDiaryNo = correctedBillInterface.getReadingDiaryNo();
        }

        if (correctedBillInterface.getConsumerNo() != null) {
            consumerNo = correctedBillInterface.getConsumerNo();
        }

        if (correctedBillInterface.getBillMonth() != null) {
            billMonth = correctedBillInterface.getBillMonth();

        }

        if (correctedBillInterface.getBillTypeCode() != null) {
            billTypeCode = correctedBillInterface.getBillTypeCode();
        }
        if (correctedBillInterface.getBillDate() != null) {
            billDate = correctedBillInterface.getBillDate();
        }

        if (correctedBillInterface.getDueDate() != null) {
            dueDate = correctedBillInterface.getDueDate();
        }

        if (correctedBillInterface.getChequeDueDate() != null) {
            chequeDueDate = correctedBillInterface.getChequeDueDate();
        }

        if (correctedBillInterface.getCurrentReadDate() != null) {
            currentReadDate = correctedBillInterface.getCurrentReadDate();
        }

        if (correctedBillInterface.getCurrentRead() != null) {
            currentRead = correctedBillInterface.getCurrentRead();
        }

        if (correctedBillInterface.getPreviousRead() != null) {
            previousRead = correctedBillInterface.getPreviousRead();
        }

        if (correctedBillInterface.getDifference() != null) {
            difference = correctedBillInterface.getDifference();
        }
        if (correctedBillInterface.getMf() != null) {
            mf = correctedBillInterface.getMf();
        }

        BigDecimal originalBillMeteredUnit = originalBillInterface.getMeteredUnit();
        BigDecimal correctedBillMeteredUnit = correctedBillInterface.getMeteredUnit();
        if (originalBillMeteredUnit != null && correctedBillMeteredUnit != null) {
            differenceMeteredUnit = correctedBillMeteredUnit.subtract(originalBillMeteredUnit);
        }

        if (correctedBillInterface.getAssessment() != null) {
            assessment = correctedBillInterface.getAssessment();
        }

        BigDecimal originalBillTotalUnit = originalBillInterface.getTotalUnit();
        BigDecimal correctedBillTotalUnit = correctedBillInterface.getTotalUnit();
        if (originalBillTotalUnit != null && correctedBillTotalUnit != null) {
            differenceTotalUnit = correctedBillTotalUnit.subtract(originalBillTotalUnit);
        }

        BigDecimal originalBillGMCUnit = originalBillInterface.getGmcUnit();
        BigDecimal correctedBillGMCUnit = correctedBillInterface.getGmcUnit();
        if (originalBillGMCUnit != null && correctedBillGMCUnit != null) {
            differenceGMCUnit = correctedBillGMCUnit.subtract(originalBillGMCUnit);
        }

        BigDecimal originalBillBilledUnit = originalBillInterface.getBilledUnit();
        BigDecimal correctedBillBilledUnit = correctedBillInterface.getBilledUnit();
        if (originalBillBilledUnit != null && correctedBillBilledUnit != null) {
            differenceBilledUnit = correctedBillBilledUnit.subtract(originalBillBilledUnit);
        }

        BigDecimal originalBillBilledMD = originalBillInterface.getBilledMD();
        BigDecimal correctedBillBilledMD = correctedBillInterface.getBilledMD();
        if (originalBillBilledMD != null && correctedBillBilledMD != null) {
            differenceBilledMD = correctedBillBilledMD.subtract(originalBillBilledMD);
        }

        BigDecimal originalBilledPF = originalBillInterface.getBilledPF();
        BigDecimal correctedBilledPF = correctedBillInterface.getBilledPF();
        if (originalBilledPF != null && correctedBilledPF != null) {
            differenceBilledPF = correctedBilledPF.subtract(originalBilledPF);
        }

        BigDecimal originalBillLoadFactor = originalBillInterface.getLoadFactor();
        BigDecimal correctedBillLoadFactor = correctedBillInterface.getLoadFactor();
        if (originalBillLoadFactor != null && correctedBillLoadFactor != null) {
            differenceLoadFactor = correctedBillLoadFactor.subtract(originalBillLoadFactor);
        }

        BigDecimal originalBillFixedCharge = originalBillInterface.getFixedCharge();
        BigDecimal correctedBillFixedCharge = correctedBillInterface.getFixedCharge();
        if (originalBillFixedCharge != null && correctedBillFixedCharge != null) {
            differenceFixedCharge = correctedBillFixedCharge.subtract(originalBillFixedCharge);
        }

        BigDecimal originalBillAdditionalFixedCharges1 = originalBillInterface.getAdditionalFixedCharges1();
        BigDecimal correctedBillAdditionalFixedCharges1 = correctedBillInterface.getAdditionalFixedCharges1();
        if (originalBillAdditionalFixedCharges1 != null && correctedBillAdditionalFixedCharges1 != null) {
            differenceAdditionalFixedCharges1 = correctedBillAdditionalFixedCharges1.subtract(originalBillAdditionalFixedCharges1);
        }

        BigDecimal originalBillAdditionalFixedCharges2 = originalBillInterface.getAdditionalFixedCharges2();
        BigDecimal correctedBillAdditionalFixedCharges2 = correctedBillInterface.getAdditionalFixedCharges2();
        if (originalBillAdditionalFixedCharges2 != null && correctedBillAdditionalFixedCharges2 != null) {
            differenceAdditionalFixedCharges2 = correctedBillAdditionalFixedCharges2.subtract(originalBillAdditionalFixedCharges2);
        }

        BigDecimal originalBillEnergyCharge = originalBillInterface.getEnergyCharge();
        BigDecimal correctedBillEnergyCharge = correctedBillInterface.getEnergyCharge();
        if (originalBillEnergyCharge != null && correctedBillEnergyCharge != null) {
            differenceEnergyCharge = correctedBillEnergyCharge.subtract(originalBillEnergyCharge);
        }

        BigDecimal originalBillFCACharge = originalBillInterface.getFcaCharge();
        BigDecimal correctedBillFCACharge = correctedBillInterface.getFcaCharge();
        if (originalBillFCACharge != null && correctedBillFCACharge != null) {
            differenceFCACharge = correctedBillFCACharge.subtract(originalBillFCACharge);
        }

        BigDecimal originalBillElectricityDuty = originalBillInterface.getElectricityDuty();
        BigDecimal correctedBillElectricityDuty = correctedBillInterface.getElectricityDuty();
        if (originalBillElectricityDuty != null && correctedBillElectricityDuty != null) {
            differenceElectricityDuty = correctedBillElectricityDuty.subtract(originalBillElectricityDuty);
        }

        BigDecimal originalBillMeterRent = originalBillInterface.getMeterRent();
        BigDecimal correctedBillMeterRent = correctedBillInterface.getMeterRent();
        if (originalBillMeterRent != null && correctedBillMeterRent != null) {
            differenceMeterRent = correctedBillMeterRent.subtract(originalBillMeterRent);
        }

        BigDecimal originalBillPFCharge = originalBillInterface.getPfCharge();
        BigDecimal correctedBillPFCharge = correctedBillInterface.getPfCharge();
        if (originalBillPFCharge != null && correctedBillPFCharge != null) {
            differencePFCharge = correctedBillPFCharge.subtract(originalBillPFCharge);
        }

        BigDecimal originalBillWeldingTransformerSurcharge = originalBillInterface.getWeldingTransformerSurcharge();
        BigDecimal correctedBillWeldingTransformerSurcharge = correctedBillInterface.getWeldingTransformerSurcharge();
        if (originalBillWeldingTransformerSurcharge != null && correctedBillWeldingTransformerSurcharge != null) {
            differenceWeldingTransformerSurcharge = correctedBillWeldingTransformerSurcharge.subtract(originalBillWeldingTransformerSurcharge);
        }

        BigDecimal originalBillLoadFactorIncentive = originalBillInterface.getLoadFactor();
        BigDecimal correctedBillLoadFactorIncentive = correctedBillInterface.getLoadFactorIncentive();
        if (originalBillLoadFactorIncentive != null && correctedBillLoadFactorIncentive != null) {
            differenceLoadFactorIncentive = correctedBillLoadFactorIncentive.subtract(originalBillLoadFactorIncentive);
        }

        BigDecimal originalBillSDInterest = originalBillInterface.getSdInterest();
        BigDecimal correctedBillSDInterest = correctedBillInterface.getSdInterest();
        if (originalBillSDInterest != null && correctedBillSDInterest != null) {
            differenceSDInterest = correctedBillSDInterest.subtract(originalBillSDInterest);
        }
        BigDecimal originalBillCCBAdjustment = originalBillInterface.getCcbAdjustment();
        BigDecimal correctedBillCCBAdjustment = correctedBillInterface.getCcbAdjustment();
        if (originalBillCCBAdjustment != null && correctedBillCCBAdjustment != null) {
            differenceCCBAdjustment = correctedBillCCBAdjustment.subtract(originalBillCCBAdjustment);
        }

        BigDecimal originalBillLockCredit = originalBillInterface.getLockCredit();
        BigDecimal correctedBillLockCredit = correctedBillInterface.getLockCredit();
        if (originalBillLockCredit != null && correctedBillLockCredit != null) {
            differenceLockCredit = correctedBillLockCredit.subtract(originalBillLockCredit);
        }

        BigDecimal originalBillOtherAdjustment = originalBillInterface.getOtherAdjustment();
        BigDecimal correctedBillOtherAdjustment = correctedBillInterface.getOtherAdjustment();
        if (originalBillOtherAdjustment != null && correctedBillOtherAdjustment != null) {
            differenceOtherAdjustment = correctedBillOtherAdjustment.subtract(originalBillOtherAdjustment);
        }

        BigDecimal originalBillEmployeeRebate = originalBillInterface.getEmployeeRebate();
        BigDecimal correctedBillEmployeeRebate = correctedBillInterface.getEmployeeRebate();
        if (originalBillEmployeeRebate != null && correctedBillEmployeeRebate != null) {
            differenceEmployeeRebate = correctedBillEmployeeRebate.subtract(originalBillEmployeeRebate);
        }

        BigDecimal originalBillOnlinePaymentRebate = originalBillInterface.getOnlinePaymentRebate();
        BigDecimal correctedBillOnlinePaymentRebate = correctedBillInterface.getOnlinePaymentRebate();
        if (originalBillOnlinePaymentRebate != null && correctedBillOnlinePaymentRebate != null) {
            differenceOnlinePaymentRebate = correctedBillOnlinePaymentRebate.subtract(originalBillOnlinePaymentRebate);
        }

        BigDecimal originalBillPrepaidMeterRebate = originalBillInterface.getPrepaidMeterRebate();
        BigDecimal correctedBillPrepaidMeterRebate = correctedBillInterface.getPrepaidMeterRebate();
        if (originalBillPrepaidMeterRebate != null && correctedBillPrepaidMeterRebate != null) {
            differencePrepaidMeterRebate = correctedBillPrepaidMeterRebate.subtract(originalBillPrepaidMeterRebate);
        }

        BigDecimal originalBillPromptPaymentIncentive = originalBillInterface.getPromptPaymentIncentive();
        BigDecimal correctedBillPromptPaymentIncentive = correctedBillInterface.getPromptPaymentIncentive();
        if (originalBillPromptPaymentIncentive != null && correctedBillPromptPaymentIncentive != null) {
            differenceBillPromptPaymentIncentive = correctedBillPromptPaymentIncentive.subtract(originalBillPromptPaymentIncentive);
        }

        BigDecimal originalBillAdvancePaymentIncentive = originalBillInterface.getAdvancePaymentIncentive();
        BigDecimal correctedBillAdvancePaymentIncentive = correctedBillInterface.getAdvancePaymentIncentive();
        if (correctedBillAdvancePaymentIncentive != null && originalBillAdvancePaymentIncentive != null) {
            differenceAdvancePaymentIncentive = correctedBillAdvancePaymentIncentive.subtract(originalBillAdvancePaymentIncentive);
        }

        BigDecimal originalBillDemandSideIncentive = originalBillInterface.getDemandSideIncentive();
        BigDecimal correctedBillDemandSideIncentive = correctedBillInterface.getDemandSideIncentive();
        if (correctedBillDemandSideIncentive != null && originalBillDemandSideIncentive != null) {
            differenceDemandSideIncentive = correctedBillDemandSideIncentive.subtract(originalBillDemandSideIncentive);
        }

        BigDecimal originalBillSubsidy = originalBillInterface.getSubsidy();
        BigDecimal correctedBillSubsidy = correctedBillInterface.getSubsidy();
        if (originalBillSubsidy != null && correctedBillSubsidy != null) {
            differenceSubsidy = correctedBillSubsidy.subtract(originalBillSubsidy);
        }

        BigDecimal originalBillCurrentAmount = originalBillInterface.getCurrentBill();
        BigDecimal correctedBillCurrentAmount = correctedBillInterface.getCurrentBill();
        if (originalBillCurrentAmount != null && correctedBillCurrentAmount != null) {
            differenceCurrentBillAmount = correctedBillCurrentAmount.subtract(originalBillCurrentAmount);
        }

        BigDecimal originalBillArrear = originalBillInterface.getArrear();
        BigDecimal correctedBillArrear = correctedBillInterface.getArrear();
        if (originalBillArrear != null && correctedBillArrear != null) {
            differenceArrear = correctedBillArrear.subtract(originalBillArrear);
        }

        BigDecimal originalBillCumulativeSurcharge = originalBillInterface.getCumulativeSurcharge();
        BigDecimal correctedBillCumulativeSurcharge = correctedBillInterface.getCumulativeSurcharge();
        if (originalBillCumulativeSurcharge != null && correctedBillCumulativeSurcharge != null) {
            differenceCumulativeSurcharge = correctedBillCumulativeSurcharge.subtract(originalBillCumulativeSurcharge);
        }

        BigDecimal originalBillSurchargeDemanded = originalBillInterface.getSurchargeDemanded();
        BigDecimal correctedBillSurchargeDemanded = correctedBillInterface.getSurchargeDemanded();
        if (originalBillSurchargeDemanded != null && correctedBillSurchargeDemanded != null) {
            differenceSurchargeDemanded = correctedBillSurchargeDemanded.subtract(originalBillSurchargeDemanded);
        }

        BigDecimal originalBillNetAmount = originalBillInterface.getNetBill();
        BigDecimal correctedBillNetBillAmount = correctedBillInterface.getNetBill();
        if (originalBillNetAmount != null && correctedBillNetBillAmount != null) {
            differenceNetBillAmount = correctedBillNetBillAmount.subtract(originalBillNetAmount);
        }

        BigDecimal originalBillAsdBilled = originalBillInterface.getAsdBilled();
        BigDecimal correctedBillAsdBilled = correctedBillInterface.getAsdBilled();
        if (originalBillAsdBilled != null && correctedBillAsdBilled != null) {
            differenceAsdBilled = correctedBillAsdBilled.subtract(originalBillAsdBilled);
        }

        BigDecimal originalBillAsdArrear = originalBillInterface.getAsdArrear();
        BigDecimal correctedBillAsdArrear = correctedBillInterface.getAsdArrear();
        if (originalBillAsdArrear != null && correctedBillAsdArrear != null) {
            differenceAsdArrear = correctedBillAsdArrear.subtract(originalBillAsdArrear);
        }

        BigDecimal originalBillAsdInstallment = originalBillInterface.getAsdInstallment();
        BigDecimal correctedBillAsdInstallment = correctedBillInterface.getAsdInstallment();
        if (correctedBillAsdInstallment != null && originalBillAsdInstallment != null) {
            differenceAsdInstallment = correctedBillAsdInstallment.subtract(originalBillAsdInstallment);
        }

        BigDecimal originalBillPristineElectricityDuty = originalBillInterface.getPristineElectricityDuty();
        BigDecimal correctedBillPristineElectricityDuty = correctedBillInterface.getPristineElectricityDuty();
        if (originalBillPristineElectricityDuty != null && correctedBillPristineElectricityDuty != null) {
            differencePristineElectricityDuty = correctedBillPristineElectricityDuty.subtract(originalBillPristineElectricityDuty);
        }

        BigDecimal originalBillPristineNetBill = originalBillInterface.getPristineNetBill();
        BigDecimal correctedBillPristineNetBill = correctedBillInterface.getPristineNetBill();
        if (originalBillPristineNetBill != null && correctedBillPristineNetBill != null) {
            differencePristineNetBill = correctedBillPristineNetBill.subtract(originalBillPristineNetBill);
        }

        BigDecimal originalBillCurrentSurchargeAmount = originalBillInterface.getCurrentBillSurcharge();
        BigDecimal correctedBillCurrentSurchargeAmount = correctedBillInterface.getCurrentBillSurcharge();
        if (originalBillCurrentSurchargeAmount != null && correctedBillCurrentSurchargeAmount != null) {
            differenceCurrentSurchargeAmount = correctedBillCurrentSurchargeAmount.subtract(originalBillCurrentSurchargeAmount);
        }

        BigDecimal originalBillXrayFixedCharge = originalBillInterface.getXrayFixedCharge();
        BigDecimal correctedBillXrayFixedCharge = correctedBillInterface.getXrayFixedCharge();
        if (originalBillXrayFixedCharge != null && correctedBillXrayFixedCharge != null) {
            differenceXrayFixedCharge = correctedBillXrayFixedCharge.subtract(originalBillXrayFixedCharge);
        }
        if (consumerNo != null) {
            setAuditDetails(billCorrectionInterface);
            usedOnMis = false;
            isDeleted = false;
        }

        billCorrectionInterface = BillCorrectionFactory.build(billID, referenceBillId, locationCode, groupNo, readingDiaryNo, consumerNo, billMonth, billTypeCode, billDate, dueDate, chequeDueDate, currentReadDate, currentRead, previousRead, difference, mf, differenceMeteredUnit, assessment, differenceTotalUnit, differenceGMCUnit, differenceBilledUnit, differenceBilledMD, differenceBilledPF, differenceLoadFactor, differenceFixedCharge, differenceAdditionalFixedCharges1, differenceAdditionalFixedCharges2, differenceEnergyCharge, differenceFCACharge, differenceElectricityDuty, differenceMeterRent, differencePFCharge, differenceWeldingTransformerSurcharge, differenceLoadFactorIncentive, differenceSDInterest, differenceCCBAdjustment, differenceLockCredit, differenceOtherAdjustment, differenceEmployeeRebate, differenceOnlinePaymentRebate, differencePrepaidMeterRebate, differenceBillPromptPaymentIncentive, differenceAdvancePaymentIncentive, differenceDemandSideIncentive, differenceSubsidy, differenceCurrentBillAmount, differenceArrear, differenceCumulativeSurcharge, differenceSurchargeDemanded, differenceNetBillAmount, differenceAsdBilled, differenceAsdArrear, differenceAsdInstallment, isDeleted, createdBy, createdOn, updatedBy, updatedOn, differencePristineElectricityDuty, differencePristineNetBill, differenceCurrentSurchargeAmount, differenceXrayFixedCharge, usedOnMis);  //todo use factory.
        return billCorrectionInterface;
    }

    public BillCorrectionInterface insert(BillCorrectionInterface billCorrectionInterfaceToAdd)  {
        String methodName = "insert() :";
        logger.info(methodName + "called");
        BillCorrectionInterface insertedBillCorrectionInterface = null;
        if(billCorrectionInterfaceToAdd != null) {
            setAuditDetails(billCorrectionInterfaceToAdd);
            insertedBillCorrectionInterface = billCorrectionDAO.add(billCorrectionInterfaceToAdd);
        }
        return insertedBillCorrectionInterface;
    }

    public BillCorrectionInterface update(BillCorrectionInterface billCorrectionInterfaceToUpdate)  {
        String methodName = "update() : ";
        logger.info(methodName + "called");
        BillCorrectionInterface updatedBillCorrectionInterface = null;
        if(billCorrectionInterfaceToUpdate != null) {
            setUpdateAuditDetails(billCorrectionInterfaceToUpdate);
            updatedBillCorrectionInterface = billCorrectionDAO.update(billCorrectionInterfaceToUpdate);
        }
        return updatedBillCorrectionInterface;
    }

    private void setAuditDetails(BillCorrectionInterface billCorrectionInterface) {
        final String methodName = "setAuditDetails() : ";
        logger.info(methodName + "called");
        if (billCorrectionInterface != null) {
            String user = GlobalResources.getLoggedInUser();
            billCorrectionInterface.setCreatedBy(user + " " + globalResources.getProjectDetail());
            billCorrectionInterface.setCreatedOn(GlobalResources.getCurrentDate());
            billCorrectionInterface.setUpdatedBy(user + " " + globalResources.getProjectDetail());
            billCorrectionInterface.setUpdatedOn(GlobalResources.getCurrentDate());
        }
    }

    private void setUpdateAuditDetails(BillCorrectionInterface billCorrectionInterface) {
        final String methodName = "setUpdateAuditDetails() : ";
        logger.info(methodName + "called");
        if (billCorrectionInterface != null) {
            String user = GlobalResources.getLoggedInUser();
            billCorrectionInterface.setUpdatedBy(user + " " + globalResources.getProjectDetail());
            billCorrectionInterface.setUpdatedOn(GlobalResources.getCurrentDate());
        }
    }
}
