package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.CircleInterface;
import com.mppkvvcl.ngbdao.daos.CircleDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by MITHLESH on 18-07-2017.
 */
@Service
public class CircleService {

    /**
     * Asking Spring to inject CircleRepository so that we can use it in various method
     */
    @Autowired
    CircleDAO circleDAO;
    /**
     * This Logger Object is used to logging in current class
     */
    Logger logger = GlobalResources.getLogger(CircleService.class);

    /**
     * This getAll() method is used to fetch List of Circle from circle table<br><br>
     * return list of circle
     */
    public List<? extends CircleInterface> getAll() {
        String methodName = "getAll() : ";
        logger.info(methodName + "Request receive to fetch List<Circle> from Circle");
        List<? extends CircleInterface> circles = circleDAO.getAll();
        if(circles != null){
            if(circles.size() > 0){
                logger.info(methodName + "Successfully got List<Circle> with size: "+circles.size());
            }else{
                logger.error(methodName + "Successfully got List<Circle> with size: "+circles.size());
            }
        }else{
            logger.error(methodName + "Fetched List<Circle> is null");
        }
        return circles;
    }
}
