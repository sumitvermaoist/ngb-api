package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerNoGeneratorInterface;
import com.mppkvvcl.ngbdao.daos.ConsumerNoGeneratorDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by SUMIT on 20-06-2017.
 */
@Service
public class ConsumerNoGeneratorService {
    /**
     * Asking Spring to inject ConsumerNoGeneratorRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on  consumer_no_generator table
     * at the backend Database
     */
    @Autowired
    private ConsumerNoGeneratorDAO consumerNoGeneratorDAO;

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ConsumerNoGeneratorService.class);

    /**
     * This method takes location code and fetch consumerNoGenerator object from backend database.
     * Return consumerNoGenerator if found else return null.<br><br>
     * param locationCode<br>
     * return consumerNoGenerator<br>
     */
    public ConsumerNoGeneratorInterface getByLocationCode(String locationCode){
        String methodName = " getByLocationCode() : ";
        ConsumerNoGeneratorInterface consumerNoGenerator = null;
        logger.info(methodName + "Got request to get ConsumerNoGenerator with input parameter as : " + locationCode);
        if (locationCode != null){
            logger.info(methodName + "Calling ConsumerNoGeneratorRepository for getting consumerNoGenerator ");
            consumerNoGenerator = consumerNoGeneratorDAO.getByLocationCode(locationCode);
            if (consumerNoGenerator != null){
                logger.info(methodName + "Successfully got one row for ConsumerNoGenerator");
            }else {
                logger.error(methodName + "Unable to get ConsumerNoGenerator. ConsumerNoGeneratorRepository returning null");
            }
        }else {
            logger.error(methodName + "Received ConsumerNoGenerator object is null ");
        }
        return consumerNoGenerator;
    }
}
