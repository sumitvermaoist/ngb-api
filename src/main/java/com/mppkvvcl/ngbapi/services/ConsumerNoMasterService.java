package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.custombeans.GroupConsumerData;
import com.mppkvvcl.ngbapi.custombeans.Read;
import com.mppkvvcl.ngbapi.custombeans.ReadingDiaryConsumerData;
import com.mppkvvcl.ngbapi.custombeans.ZoneConsumerData;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbinterface.interfaces.*;
import com.mppkvvcl.ngbdao.daos.ConsumerNoMasterDAO;
import com.mppkvvcl.ngbentity.beans.Adjustment;
import com.mppkvvcl.ngbentity.beans.ReadMaster;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by SUMIT on 20-06-2017.
 */

@Service
public class ConsumerNoMasterService {
    /**
     * Asking Spring to inject ConsumerNoMasterRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on  table
     * at the backend Database
     */
    @Autowired
    private ConsumerNoMasterDAO consumerNoMasterDAO;

    @Autowired
    private ReadService readService;

    @Autowired
    private TariffDescriptionService tariffDescriptionService;

    @Autowired
    private ConfiguratorService configuratorService;

    @Autowired
    private ReadMasterService readMasterService;

    @Autowired
    private ReadMasterPFService readMasterPFService;

    @Autowired
    private ReadMasterKWService readMasterKWService;

    @Autowired

    private TariffService tariffService;

    @Autowired
    private ConsumerConnectionInformationService consumerConnectionInformationService;

    @Autowired
    private ReadTypeConfiguratorService readTypeConfiguratorService;

    @Autowired
    private MeterMasterService meterMasterService;

    @Autowired
    private ConsumerMeterMappingService consumerMeterMappingService;

    @Autowired
    private  ScheduleService scheduleService;

    @Autowired
    private  AdjustmentService adjustmentService;

    @Autowired
    ConsumerConnectionAreaInformationService consumerConnectionAreaInformationService;

    @Autowired
    private ZoneService zoneService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private ReadingDiaryNoService readingDiaryNoService;

    /**
     /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ConsumerNoMasterService.class);

    /**
     * This method takes oldServiceNoOne from backend database.Return consumerNoMaster if successful else return null.<br><br>
     * param oldServiceNoOne<br>
     * return consumerNoMaster
     */
    public ConsumerNoMasterInterface getByOldServiceNoOne(String oldServiceNoOne){
        String methodName = " getByOldServiceNoOne : ";
        logger.info(methodName + "Started getByOldServiceNoOne");
        ConsumerNoMasterInterface consumerNoMaster = null;
        if(oldServiceNoOne != null){
            logger.info(methodName + " Calling ConsumerNoMasterRepository for getting ConsumerNoMaster ");
            consumerNoMaster = consumerNoMasterDAO.getByOldServiceNoOne(oldServiceNoOne);
            if (consumerNoMaster != null){
                logger.info(methodName + "Successfully got one row for consumerNoMaster");
            }else {
                logger.error(methodName + "Unable to row from ConsumerNoMasterRepository. ConsumerNoMasterRepository returning null");
            }
        }else{
            logger.error(methodName + " Received oldServiceNoOne  is null ");
        }
        return consumerNoMaster;

    }

    /**
     * This method takes oldServiceNoOne and locationCode from backend database.Return consumerNoMaster if successful else return null.<br><br>
     * @param oldServiceNoOne
     * @param locationCode
     * return consumerNoMaster
     */

    public ConsumerNoMasterInterface getByOldServiceNoOneAndLocationCode(String oldServiceNoOne, String locationCode){
        String methodName = " getByOldServiceNoOneAndLocationCode() : ";
        logger.info(methodName + "called");
        ConsumerNoMasterInterface consumerNoMaster = null;
        if(oldServiceNoOne != null && locationCode != null){
            consumerNoMaster = consumerNoMasterDAO.getByOldServiceNoOneAndLocationCode(oldServiceNoOne,locationCode);
        }
        return consumerNoMaster;

    }

    /**
     * added by : Preetesh 21 Aug 2017
     * this method will return the custom bean Read, so that Final Read can be prepared for PDC consumer.
     * before that we check for domestic , single-phase connection, and than difference in connection date and current date.
     * As per Supple Code 2013 --
     * Consumers other than domestic and single-phase non-domestic category can terminate the agreement
     * after the expiry of the initial period of two years.
     * @param consumerNo
     * @return
     * @throws Exception
     */
    public Read getPDCDataForConsumerNo(String consumerNo) throws Exception {
        String methodName = " getPDCDataForConsumerNo : ";
        logger.info(methodName + "Started getPDCDataForConsumerNo");
        ConsumerNoMasterInterface consumerNoMaster = null;
        Read readToReturn = null;
        if(consumerNo == null ) {
            logger.error(methodName + " Received consumer no is null ");
            throw new Exception("consumer no passed is null");
        }

        consumerNo = consumerNo.trim();
        logger.info(methodName + " Calling ConsumerNoMasterRepository for getting ConsumerNoMaster ");
        consumerNoMaster = consumerNoMasterDAO.getByConsumerNo(consumerNo);
        if (consumerNoMaster == null){
            logger.error(methodName + "Unable to fetch row from ConsumerNoMasterRepository");
            throw new Exception("Consumer Not found with consumer no: "+consumerNo);
        }
        logger.info(methodName + "Successfully got one row for consumerNoMaster"+consumerNoMaster);

        String status = consumerNoMaster.getStatus();
        if(status == null ){
            logger.error(methodName + "status found null");
            throw new Exception("status found null");
        }

        if(status.equals(ConsumerNoMasterInterface.STATUS_INACTIVE)){
            logger.error(methodName + "Consumer is not in ACTIVE state, can't process PDC");
            throw new Exception("Consumer is not in ACTIVE state, can't process PDC");
        }

        Read readBeforeCheck = readService.getLatestByConsumerNoForPunchingFinalReading(consumerNo);
        if(readBeforeCheck == null) {
            logger.error(methodName + "some error in fetching CustomRead");
            throw new Exception("some error in fetching CustomRead");
        }

        ConsumerConnectionInformationInterface consumerConnectionInformation = consumerConnectionInformationService.getByConsumerNo(consumerNo);
        if(consumerConnectionInformation == null){
            logger.error(methodName + "consumer connection information not found");
            throw new Exception("consumer connection information not found");
        }

        String tariffCategory = consumerConnectionInformation.getTariffCategory();
        if(tariffCategory == null) {
            logger.error(methodName + "tariffCategory not found");
            throw new Exception("tariffCategory not found");
        }
        TariffDescriptionInterface tariffDescription = tariffDescriptionService.getByTariffCategory(tariffCategory);
        if(tariffDescription == null){
            logger.error(methodName + "tariffDescription not found");
            throw new Exception("tariffDescription not found");
        }
        String tariffDescriptionString = tariffDescription.getTariffDescription();
        if(tariffDescriptionString.equals(TariffDescriptionInterface.TARIFF_DESCRIPTION_DOMESTIC)){
            readToReturn = readBeforeCheck;
        }else{
            String phase = consumerConnectionInformation.getPhase();
            if(phase.equals(ConsumerConnectionInformationInterface.PHASE_SINGLE)){
                readToReturn = readBeforeCheck;
            }else{
                Date dateOfConnection = consumerConnectionInformation.getConnectionDate();
                long connectionAge = GlobalResources.getDateDiffInDays(GlobalResources.getCurrentDate(),dateOfConnection);
                /**
                 * checking if connection is more than 2 years old, i.e.consumer has completed agreement period
                 */
                long agreementPeriodInDays = configuratorService.getValueForCode(ConfiguratorInterface.AGREEMENT_PERIOD);
                if(connectionAge < agreementPeriodInDays){
                    logger.error(methodName + "consumer is under agreement period");
                    throw new Exception("consumer is under agreement period");
                }
                readToReturn = readBeforeCheck;
            }
        }
        return readToReturn;
    }


    /**
     *
     * @param consumerNo
     * @param finalRead
     * @param pdcDateString
     * @param pdcRemark
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public ConsumerNoMasterInterface pdcConsumerAndInsertFinalRead(String consumerNo, ReadMaster finalRead, String pdcDateString, String pdcRemark)throws Exception {
        String methodName = " pdcConsumerAndInsertFinalRead : ";
        logger.info(methodName + "Started PDC check process for ConsumerNo"+consumerNo);
        ConsumerNoMasterInterface consumerNoMaster = null;
        ConsumerNoMasterInterface updatedConsumerNoMaster;

        if(consumerNo == null || pdcDateString == null || pdcRemark == null) {
            logger.error(methodName + " Received consumer no is null ");
            throw new Exception("consumer no passed is null");
        }
        consumerNo = consumerNo.trim();
        Date pdcDate = GlobalResources.getDateFromString(pdcDateString);
        logger.info(methodName + " Calling ConsumerNoMasterRepository for getting ConsumerNoMaster ");

        consumerNoMaster = consumerNoMasterDAO.getByConsumerNo(consumerNo);
        if (consumerNoMaster == null){
            logger.error(methodName + "Unable to fetch row from ConsumerNoMasterRepository");
            throw new Exception("Unable to fetch row from ConsumerNoMasterRepository");
        }
        logger.info(methodName + "Successfully got one row for consumerNoMaster"+consumerNoMaster);

        String status = consumerNoMaster.getStatus();
        if(status == null ){
            logger.error(methodName + "status found null");
            throw new Exception("status found null");
        }
        if(status.equals(ConsumerNoMasterInterface.STATUS_INACTIVE)){
            logger.error(methodName + "Consumer is not in ACTIVE state, cant do anything");
            throw new Exception("Consumer is not in ACTIVE state, cant do anything");
        }

        ConsumerConnectionInformationInterface consumerConnectionInformation = consumerConnectionInformationService.getByConsumerNo(consumerNo);
        if(consumerConnectionInformation == null){
            logger.error(methodName + "consumer connection information not found");
            throw new Exception("consumer connection information not found");
        }
        String tariffCategory = consumerConnectionInformation.getTariffCategory();
        if(tariffCategory == null) {
            logger.error(methodName + "tariffCategory not found");
            throw new Exception("tariffCategory not found");
        }
        TariffDescriptionInterface tariffDescription = tariffDescriptionService.getByTariffCategory(tariffCategory);
        if(tariffDescription == null){
            logger.error(methodName + "tariffDescription not found");
            throw new Exception("tariffDescription not found");
        }
        String tariffDescriptionString = tariffDescription.getTariffDescription();
        if(tariffDescriptionString.equals(TariffDescriptionInterface.TARIFF_DESCRIPTION_DOMESTIC)){
            updatedConsumerNoMaster = processPDC(consumerNo,consumerNoMaster, finalRead, consumerConnectionInformation, pdcDate, pdcRemark);
        }else{
            String phase = consumerConnectionInformation.getPhase();
            if(phase.equals(ConsumerConnectionInformationInterface.PHASE_SINGLE)){
                updatedConsumerNoMaster = processPDC(consumerNo,consumerNoMaster, finalRead,consumerConnectionInformation, pdcDate, pdcRemark);
            }else{
                Date dateOfConnection = consumerConnectionInformation.getConnectionDate();
                long connectionAge = GlobalResources.getDateDiffInDays(GlobalResources.getCurrentDate(),dateOfConnection);
                /**
                 * checking if connection is more than 2 years old, i.e.consumer has completed agreement period
                 */
                long agreementPeriodInDays = configuratorService.getValueForCode(ConfiguratorInterface.AGREEMENT_PERIOD);
                if(connectionAge < agreementPeriodInDays){
                    logger.error(methodName + "consumer is under agreement period");
                    throw new Exception("consumer is under agreement period");
                }
                updatedConsumerNoMaster = processPDC(consumerNo,consumerNoMaster, finalRead, consumerConnectionInformation, pdcDate, pdcRemark);
            }
        }
        return updatedConsumerNoMaster;
    }

    @Transactional(rollbackFor = Exception.class)
    private ConsumerNoMasterInterface processPDC(String consumerNo, ConsumerNoMasterInterface consumerNoMaster, ReadMaster finalRead, ConsumerConnectionInformationInterface consumerConnectionInformation, Date pdcDate, String pdcRemark)throws Exception {
        String methodName = " processPDC : ";

        logger.info(methodName + " fetching custom read and checking its sub-beans ");
        Read customRead = readService.getLatestByConsumerNoForPunchingFinalReading(consumerNo);
        if(customRead == null) {
            logger.error(methodName + "some error in fetching CustomRead");
            throw new Exception("some error in fetching CustomRead");
        }

        if (finalRead == null) {
            logger.error(methodName + "final read is null in method .");
            throw new Exception("final read is null in method");
        }

        String meteringStatus = consumerConnectionInformation.getMeteringStatus();
        if(meteringStatus == null){
            logger.error(methodName + "Metering Status is null in method .");
            throw new Exception("Metering Status is null in method");
        }
        if(meteringStatus.equals(ConsumerConnectionInformationInterface.METERING_STATUS_METERED)){

            ReadMasterInterface insertedFinalRead = readMasterService.insertFinalRead(consumerNo,finalRead,customRead);
            ConsumerMeterMappingInterface consumerMeterMapping = consumerMeterMappingService.InActivateMappingByConsumerNo(consumerNo,insertedFinalRead);
            AdjustmentInterface rcdcAdjustment = raiseRCDC(consumerNo,insertedFinalRead.getBillMonth(),pdcDate,pdcRemark);

            logger.info(methodName + "data updated successfully for metered consumer: "+rcdcAdjustment+
                    " ConsumerMeterMapping: "+consumerMeterMapping+" RCDCAdjustment: "+rcdcAdjustment);
        }else if(meteringStatus.equals(ConsumerConnectionInformationInterface.METERING_STATUS_UNMETERED)) {
            ScheduleInterface schedule = customRead.getSchedule();
            String billMonth;
            if(schedule != null){
                logger.info(methodName + " Not-Submitted, Pending Schedule present for consumer's group ");
                billMonth = schedule.getBillMonth();
            }else{
                logger.info(methodName + " Not-Submitted, Pending Schedule not present for consumer's group ");
                schedule = scheduleService.getLatestCompletedScheduleByConsumerNo(consumerNo);
                if(schedule == null){
                    logger.error(methodName + "completed schedule not found");
                    throw new Exception("completed schedule not found");
                }
                String lastScheduleBillMonth = schedule.getBillMonth();
                billMonth = GlobalResources.getNextMonth(lastScheduleBillMonth);
            }
            AdjustmentInterface rcdcAdjustment = raiseRCDC(consumerNo,billMonth,pdcDate,pdcRemark);
            logger.info(methodName + "adjustment RCDC raised successfully for unmetered consumer: "+rcdcAdjustment);
        } else{
            logger.error(methodName + "invalid metering status"+meteringStatus);
            throw new Exception("invalid metering status");
        }

        consumerNoMaster.setStatus(ConsumerNoMasterInterface.STATUS_INACTIVE);
        updateAuditDetails(consumerNoMaster);
        ConsumerNoMasterInterface updatedConsumerNoMaster = consumerNoMasterDAO.update(consumerNoMaster);
        if(updatedConsumerNoMaster == null){
            logger.error(methodName + "failed in saving consumerNoMaster ");
            throw new Exception("failed in saving consumerNoMaster");
        }
        logger.info(methodName + "PDC process completed successfully, ConsumerNo Master: "+consumerNoMaster);
        return updatedConsumerNoMaster;
    }

    private AdjustmentInterface raiseRCDC(String consumerNo, String billMonth, Date date, String remark)throws Exception {
        AdjustmentInterface rcdcAdjustment = new Adjustment();
        //hard coded code
        rcdcAdjustment.setCode(3);
        long amount = configuratorService.getValueForCode(AdjustmentInterface.CODE_RC_DC);
        rcdcAdjustment.setAmount(new BigDecimal(amount));
        rcdcAdjustment.setConsumerNo(consumerNo);
        rcdcAdjustment.setPostingBillMonth(billMonth);
        rcdcAdjustment.setPostingDate(date);
        rcdcAdjustment.setDeleted(AdjustmentInterface.DELETED_FALSE);
        rcdcAdjustment.setPosted(AdjustmentInterface.POSTED_FALSE);
        rcdcAdjustment.setRemark(remark);
        AdjustmentInterface insertedAdjustment = adjustmentService.insert(rcdcAdjustment);
        return insertedAdjustment;
    }

    public void updateAuditDetails(ConsumerNoMasterInterface consumerNoMaster) {
        consumerNoMaster.setUpdatedBy(GlobalResources.getLoggedInUser());
        consumerNoMaster.setUpdatedOn(GlobalResources.getCurrentDate());
    }

    public ConsumerNoMasterInterface getByConsumerNo(String consumerNo)  {
        String methodName = "getByConsumerNo() :";
        ConsumerNoMasterInterface consumerNoMaster = null;
        logger.info( methodName + "called");
        if(consumerNo != null) {
            consumerNoMaster = consumerNoMasterDAO.getByConsumerNo(consumerNo);
        }
        return consumerNoMaster;
    }

    public long countByLocationCodeAndStatus(String locationCode, String status){
        final String methodName = "countByLocationCodeAndStatus() : ";
        logger.info(methodName + "called");
        long count = 0;
        if(locationCode != null && status != null){
            count = consumerNoMasterDAO.countByLocationCodeAndStatus(locationCode,status);
        }
        return count;
    }

    public long countByLocationCode(String locationCode) {
        final String methodName = "countByLocationCode() : ";
        logger.info(methodName + "called");
        long count = 0;
        if(locationCode != null ){
            count = consumerNoMasterDAO.countByLocationCode(locationCode);
        }
        return count;
    }

    public long countByGroupNoAndStatus(String groupNo, String status){
        final String methodName = "countByGroupNoAndStatus() : ";
        logger.info(methodName + "called");
        long count = 0;
        if(groupNo != null && status != null){
            count = consumerNoMasterDAO.countByGroupNoAndStatus(groupNo,status);
        }
        return count;
    }

    public long countByGroupNo(String groupNo) {
        final String methodName = "countByGroupNo() : ";
        logger.info(methodName + "called");
        long count = 0;
        if(groupNo != null ){
            count = consumerNoMasterDAO.countByGroupNo(groupNo);
        }
        return count;
    }

    public long countByGroupNoAndReadingDiaryNoAndStatus(String groupNo,String readingDiaryNo, String status){
        final String methodName = "countByGroupNoAndReadingDiaryNoAndStatus() : ";
        logger.info(methodName + "called");
        long count = 0;
        if(groupNo != null && status != null){
            count = consumerNoMasterDAO.countByGroupNoAndReadingDiaryNoAndStatus(groupNo,readingDiaryNo,status);
        }
        return count;
    }

    public long countByGroupNoAndReadingDiaryNo(String groupNo, String readingDiaryNo) {
        final String methodName = "countByGroupNoAndReadingDiaryNo() : ";
        logger.info(methodName + "called");
        long count = 0;
        if(groupNo != null && readingDiaryNo != null ){
            count = consumerNoMasterDAO.countByGroupNoAndReadingDiaryNo(groupNo,readingDiaryNo);
        }
        return count;
    }

    public ZoneConsumerData getZoneConsumerDataByLocationCode(String locationCode, ErrorMessage errorMessage) {
        final String methodName = "getZoneConsumerDataByLocationCode() : ";
        logger.info(methodName + "called");

        if(locationCode == null){
            errorMessage.setErrorMessage("location code is null");
            return null;
        }
        ZoneInterface zone = zoneService.getByLocationCode(locationCode);

        if(zone == null) {
            errorMessage.setErrorMessage("zone does not exist with given location code");
            return null;
        }

        ZoneConsumerData zoneConsumerData = new ZoneConsumerData();
        zoneConsumerData.setZone(zone);
        zoneConsumerData.setActiveConsumers(countByLocationCodeAndStatus(locationCode, ConsumerNoMasterInterface.STATUS_ACTIVE));
        zoneConsumerData.setInactiveConsumers(countByLocationCodeAndStatus(locationCode, ConsumerNoMasterInterface.STATUS_INACTIVE));
        zoneConsumerData.setConsumers(countByLocationCode(locationCode));
        List<GroupInterface> groups = groupService.getByLocationCode(locationCode);
        List<GroupConsumerData> groupConsumerDataList = new ArrayList<>() ;
        for (GroupInterface group : groups ) {
            GroupConsumerData groupData = getGroupConsumerDataByGroup(group,errorMessage);
            if(groupData == null){
                return null;
            }
            groupConsumerDataList.add(groupData);
        }
        zoneConsumerData.setGroups(groupConsumerDataList);
        return zoneConsumerData;
    }


    public GroupConsumerData getGroupConsumerDataByGroupNo(String groupNo, ErrorMessage errorMessage) {
        final String methodName = "getGroupConsumerDataByGroupNo() : ";
        logger.info(methodName + "called");
        GroupConsumerData groupConsumerData = null;

        if(groupNo == null){
            errorMessage.setErrorMessage("group no is null");
            return null;
        }
        GroupInterface group = groupService.getByGroupNo(groupNo);

        if(group == null) {
            errorMessage.setErrorMessage("group does not exist with given group no");
            return null;
        }

        groupConsumerData = getGroupConsumerDataByGroup(group, errorMessage);
        return groupConsumerData;
    }

    public GroupConsumerData getGroupConsumerDataByGroup(GroupInterface group, ErrorMessage errorMessage) {
        final String methodName = "getGroupConsumerDataByGroup() : ";
        logger.info(methodName + "called");
        if(group == null){
            errorMessage.setErrorMessage("group object is null");
            return null;
        }
        String groupNo = group.getGroupNo();
        GroupConsumerData groupConsumerData = new GroupConsumerData();
        groupConsumerData.setGroup(group);
        groupConsumerData.setActiveConsumers(countByGroupNoAndStatus(groupNo, ConsumerNoMasterInterface.STATUS_ACTIVE));
        groupConsumerData.setInactiveConsumers(countByGroupNoAndStatus(groupNo, ConsumerNoMasterInterface.STATUS_INACTIVE));
        groupConsumerData.setConsumers(countByGroupNo(groupNo));

        List<ReadingDiaryConsumerData> readingDiaryConsumerDataList = new ArrayList<>();
        List<ReadingDiaryNoInterface> readingDiaries = readingDiaryNoService.getByGroupNo(groupNo);

        for (ReadingDiaryNoInterface readingDiary:readingDiaries) {
            ReadingDiaryConsumerData readingDiaryConsumerData = getReadingDiaryConsumerDataByReadingDiary(readingDiary,errorMessage);
            if(readingDiaryConsumerData == null){
                return null;
            }
            readingDiaryConsumerDataList.add(readingDiaryConsumerData);
        }
        groupConsumerData.setReadingDiaries(readingDiaryConsumerDataList);
        return groupConsumerData;
    }


    public ReadingDiaryConsumerData getReadingDiaryConsumerDataByGroupNoAndReadingDiaryNo(String groupNo, String readingDiaryNo, ErrorMessage errorMessage) {
        final String methodName = "getReadingDiaryConsumerDataByReadingDiaryNo() : ";
        logger.info(methodName + "called");

        if(readingDiaryNo == null){
            errorMessage.setErrorMessage("readingDiaryNo is null");
            return null;
        }

        if(groupNo == null){
            errorMessage.setErrorMessage("groupNo is null");
            return null;
        }

        ReadingDiaryNoInterface readingDiary = readingDiaryNoService.getByGroupNoAndReadingDiaryNo(groupNo,readingDiaryNo);

        if(readingDiary == null) {
            errorMessage.setErrorMessage("reading diary does not exist with given group no and diary no");
            return null;
        }

        ReadingDiaryConsumerData readingDiaryConsumerData = getReadingDiaryConsumerDataByReadingDiary(readingDiary,errorMessage);
        return readingDiaryConsumerData;
    }

    public ReadingDiaryConsumerData getReadingDiaryConsumerDataByReadingDiary(ReadingDiaryNoInterface readingDiary, ErrorMessage errorMessage) {
        final String methodName = "getReadingDiaryConsumerDataByReadingDiary() : ";
        logger.info(methodName + "called");

        if(readingDiary == null){
            errorMessage.setErrorMessage("group object is null");
            return null;
        }
        String readingDiaryNo = readingDiary.getReadingDiaryNo();
        String groupNo = readingDiary.getGroupNo();
        ReadingDiaryConsumerData readingDiaryConsumerData = new ReadingDiaryConsumerData();
        readingDiaryConsumerData.setReadingDiary(readingDiary);
        readingDiaryConsumerData.setActiveConsumers(countByGroupNoAndReadingDiaryNoAndStatus(groupNo, readingDiaryNo, ConsumerNoMasterInterface.STATUS_ACTIVE));
        readingDiaryConsumerData.setInactiveConsumers(countByGroupNoAndReadingDiaryNoAndStatus(groupNo, readingDiaryNo, ConsumerNoMasterInterface.STATUS_INACTIVE));
        readingDiaryConsumerData.setConsumers(countByGroupNoAndReadingDiaryNo(groupNo,readingDiaryNo));
        return readingDiaryConsumerData;
    }
}
