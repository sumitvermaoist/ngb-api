package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.DiscomInterface;
import com.mppkvvcl.ngbdao.daos.DiscomDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Created by SHIVANSHU on 17-07-2017.
 */
@Service
public class DiscomService {
    /**
     * Asking spring to inject DiscomRepository so that we can use its various methods.
     */
    @Autowired
    private DiscomDAO discomDAO;

    /**
     * getting logger for logging in current class.
     */
    Logger logger = GlobalResources.getLogger(DiscomService.class);

    /**
     * This gelAll() method is used for getting all object of discom<br><br>
     * return List of Discom
     */
    public List<? extends DiscomInterface> getAll(){
        String methodName = "getAll() : ";
        logger.info(methodName+"Starting to get all Discom");
        List<? extends DiscomInterface> discoms = discomDAO.getAll();
        if (discoms != null){
            logger.info(methodName+"Discom list found successfully");
            if (discoms.size() > 0 ){
                logger.info(methodName+"List found with Size"+discoms.size());
            }else{
                logger.error(methodName+"Discom list found but no object consist");
            }
        }else{
            logger.error(methodName+"Discom list return null");
        }
        return discoms;
    }

}
