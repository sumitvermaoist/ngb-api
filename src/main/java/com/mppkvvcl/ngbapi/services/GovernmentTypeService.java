package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.GovernmentTypeInterface;
import com.mppkvvcl.ngbdao.daos.GovernmentTypeDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by SUMIT on 15-06-2017.
 */
@Service
public class GovernmentTypeService {
    /**
     * Asking Spring to inject GovernmentTypeRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on xray connection information  table
     * at the backend Database
     */
    @Autowired
    private GovernmentTypeDAO governmentTypeDAO;

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(GovernmentTypeService.class);

    /**
     * This insert method takes governmentType object and insert it into the table in the backend database.
     * return insertedGovernmentType if successful else return null.<br><br>
     * param governmentType<br>
     * return insertedGovernmentType<br>
     */
    public GovernmentTypeInterface insert(GovernmentTypeInterface governmentType){
        String methodName = " insert : ";
        GovernmentTypeInterface insertedGovernmentType = null;
        logger.info(methodName + "Started insertion for GovernmentType ");
        if(governmentType != null){
            logger.info(methodName + " Calling GovernmentTypeRepository for inserting GovernmentType ");
            insertedGovernmentType = governmentTypeDAO.add(governmentType);
            if (insertedGovernmentType != null){
                logger.info(methodName + "Successfully inserted one row for GovernmentType");
            }else {
                logger.error(methodName + "Unable to insert into GovernmentType. GovernmentTypeRepository returning null");
            }
        }else{
            logger.error(methodName + " Received GovernmentType object is null ");
        }
        return insertedGovernmentType;
    }

    /**
     * This method fetches list of government type from backend database.
     * Return list if successful else return null.<br><br>
     * return governmentTypes<br>
     */
    public List<? extends GovernmentTypeInterface> getAll(){
        String methodName = "getAll() : ";
        logger.info(methodName + "Got request to view all Government Type");
        List<? extends GovernmentTypeInterface> governmentTypes = null;
        governmentTypes = governmentTypeDAO.getAll();
        if(governmentTypes != null){
            if(governmentTypes.size() > 0 ){
                logger.info(methodName + "List received successfully");
            }else{
                logger.error(methodName + "No content found in list");
            }
        }else{
            logger.error(methodName + "List not found");
        }
        return  governmentTypes;
    }
}
