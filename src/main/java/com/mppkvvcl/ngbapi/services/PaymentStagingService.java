package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbdao.daos.PaymentStagingDAO;
import com.mppkvvcl.ngbentity.beans.ErrorMessage;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerNoMasterInterface;
import com.mppkvvcl.ngbinterface.interfaces.PaymentStagingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 1/3/2018.
 */
@Service
public class PaymentStagingService {
    /**
     * Getting logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(PaymentStagingService.class);

    @Autowired
    private PaymentStagingDAO paymentStagingDAO;

    @Autowired
    private ConsumerNoMasterService consumerNoMasterService;

    /**
     * This method takes ivrs and fetch a list of payments.
     * Return list if successful else return null.<br><br>
     * param ivrs<br>
     * return List of Payment<br>
     */
    public List<PaymentStagingInterface> getByIvrs(String ivrs) {
        String methodName = "getByIvrs() : ";
        List<PaymentStagingInterface> paymentStagingInterfaces = null;
        logger.info(methodName + "called " );
        if(ivrs != null){
            paymentStagingInterfaces = paymentStagingDAO.getByIvrs(ivrs);
        }
        return paymentStagingInterfaces;
    }

    /**
     *
     * @param paymentStagingInterface
     * @param errorMessage
     * @return
     */
    public PaymentStagingInterface insert(PaymentStagingInterface paymentStagingInterface, ErrorMessage errorMessage) {
        String methodName = "insert() : ";
        PaymentStagingInterface insertedPaymentStagingInterface = null;
        logger.info(methodName + "called " );
        if(paymentStagingInterface != null){
  /*todo Check consumer belongs to migrated DC
            String ivrs = paymentStagingInterface.getIvrs();
            String locationCode = paymentStagingInterface.getLocationCode();
            ConsumerNoMasterInterface consumerNoMasterInterface = consumerNoMasterService.getByOldServiceNoOneAndLocationCode(ivrs, locationCode);
            if(consumerNoMasterInterface == null){
                logger.error("not a valid ivrs");
                errorMessage.setErrorMessage("not a valid ivrs");
                return null;
            }
    */
            setAuditDetails(paymentStagingInterface);
            paymentStagingInterface.setStatus(PaymentStagingInterface.STATUS_PENDING);
            insertedPaymentStagingInterface = paymentStagingDAO.add(paymentStagingInterface);
        }
        return insertedPaymentStagingInterface;
    }

    private void setAuditDetails(PaymentStagingInterface paymentStagingInterface) {
        String loggedInUser = GlobalResources.getLoggedInUser();
        Date currentDate = GlobalResources.getCurrentDate();
        paymentStagingInterface.setCreatedBy(loggedInUser);
        paymentStagingInterface.setCreatedOn(currentDate);
        paymentStagingInterface.setUpdatedBy(loggedInUser);
        paymentStagingInterface.setUpdatedOn(currentDate);
    }

    /**
     * This method takes status and fetch a list of PaymentStaging.
     * Return list if successful else return null.<br><br>
     * param status<br>
     * return List of Payment<br>
     */
    public List<PaymentStagingInterface> getByStatus(String status) {
        String methodName = "getByStatus() : ";
        List<PaymentStagingInterface> paymentStagingInterfaces = null;
        logger.info(methodName + "called " );
        if(status != null){
            paymentStagingInterfaces = paymentStagingDAO.getByStatus(status);
        }
        return paymentStagingInterfaces;
    }

    /**
     * This method takes status and fetch a list of PaymentStaging.
     * Return list if successful else return null.<br><br>
     * param status<br>
     * return List of Payment<br>
     */
    public List<PaymentStagingInterface> getByDateOfTransactionAndStatus(Date dateOfTransaction, String status) {
        String methodName = "getByDateOfTransactionAndStatus() : ";
        List<PaymentStagingInterface> paymentStagingInterfaces = null;
        logger.info(methodName + "called " );
        if(dateOfTransaction != null && status != null){
            paymentStagingInterfaces = paymentStagingDAO.getByDateOfTransactionAndStatus(dateOfTransaction,status);
        }
        return paymentStagingInterfaces;
    }

    public PaymentStagingInterface update(PaymentStagingInterface paymentStagingInterface){
        final String methodName = "update() : ";
        logger.info(methodName+"called");
        PaymentStagingInterface updatedPaymentStaging = null;
        paymentStagingInterface.setUpdatedOn(GlobalResources.getCurrentDate());
        paymentStagingInterface.setUpdatedBy(GlobalResources.getLoggedInUser());
        if(paymentStagingInterface != null){
            updatedPaymentStaging = paymentStagingDAO.update(paymentStagingInterface);
        }
        return updatedPaymentStaging;
    }
}
