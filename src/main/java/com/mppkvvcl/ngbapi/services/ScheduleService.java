package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.security.services.UserDetailService;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionAreaInformationInterface;
import com.mppkvvcl.ngbinterface.interfaces.ScheduleInterface;
import com.mppkvvcl.ngbdao.daos.ScheduleDAO;
import com.mppkvvcl.ngbentity.beans.Schedule;
import com.mppkvvcl.ngbentity.beans.UserDetail;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 7/1/2017.
 */
@Service
public class ScheduleService {

    @Autowired
    private ScheduleDAO scheduleDAO;

    @Autowired
    private HolidayService holidayService;

    @Autowired
    private LocalHolidayService localHolidayService;

    @Autowired
    private UserDetailService userDetailService;

    @Autowired
    private ConsumerConnectionAreaInformationService consumerConnectionAreaInformationService;

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(ScheduleService.class);

    public List<ScheduleInterface> getByGroupNoAndBillStatusAndSubmitted(String groupNo, String billStatus, String submitted) {
        String methodName = " getByGroupNoAndBillStatusAndSubmitted(): ";
        logger.info(methodName + "called with " + groupNo + " " + billStatus + " " + submitted);
        List<ScheduleInterface> schedules = null;
        if(groupNo != null && billStatus != null && submitted != null){
            schedules = scheduleDAO.getByGroupNoAndBillStatusAndSubmitted(groupNo, billStatus, submitted);
        }
        return schedules;
    }

    /**
     * This method takes groupNo and fetch the latest schedule from schedule
     * Return schedule if successful else return null.
     * <p>
     * param groupNo
     * return
     */
    public ScheduleInterface getLatestScheduleByGroupNo(String groupNo) {
        String methodName = " getLatestScheduleByGroupNo() : ";
        logger.info(methodName + "called with groupNo : " + groupNo);
        ScheduleInterface schedule = null;
        if (groupNo != null) {
            schedule = scheduleDAO.getTopByGroupNoOrderByIdDesc(groupNo);
        }
        return schedule;
    }

    /**
     * This method takes groupNo and fetch the latest schedule from schedule
     * Return schedule if successful else return null.
     * <p>
     * param groupNo
     * return
     */
    public List<ScheduleInterface> getAllSchedulesByGroupNo(String groupNo) {
        String methodName = " getAllSchedulesByGroupNo() : ";
        List<ScheduleInterface> schedules = null;
        logger.info(methodName + "called for groupNo : " + groupNo);
        if (groupNo != null) {
            schedules = scheduleDAO.getByGroupNoOrderByIdDesc(groupNo);
        }
        return schedules;
    }

    /**
     * This insert method takes schedule object and insert that schedule in backend database.
     * 1. this method fetch the top last schedule of group
     * 2. and check bill month in supplied schedule is in sequence, and there is no schedule already exist for given bill month
     * 3. check 15 days interval in-between two schedules.
     * 4. if all goes fine, set required variable and save the schedule
     *
     * @param scheduleToInsert return insertedSchedule
     */

    public ScheduleInterface insert(ScheduleInterface scheduleToInsert) throws Exception{
        String methodName = "insert() : ";
        logger.info(methodName + "called");
        ScheduleInterface insertedSchedule = null;
        if (scheduleToInsert == null) {
            logger.error(methodName + "Input param schedule found null");
            throw new Exception("schedule to add not found ");
        }
        String groupNo = scheduleToInsert.getGroupNo();
        if(groupNo == null){
            logger.error(methodName + " group No is null input schedule");
            throw new Exception(" group No is null ");
        }
        ScheduleInterface topSchedule = getLatestScheduleByGroupNo(groupNo);
        logger.info(methodName + "got top schedule as " + topSchedule);
        if (topSchedule != null) {
            String topScheduleBillMonth = topSchedule.getBillMonth();
            String toBeScheduleBillMonth = GlobalResources.getNextMonth(topScheduleBillMonth);
            String scheduleToInsertBillMonth = scheduleToInsert.getBillMonth();
            if (scheduleToInsertBillMonth.equals(toBeScheduleBillMonth)) {
                if(checkDueDateInSchedule(scheduleToInsert)){
                    logger.info(methodName + "due dates in schedule are correct ");
                }else{
                    logger.error(methodName + "due dates are not correct in schedule");
                    throw new Exception("due dates are not correct in schedule ");
                }
                Date scheduleToInsertBillDate = scheduleToInsert.getBillDate();
                Date topScheduleBillDate = topSchedule.getBillDate();
                long noOfDaysDifference = GlobalResources.getDateDiffInDays(topScheduleBillDate, scheduleToInsertBillDate);
                logger.info(methodName + "no of days between old and new billdate " + noOfDaysDifference);
                if (noOfDaysDifference <= ScheduleInterface.DIFFERENCE_BETWEEN_SCHEDULE) {
                    logger.error(methodName + "difference between two schedules is not more than 15 days");
                    throw new Exception("difference between two schedules is not more than 15 days ");
                }
                logger.info(methodName + "Difference between 2 dates is more than 15 days, hence going for insertion");
                setDefaultValuesForSchedule(scheduleToInsert);
                insertedSchedule = scheduleDAO.add(scheduleToInsert);
                if (insertedSchedule == null) {
                    logger.error(methodName + "Unable to insert schedule object");
                    throw new Exception("Unable to insert schedule object ");
                }
                logger.info(methodName + "schedule object inserted successfully");
            } else {
                if (scheduleToInsertBillMonth.equals(topScheduleBillMonth)) {
                    logger.error(methodName + "schedule already exist");
                    throw new Exception("schedule already exist ");
                } else {
                    logger.error(methodName + " bill month in supplied schedule is not in sync " +
                            "found last schedule is " + topSchedule);
                    throw new Exception("bill month in supplied schedule is not in sync ");
                }
            }
        } else {
            logger.info(methodName + "old schedule not found, checking if it is new group");
            long scheduleCount = scheduleDAO.getCountByGroupNo(groupNo);
            if (scheduleCount == 0) {
                if(checkDueDateInSchedule(scheduleToInsert)){
                    logger.info(methodName + "due dates in schedule are correct ");
                }else{
                    logger.error(methodName + "due dates are not correct in schedule");
                    throw new Exception("due dates are not correct in schedule ");
                }
                setDefaultValuesForSchedule(scheduleToInsert);
                insertedSchedule = scheduleDAO.add(scheduleToInsert);
                if (insertedSchedule == null) {
                    logger.error(methodName + "Unable to insert schedule object");
                    throw new Exception("Unable to insert schedule object ");
                }
                logger.info(methodName + "schedule object inserted successfully");
            } else {
                logger.error(methodName + "previous month schedule not found ");
                throw new Exception("previous month schedule not found ");
            }
        }
        return insertedSchedule;
    }

    private boolean checkDueDateInSchedule(ScheduleInterface scheduleToInsert) throws Exception {
        String methodName = "checkDueDateInSchedule() : ";
        boolean checkDueDate = false;
        Date scheduleToInsertBillDate = scheduleToInsert.getBillDate();
        List<Date> dueDates = getDueDates(scheduleToInsertBillDate);
        if(dueDates == null){
            logger.error(methodName + "not able to determine due dates at server ");
            throw new Exception("not able to determine due dates at server ");
        }

        if(GlobalResources.getDateDiffInDays(dueDates.get(0),scheduleToInsert.getChequeDueDate()) != 0){
            logger.error(methodName + "cheque due date is not as expected ");
            throw new Exception("cheque due date is not as expected ");
        }

        if( GlobalResources.getDateDiffInDays(dueDates.get(1),scheduleToInsert.getDueDate()) != 0){
            logger.error(methodName + " due date is incorrect");
            throw new Exception(" due date is incorrect ");
        }
        logger.info(methodName + "due dates check successful ");
        checkDueDate = true;
        return checkDueDate;
    }

    private void setDefaultValuesForSchedule(ScheduleInterface scheduleToInsert){
        scheduleToInsert.setBillStatus(ScheduleInterface.BILL_STATUS_PENDING);
        scheduleToInsert.setSubmitted(ScheduleInterface.SUBMITTED_N);
        scheduleToInsert.setR15Status(ScheduleInterface.R15_STATUS_UNFREEZE);
        String loggedInUser = GlobalResources.getLoggedInUser();
        Date currentDate = GlobalResources.getCurrentDate();
        scheduleToInsert.setCreatedBy(loggedInUser);
        scheduleToInsert.setCreatedOn(currentDate);
        scheduleToInsert.setUpdatedBy(loggedInUser);
        scheduleToInsert.setUpdatedOn(currentDate);
    }


    /**
     * Added By : Preetesh Date: 21 July 2017
     * This editPendingSchedule method takes schedule object and update that schedule in backend database.
     * 1. first it check that schedule exists or not by query db with ID.
     * 2. then it compare "billMonth" of supplied schedule with last completed schedule, to check it is in sequence
     * 3. next it check for the difference of 15 days between the last schedule and current schedule
     * 4. if all goes right change updated-By and On, and update the schedule
     * Return updatedSchedule if updating successful else return null.
     *
     * @param scheduleToUpdate
     * @return insertedSchedule
     */
    public ScheduleInterface editPendingSchedule(Schedule scheduleToUpdate)throws Exception {
        String methodName = "editPendingSchedule() : ";
        ScheduleInterface updatedSchedule = null;
        logger.info(methodName + "Got request to update schedule" + scheduleToUpdate);
        if (scheduleToUpdate == null) {
            logger.error(methodName + "Input param schedule found null");
            throw new Exception("Input param schedule found null ");
        }

        long scheduleId = scheduleToUpdate.getId();
        ScheduleInterface existingSchedule = getById(scheduleId);

        String billingStatus = existingSchedule.getBillStatus();
        if(billingStatus.equals(ScheduleInterface.BILL_STATUS_COMPLETED)){
            logger.error(methodName + "Billing Status for schedule is Completed in db " +existingSchedule);
            throw new Exception("Billing Status for schedule is Completed in db  ");
        }
        String billMonthInScheduleToUpdate = scheduleToUpdate.getBillMonth();
        String billMonthInExistingSchedule = existingSchedule.getBillMonth();
        if (!billMonthInExistingSchedule.equals(billMonthInScheduleToUpdate)) {
            logger.error(methodName + "Bill months are not same in supplied schedule and existing schedule ");
            throw new Exception(" Bill months are not same in supplied schedule and existing schedule ");
        }
        String groupNo = scheduleToUpdate.getGroupNo();
        String previousBillMonth = GlobalResources.getPreviousMonth(billMonthInScheduleToUpdate);
        ScheduleInterface lastSchedule = scheduleDAO.getByGroupNoAndBillMonth(groupNo, previousBillMonth);
        logger.info(methodName + "got last schedule as " + lastSchedule);
        if (lastSchedule != null) {
            if(checkDueDateInSchedule(scheduleToUpdate)){
                logger.info(methodName + "due dates in schedule are correct ");
            }else{
                logger.error(methodName + "due dates are not correct in schedule");
                throw new Exception("due dates are not correct in schedule ");
            }
            Date lastScheduleBillDate = lastSchedule.getBillDate();
            Date scheduleToUpdateBillDate = scheduleToUpdate.getBillDate();
            long noOfDaysDifference = GlobalResources.getDateDiffInDays(lastScheduleBillDate, scheduleToUpdateBillDate);
            logger.info(methodName + "no of days between old and new billdate " + noOfDaysDifference);
            if (noOfDaysDifference > ScheduleInterface.DIFFERENCE_BETWEEN_SCHEDULE) {
                logger.info(methodName + "Difference between 2 dates is more than 15 days, hence going for updation");
                updatedSchedule = update(scheduleToUpdate);
            } else {
                logger.error(methodName + "difference between two schedules is not more than 15 days");
                throw new Exception("difference between two schedules is not more than 15 days ");
            }
        } else {
            logger.info(methodName + "old schedule not found");
            long scheduleCount = scheduleDAO.getCountByGroupNo(groupNo);
            if (scheduleCount == 1) {
                if(checkDueDateInSchedule(scheduleToUpdate)){
                    logger.info(methodName + "due dates in schedule are correct ");
                }else{
                    logger.error(methodName + "due dates are not correct in schedule");
                    throw new Exception("due dates are not correct in schedule ");
                }
                updatedSchedule = update(scheduleToUpdate);
            } else {
                logger.error(methodName + "last month schedule not found ");
                throw new Exception("last month schedule not found ");
            }
        }
        return updatedSchedule;
    }


    public ScheduleInterface getLatestNotSubmittedPendingScheduleByConsumerNo(String consumerNo) {
        String methodName = "getLatestNotSubmittedPendingScheduleByConsumerNo() : ";
        logger.info(methodName + "called for consumerNo " + consumerNo);
        ScheduleInterface selectedSchedule = null;
        if (consumerNo != null) {
            consumerNo = consumerNo.trim();
            ConsumerConnectionAreaInformationInterface consumerConnectionAreaInformation = consumerConnectionAreaInformationService.getByConsumerNo(consumerNo);
            if (consumerConnectionAreaInformation != null && consumerConnectionAreaInformation.getGroupNo() != null) {
                List<ScheduleInterface> schedules = null;
                schedules = getByGroupNoAndBillStatusAndSubmitted(consumerConnectionAreaInformation.getGroupNo(), ScheduleInterface.BILL_STATUS_PENDING, Schedule.SUBMITTED_N);
                if (schedules != null && schedules.size() == 1) {
                    ScheduleInterface schedule = schedules.get(0);
                    if (schedule != null) {
                        logger.info(methodName + "Schedule found for given consumer's group as: " + schedule);
                        selectedSchedule = schedule;
                    }
                }
            }
        }
        return selectedSchedule;
    }

    public ScheduleInterface getLatestCompletedScheduleByConsumerNo(String consumerNo) {
        String methodName = "getLatestNotSubmittedPendingScheduleByConsumerNo() : ";
        logger.info(methodName + "called");
        ScheduleInterface selectedSchedule = null;
        if (consumerNo != null) {
            consumerNo = consumerNo.trim();
            ConsumerConnectionAreaInformationInterface consumerConnectionAreaInformation = consumerConnectionAreaInformationService.getByConsumerNo(consumerNo);
            logger.info(methodName + "consumer area information for given consumer found as: " + consumerConnectionAreaInformation);
            if (consumerConnectionAreaInformation != null && consumerConnectionAreaInformation.getGroupNo() != null) {
                ScheduleInterface schedule = null;
                String groupNo = consumerConnectionAreaInformation.getGroupNo();
                schedule = scheduleDAO.getTopByGroupNoAndBillStatusOrderByIdDesc(groupNo, ScheduleInterface.BILL_STATUS_COMPLETED);
                if (schedule != null) {
                    logger.info(methodName + "Schedule found for given consumer's group as: " + schedule);
                    selectedSchedule = schedule;
                } else {
                    logger.error(methodName + "Schedule not found for given consumer's group");
                }
            } else {
                logger.error(methodName + "consumerConnectionAreaInformation not found");
            }
        } else {
            logger.error(methodName + " inputs parameters is null.Returning bad request");
        }
        return selectedSchedule;
    }


    /**
     * Added By: Preetesh.    Date 12 July 2017
     * this method will return the last BillMonth for which Billing has been completed
     * param groupNo
     * return
     */
    public String getLastParticipatedBillMonthByGroupNo(String groupNo) throws Exception {
        String methodName = "getLastParticipatedBillMonthByGroupNo() : ";
        logger.info(methodName + "called for groupNo " + groupNo);
        String billMonth = null;
        if (groupNo != null) {
            groupNo = groupNo.trim();
            ScheduleInterface schedule = scheduleDAO.getTopByGroupNoAndBillStatusOrderByIdDesc(groupNo, ScheduleInterface.BILL_STATUS_COMPLETED);
            if (schedule != null) {
                logger.info(methodName + "Schedule found for given consumer's group as: " + schedule);
                billMonth = schedule.getBillMonth();
            } else {
                logger.error(methodName + "Schedule not found for given consumer's group");
                throw new Exception("Schedule not found for given consumer's group");
            }
        }
        return billMonth;
    }

    /**
     * Added By: Preetesh.    Date 12 July 2017
     * This method will return Scheduled and Not Submitted BillMonth
     * param groupNo
     * return
     */
    public String getBillMonthForNotSubmittedPendingScheduleByGroupNo(String groupNo) {
        String methodName = "getBillMonthForNotSubmittedPendingScheduleByGroupNo() : ";
        logger.info(methodName + "called");
        String billMonth = null;
        if (groupNo != null) {
            groupNo = groupNo.trim();
            List<ScheduleInterface> schedules = getByGroupNoAndBillStatusAndSubmitted(groupNo, ScheduleInterface.BILL_STATUS_PENDING, Schedule.SUBMITTED_N);
            if (schedules != null && schedules.size() == 1) {
                ScheduleInterface schedule = schedules.get(0);
                if (schedule != null) {
                    logger.info(methodName + "Schedule found for given consumer's group as: " + schedule);
                    billMonth = schedule.getBillMonth();
                } else {
                    logger.error(methodName + "Schedule not found for given consumer's group");
                }
            } else {
                logger.error(methodName + "Schedules not found");
            }
        }
        return billMonth;
    }


    public List<Date> getDueDates(Date billDate) throws Exception {
        String methodName = "getDueDates() : ";
        logger.info(methodName + "called");
        List<Date> dueDates = null;
        UserDetail userDetail = userDetailService.getByUsername(GlobalResources.getLoggedInUser());
        if(userDetail == null){
            logger.error(methodName + "error in fetching userDetail detail ");
            throw new Exception("error in fetching userDetail detail ");
        }
        String locationCode = userDetail.getLocationCode();
        if(locationCode==null){
            logger.error(methodName + "location code not found ");
            throw new Exception("location code not found");
        }
        logger.info(methodName + "bill date in date format "+billDate);
        Date dueDate = GlobalResources.addDaysInDate(ScheduleInterface.DIFFERENCE_BETWEEN_BILL_DATE_AND_DUE_DATE,billDate);
        Date confirmDueDate = checkHolidayAndSetDueDate(dueDate,locationCode );
        if(confirmDueDate == null){
            logger.error(methodName + "please select some other bill date, too many holidays ");
            throw new Exception("please select some other bill date, too many holidays");
        }
        Date chequeDueDate = GlobalResources.addDaysInDate(ScheduleInterface.DIFFERENCE_BETWEEN_BILL_DATE_AND_CHEQUE_DUE_DATE,billDate);
        Date confirmChequeDueDate = checkHolidayAndSetDueDate(chequeDueDate,locationCode );
        if(confirmChequeDueDate == null){
            logger.error(methodName + "please select some other bill date, too many holidays ");
            throw new Exception("please select some other bill date, too many holidays");
        }
        dueDates = new ArrayList<Date>();
        dueDates.add(confirmChequeDueDate);
        dueDates.add(confirmDueDate);
        logger.info("due Dates selected as "+dueDates);
        return dueDates;
    }


    private Date checkHolidayAndSetDueDate(Date dueDate, String locationCode) {
        String methodName = "checkHolidayAndSetDueDate() : ";
        logger.info(methodName + "called " + dueDate + " " + locationCode);
        Date finalizedDate = null;
        for(int plusDays = 0, minusDays=0; plusDays < 7; plusDays++, minusDays--){
            Date tentativeDueDate = GlobalResources.addDaysInDate(plusDays,dueDate);
            logger.info(methodName + "tentativeDateWithPlus "+tentativeDueDate);
            boolean checkHoliday = holidayService.checkHoliday(tentativeDueDate);
            boolean checkLocalHoliday = localHolidayService.checkLocalHoliday(tentativeDueDate,locationCode);
            boolean checkSunday = holidayService.checkSunday(tentativeDueDate);
            boolean checkSecondSaturday = holidayService.checkSecondSaturday(tentativeDueDate);
            if(checkHoliday || checkLocalHoliday || checkSunday || checkSecondSaturday){
                tentativeDueDate = GlobalResources.addDaysInDate(minusDays,dueDate);
                checkHoliday = holidayService.checkHoliday(tentativeDueDate);
                checkLocalHoliday = localHolidayService.checkLocalHoliday(tentativeDueDate,locationCode);
                checkSunday = holidayService.checkSunday(tentativeDueDate);
                checkSecondSaturday = holidayService.checkSecondSaturday(tentativeDueDate);
                if(checkHoliday || checkLocalHoliday || checkSunday || checkSecondSaturday){
                    logger.info(methodName + "its a holiday on date "+tentativeDueDate);
                    continue;
                }else{
                    logger.info(methodName + "non holiday Date found "+tentativeDueDate);
                    finalizedDate = tentativeDueDate;
                    break;
                }
            }else{
                logger.info(methodName + "non holiday Date found"+tentativeDueDate);
                finalizedDate = tentativeDueDate;
                break;
            }
        }
        return finalizedDate;
    }

    public List<ScheduleInterface> getTop2ByGroupNo(String groupNo) {
        String methodName = " getTop2ByGroupNo() : ";
        List<ScheduleInterface> schedules = null;
        logger.info(methodName + "Got request to get top 2 schedules against groupNo : " + groupNo);
        if (groupNo != null) {
            logger.info(methodName + "Calling schedule Repo method to get Schedules ");
            schedules = scheduleDAO.getTop2ByGroupNoOrderByIdDesc(groupNo);
            if (schedules != null && schedules.size() == 2) {
                logger.info(methodName + "Got " + schedules.size() + " schedules against groupNo " + groupNo);
            } else {
                logger.error(methodName + "Schedules not found against groupNo" + groupNo);
            }
        } else {
            logger.error(methodName + "Input param groupNo found null :" + groupNo);
        }
        return schedules;
    }


    public ScheduleInterface submit(long scheduleId)throws Exception {
        String methodName = "submit() : ";
        logger.info(methodName + "called for id " + scheduleId);
        ScheduleInterface updatedSchedule = null;
        ScheduleInterface existingSchedule = getById(scheduleId);
        String submittedStatus = existingSchedule.getSubmitted();
        if(submittedStatus.equals(ScheduleInterface.SUBMITTED_Y)){
            logger.error(methodName + "schedule already in submitted status  " + scheduleId);
            throw new Exception("schedule already in submitted status  ");
        }
        existingSchedule.setSubmitted(ScheduleInterface.SUBMITTED_Y);
        updatedSchedule = update(existingSchedule);
        return updatedSchedule;
    }

    public ScheduleInterface getById(long scheduleId) {
        String methodName = "getById() : ";
        logger.info(methodName + "called for id " + scheduleId);
        ScheduleInterface existingSchedule = null;
        if(scheduleId > 0) {
            existingSchedule = scheduleDAO.getById(scheduleId);
        }
        return existingSchedule;
    }


    public ScheduleInterface update(ScheduleInterface scheduleToUpdate) {
        String methodName = "update() : ";
        ScheduleInterface updatedSchedule = null;
        logger.info(methodName + "Got request to update schedule");
        if (scheduleToUpdate != null) {
            long scheduleId = scheduleToUpdate.getId();
            ScheduleInterface existingSchedule = getById(scheduleId);
            if (existingSchedule != null) {
                scheduleToUpdate.setUpdatedBy(GlobalResources.getLoggedInUser());
                scheduleToUpdate.setUpdatedOn(GlobalResources.getCurrentDate());
                updatedSchedule = scheduleDAO.update(scheduleToUpdate);
            }else{
                logger.error(methodName + "schedule not found for updating with id "+scheduleId);
            }
        }else{
            logger.error(methodName + "Input param schedule found null");
        }
        return updatedSchedule;
    }

    public  ScheduleInterface getByGroupNoAndBillMonth(String groupNo, String billMonth){
        String methodName = "getByGroupNoAndBillMonth() : ";
        logger.info(methodName+ "called");
        ScheduleInterface schedule = null;
        if(groupNo != null && billMonth != null){
            schedule = scheduleDAO.getByGroupNoAndBillMonth(groupNo,billMonth);
        }
        return schedule;
    }
}