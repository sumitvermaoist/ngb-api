package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.SeasonalConnectionInformationInterface;
import com.mppkvvcl.ngbdao.daos.SeasonalConnectionInformationDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by SUMIT on 15-06-2017.
 */
@Service
public class SeasonalConnectionInformationService {
    /**
     * Asking Spring to inject SeasonalConnectionInformationRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on consumer government mapping  table
     * at the backend Database
     */
    @Autowired
    private SeasonalConnectionInformationDAO seasonalConnectionInformationDAO;

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(SeasonalConnectionInformationService.class);

    /**
     * This insert method takes seasonalConnectionInformation object and insert it into the SeasonalConnectionInformation table in the backend database.<br>
     * Return insertedSeasonalConnectionInformation object if successful else return null.<br><br>
     * param seasonalConnectionInformation<br><br>
     * return insertedSeasonalConnectionInformation<br>
     */
    public SeasonalConnectionInformationInterface insert(SeasonalConnectionInformationInterface seasonalConnectionInformation){
        String methodName = "insert() ";
        SeasonalConnectionInformationInterface insertedSeasonalConnectionInformation = null ;
        logger.info(methodName + "Started insertion for SeasonalConnectionInformation ");
        if(seasonalConnectionInformation != null){
            logger.info(methodName + "Calling SeasonalConnectionInformationRepository for inserting SeasonalConnectionInformation");
            setAuditDetails(seasonalConnectionInformation);
            insertedSeasonalConnectionInformation = seasonalConnectionInformationDAO.add(seasonalConnectionInformation);
            if (insertedSeasonalConnectionInformation != null){
                logger.info(methodName + "Successfully inserted SeasonalConnectionInformation ");
            }else{
                logger.error(methodName + "unable to insert seasonal connection information row. Repository sending null");
            }
        }else{
            logger.error(methodName + "Received SeasonalConnectionInformation object received is null");
        }
        return insertedSeasonalConnectionInformation ;
    }

    public SeasonalConnectionInformationInterface getByConsumerNo(String consumerNo)  {
        String methodName = "getByConsumerNo() ";
        logger.info(methodName + "called");
        SeasonalConnectionInformationInterface seasonalConnectionInformation = null;
        if(consumerNo != null) {
            seasonalConnectionInformation = seasonalConnectionInformationDAO.getTopByConsumerNoOrderByIdDesc(consumerNo);
        }
        return seasonalConnectionInformation;
    }


    private  void setAuditDetails(SeasonalConnectionInformationInterface seasonalConnectionInformationInterface){
        String methodName = "setAuditDetails()  :";
        if(seasonalConnectionInformationInterface != null) {
            Date date = GlobalResources.getCurrentDate();
            String user = GlobalResources.getLoggedInUser();
            seasonalConnectionInformationInterface.setUpdatedOn(date);
            seasonalConnectionInformationInterface.setUpdatedBy(user);
            seasonalConnectionInformationInterface.setCreatedOn(date);
            seasonalConnectionInformationInterface.setCreatedBy(user);
        } else {
            logger.error(methodName + "given input is null");
        }
    }
}
