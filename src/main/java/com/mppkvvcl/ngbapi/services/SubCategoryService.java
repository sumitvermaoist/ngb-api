package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.PurposeSubCategoryMappingInterface;
import com.mppkvvcl.ngbinterface.interfaces.SubCategoryInterface;
import com.mppkvvcl.ngbinterface.interfaces.TariffInterface;
import com.mppkvvcl.ngbdao.daos.SubCategoryDAO;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SUMIT on 24-05-2017.
 */
@Service
public class SubCategoryService {

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(SubCategoryService.class);

    /**
     *Asking Spring to inject SubCategoryRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on sub_category table
     * at the backend Database
     */
    @Autowired
    private SubCategoryDAO subCategoryDAO;

    /**
     * Asking Spring to inject tariffService dependency in the variable
     * so that we get various services of tariffServices operation on tariffRepository.
     *
      */
    @Autowired
    private TariffService tariffService;

    /**
     * Asking Spring to inject purposeSubCategoryMappingService dependency in the variable
     * so that we get various services of purposeSubCategoryMappingService operation on purposeSubCategoryMappingService
     */
    @Autowired
    private PurposeSubCategoryMappingService purposeSubCategoryMappingService;

    /**
     * This method provides subcategories for below input parameters. If date is passed NULL then current date is considered for fetching out<br>
     * subcategories corresponding to currently effective date.<br><br>
     * param tariffCategory<br>
     * param meteringStatus<br>
     * param connectionType<br>
     * param effectiveDate<br>
     * param premiseType<br><br>
     * return subCategories<br>
     */
    public List<SubCategoryInterface> getByTariffCategoryAndMeteringStatusAndConnectionTypeAndEffectiveDateAndPremiseType(String tariffCategory, String meteringStatus,
                                                                                                                          String connectionType, String effectiveDate, String premiseType){
        List<SubCategoryInterface> subCategories = null;
        logger.info("findByTariffCategoryAndMeteringStatusAndConnectionTypeAndEffectiveDateAndPremiseType: Getting tariff by ");
        if(tariffCategory != null && meteringStatus != null && connectionType != null && premiseType != null){
            List<TariffInterface> tariffs = tariffService.getByTariffCategoryAndMeteringStatusAndConnectionTypeAndEffectiveDate(tariffCategory,meteringStatus,connectionType,effectiveDate);
            if(tariffs != null){
                logger.info("findByTariffCategoryAndMeteringStatusAndConnectionTypeAndEffectiveDateAndPremiseType: Fetching subcategories for total: "+tariffs.size()+" tariffs");
                subCategories = new ArrayList<>();
                for(TariffInterface tariff : tariffs){
                    logger.info("findByTariffCategoryAndMeteringStatusAndConnectionTypeAndEffectiveDateAndPremiseType: Fetching subcategories for tariff Id: "+tariff.getId()+" and premisetype: "+premiseType);
                    List<SubCategoryInterface> categories = subCategoryDAO.getByTariffIdAndPremiseType(tariff.getId(),premiseType);
                    if(categories != null){
                        subCategories.addAll(categories);
                    }
                }
                logger.info("findByTariffCategoryAndMeteringStatusAndConnectionTypeAndEffectiveDateAndPremiseType: Returning total subcategories as: "+subCategories.size());
            }
        }
        return subCategories;
    }

    /**
     * This method provides SubCategory for below input parameters.<br>
     * The SubCategory is always returned single object of SubCategory.<br><br>
     * param tariffId<br>
     * param premiseType<br>
     * param purposeOfInstallationId<br>
     * param connectedLoad<br><br>
     * return subcategory<br>
     */
    public SubCategoryInterface getByTariffIdAndPremiseTypeAndPurposeOfInstallationIdAndConnectedLoad(Long tariffId,String premiseType,Long purposeOfInstallationId,BigDecimal connectedLoad ){
        SubCategoryInterface subcategory = null;
        List<SubCategoryInterface> subCategories=null;
        logger.info("findByTariffIdAndPremiseTypeAndPurposeOfInstallationIdAndConnectedLoad: Getting Subcategory  by tariffId "
                +tariffId+" "+"premiseType "+premiseType+" "+"purposeOfInstallationId"+" "+purposeOfInstallationId+" "+"connectedLoad"+" "+connectedLoad);
        if(tariffId != null && premiseType != null && purposeOfInstallationId != null && connectedLoad != null){
            logger.info("findByTariffIdAndPremiseTypeAndPurposeOfInstallationIdAndConnectedLoad: Getting subcategories for input parameters ");
             subCategories= subCategoryDAO.getByTariffIdAndConnectedLoadAndPremiseType(tariffId,connectedLoad,premiseType);
            if(subCategories != null){
                if(subCategories.size() > 0){
                    logger.info("findByTariffIdAndPremiseTypeAndPurposeOfInstallationIdAndConnectedLoad : Got "+subCategories.size()+ "subcategories");
                    for(SubCategoryInterface subCategoryLocal:subCategories){
                        Long subCategoryCode=subCategoryLocal.getCode();
                        PurposeSubCategoryMappingInterface purposeSubCategoryMapping = purposeSubCategoryMappingService.getByPurposeOfInstallationIdAndSubCategoryCode(purposeOfInstallationId,subCategoryCode);
                        if(purposeSubCategoryMapping != null) {
                            logger.info("findByTariffIdAndPremiseTypeAndPurposeOfInstallationIdAndConnectedLoad : Got SubCategory "+subCategoryLocal);
                            subcategory=subCategoryLocal;
                            break;
                        }
                    }
                }
            }
        }
        return subcategory;
    }

    /**
     * This method provides SubCategories for above input parameters.<br>
     * The SubCategory is returns List of SubCategory.<br><br>
     * param tariffId<br>
     * param premiseType<br>
     * param purposeOfInstallationId<br>
     * param applicantType<br>
     * param connectedLoad<br><br>
     * return selectedSubCategories<br>
     */
    public List<SubCategoryInterface> getByTariffIdAndPremiseTypeAndPurposeOfInstallationIdAndApplicantTypeAndConnectedLoad(Long tariffId,String premiseType,Long purposeOfInstallationId,String applicantType,BigDecimal connectedLoad ){
        List<SubCategoryInterface> selectedSubCategories = null;
        List<SubCategoryInterface> subCategories = null;
        logger.info("getByTariffIdAndPremiseTypeAndPurposeOfInstallationIdAndApplicantTypeAndConnectedLoad: Getting Subcategory  by tariffId "
                +tariffId+" "+"premiseType "+premiseType+" "+"purposeOfInstallationId"+" "+purposeOfInstallationId+" "+"connectedLoad"+" "+connectedLoad +" applicantType "+applicantType );
        if(tariffId != null && premiseType != null && purposeOfInstallationId != null && connectedLoad != null && applicantType != null){
            logger.info("getByTariffIdAndPremiseTypeAndPurposeOfInstallationIdAndApplicantTypeAndConnectedLoad: Getting subcategories for input parameters ");
            subCategories = subCategoryDAO.getByTariffIdAndConnectedLoadAndPremiseTypeAndApplicantType(tariffId,connectedLoad,premiseType,applicantType);
            if(subCategories != null){
                if(subCategories.size() > 0){
                    selectedSubCategories = new ArrayList<SubCategoryInterface>();
                    logger.info("getByTariffIdAndPremiseTypeAndPurposeOfInstallationIdAndApplicantTypeAndConnectedLoad : Got "+subCategories.size()+ "subcategories");
                    for(SubCategoryInterface sc : subCategories){
                        Long subCategoryCode = sc.getCode();
                        if(subCategoryCode != null){
                            PurposeSubCategoryMappingInterface purposeSubCategoryMapping = purposeSubCategoryMappingService.getByPurposeOfInstallationIdAndSubCategoryCode(purposeOfInstallationId,subCategoryCode);
                            if(purposeSubCategoryMapping != null) {
                                logger.info("getByTariffIdAndPremiseTypeAndPurposeOfInstallationIdAndApplicantTypeAndConnectedLoad : Got SubCategory "+sc +" adding SubCategory in selectedSubCategories list ");
                                selectedSubCategories.add(sc);
                            }
                        }

                    }
                }
            }
        }
        return selectedSubCategories;
    }

    public SubCategoryInterface getCurrentSubcategory(long code){
        String methodName = "getlatestSubcategory()";
        logger.info(methodName + " Getting current SubCategory for input code " + code);
        SubCategoryInterface subCategory = null;
        logger.info(methodName + " Callling SubCategoryRepository to fetch SubCategory ");
        subCategory = subCategoryDAO.getTopByCodeOrderByIdDesc(code);
        if(subCategory != null){
            logger.info(methodName + "Successfully fetched SubCategory object corresponding to input code " + code);
        }else {
            logger.error(methodName + " Couldn't retrieve SubCategory object due some technical error ");
        }
        return subCategory;
    }

    public List<SubCategoryInterface> getByTariffIdAndPremiseTypeAndApplicantTypeAndConnectedLoadAndContractDemand(Long tariffId,String premiseType,String applicantType,BigDecimal connectedLoad ,BigDecimal contractDemand){
        List<SubCategoryInterface> subCategories = null;
        String methodName = "getByTariffIdAndPremiseTypeAndPurposeOfInstallationIdAndApplicantTypeAndConnectedLoadAndContractDemand () : ";
        logger.info(methodName+"Getting Subcategory  by tariffId"+tariffId+"premiseType "+premiseType+"connectedLoad"+" "+connectedLoad +" applicantType "+applicantType+"ContractDemand"+contractDemand );
        if(tariffId != null && premiseType != null && connectedLoad != null && applicantType != null && contractDemand != null){
            logger.info(methodName+ "Getting subcategories for input parameters");
            subCategories = subCategoryDAO.getByTariffIdAndConnectedLoadAndContractDemandAndPremiseTypeAndApplicantType(tariffId,connectedLoad,contractDemand,premiseType,applicantType);
        }
        return subCategories;
    }
}
