package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.custombeans.CustomLoadDetail;
import com.mppkvvcl.ngbapi.custombeans.CustomTariffDetail;
import com.mppkvvcl.ngbapi.custombeans.Read;
import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbdao.daos.TariffDetailDAO;
import com.mppkvvcl.ngbentity.beans.*;
import com.mppkvvcl.ngbinterface.interfaces.*;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by ANSHIKA on 27-07-2017.
 */
@Service
public class TariffDetailService {

    /**
     * Getting whole logger object from GlobalResources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(TariffDetailService.class);

    /**
     *Requesting spring to get singleton TariffDetailDAO object.
     */
    @Autowired
    private TariffDetailDAO tariffDetailDAO;

    @Autowired
    private ReadMasterService readMasterService;

    @Autowired
    private TariffChangeDetailService tariffChangeDetailService;

    @Autowired
    private LoadDetailService loadDetailService;

    @Autowired
    private TariffDescriptionService tariffDescriptionService;

    @Autowired
    private ConsumerConnectionInformationService consumerConnectionInformationService;

    @Autowired
    private ConfiguratorService configuratorService;

    @Autowired
    private ConsumerNoMasterService consumerNoMasterService;

    @Autowired
    private SeasonalConnectionInformationService seasonalConnectionInformationService;

    @Autowired
    private BillService billService;

    @Autowired
    private ConsumerMeterMappingService consumerMeterMappingService;

    @Autowired
    private SubCategoryService subCategoryService;

    @Autowired
    private ReadTypeConfiguratorService readTypeConfiguratorService;

    @Autowired
    private ReadService readService;

    @Autowired
    private TariffChangeMappingService tariffChangeMappingService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private TariffService tariffService;

    @Autowired
    private XrayConnectionInformationService xrayConnectionInformationService;

    @Autowired
    private MeterMasterService meterMasterService;

    @Autowired
    private TariffLoadMappingService tariffLoadMappingService;

    /**
     * this method will return the custom bean tariff, so that tariff details can be prepared
     * for Tariff change, with checking all tariff change validations.
     * param consumerNo
     * return
     * throws Exception
     */

    public CustomTariffDetail getByConsumerNo(String consumerNo) throws Exception {
        String methodName = "getByConsumerNo() :";
        logger.info(methodName + "called");
        TariffDetailInterface tariffDetail = null;
        TariffChangeDetailInterface tariffChangeDetail = null;
        ConsumerNoMasterInterface consumerNoMaster = null;
        TariffDescriptionInterface tariffDescription = null;
        LoadDetailInterface loadDetail = null;
        SeasonalConnectionInformationInterface seasonalConnectionInformation = null;
        ConsumerConnectionInformationInterface consumerConnectionInformation = null;
        Read read = null;
        List<? extends TariffChangeMappingInterface> tariffChangeMappings = null;
        SubCategoryInterface subCategory = null;
        if (consumerNo == null) {
            logger.error(methodName + "Given Consumer No is Null" + consumerNo);
            throw new Exception("Given Consumer No is Null");
        }
        consumerNo = consumerNo.trim();
        logger.info(methodName + " fetching ConsumerNoMasterRepository for getting ConsumerNoMaster ");
        consumerNoMaster = consumerNoMasterService.getByConsumerNo(consumerNo);
        if (consumerNoMaster == null) {
            logger.error(methodName + "Given consumer no doesn't exist in  ConsumerNoMaster");
            throw new Exception("Given consumer no doesn't exist in  ConsumerNoMaster");
        }
        String status = consumerNoMaster.getStatus();
        if (status == null) {
            logger.error(methodName + "status found null for given consumer no :" + consumerNo);
            throw new Exception("status found null for given consumer no ");
        }
        //Below validation Checks whether consumer is INACTIVE (PDC)
        if (status.equals(ConsumerNoMasterInterface.STATUS_INACTIVE)) {
            logger.error(methodName + "Consumer is INACTIVE consumer No:" + consumerNo);
            throw new Exception("Consumer is INACTIVE");
        }
        logger.info(methodName + "Fetching Tariff Detail for Consumer no: " + consumerNo);
        tariffDetail = tariffDetailDAO.getTopByConsumerNoOrderByIdDesc(consumerNo);
        if (tariffDetail == null) {
            logger.error(methodName + "No tariff details found for Consumer no:" + consumerNo);
            throw new Exception("No tariff details found for Consumer");
        }
        logger.info(methodName + "Fetched Tariff Details for Consumer No :"+consumerNo);
        String billMonth = tariffDetail.getBillMonth();
        if(billMonth == null){
            logger.error(methodName + "Tariff already changed , one tariff change allowed in one billing month");
            throw new Exception("Tariff already changed, one tariff change allowed in one billing month");
        }
        CustomTariffDetail customTariffDetail  = new CustomTariffDetail();
        customTariffDetail.setId(tariffDetail.getId());
        customTariffDetail.setConsumerNo(tariffDetail.getConsumerNo());
        customTariffDetail.setTariffCode(tariffDetail.getTariffCode());
        customTariffDetail.setSubcategoryCode(tariffDetail.getSubcategoryCode());
        customTariffDetail.setEffectiveStartDate(tariffDetail.getEffectiveStartDate());
        customTariffDetail.setEffectiveEndDate(tariffDetail.getEffectiveEndDate());
        customTariffDetail.setBillMonth(tariffDetail.getBillMonth());
        consumerConnectionInformation = consumerConnectionInformationService.getByConsumerNo(consumerNo);
        if (consumerConnectionInformation == null) {
            logger.error(methodName + "Consumer connection information not found for consumer no :" + consumerNo);
            throw new Exception("Consumer connection information not found for consumer ");
        }
        String tariffCode = tariffDetail.getTariffCode();
        if (tariffCode == null) {
            logger.error(methodName + "tariffCategory not found for given Consumer no:" + consumerNo);
            throw new Exception("tariffCategory not found ");
        }
        String tariffCategory = tariffCode.substring(0,3);
        tariffDescription = tariffDescriptionService.getByTariffCategory(tariffCategory);
        if (tariffDescription == null) {
            logger.error(methodName + "tariffDescription not found");
            throw new Exception("tariffDescription not found");
        }
        // fetching tariffMapping in which consumer can change tariff  from existing tariff
        logger.info(methodName+ "fetching tariff change mapping");
        tariffChangeMappings = tariffChangeMappingService.getByTariffCategory(tariffCategory);
        if(tariffChangeMappings == null){
            logger.error(methodName + "unable to fetch tariff change mapping");
            throw new Exception(methodName+ "unable to fetch tariff change mapping");
        }
        logger.info(methodName + "fetched tariff change mapping");
        customTariffDetail.setTariffChangeMappingsInterface(tariffChangeMappings);
        String tariffDescriptionString = tariffDescription.getTariffDescription();
        logger.info(methodName + "Tariff Description for consumer :"+consumerNo+" is :"+tariffDescriptionString  );
        tariffChangeDetail = tariffChangeDetailService.getByTariffDetailId(tariffDetail.getId());
        List<XrayConnectionInformationInterface> xrayConnectionInformationInterfaces = null;
        XrayConnectionInformationInterface xrayConnectionInformation = null;
        if (tariffChangeDetail == null) {
            logger.info(methodName + "Tariff not changed previously");
            customTariffDetail.setIsTariffChange(false);
            if (consumerConnectionInformation.getMeteringStatus().equals(MeteringTypeInterface.METERING_STATUS_METERED)) {
                logger.info(methodName+" consumer is of metered type, fetching meter phase");
                ConsumerMeterMappingInterface consumerMeterMapping = consumerMeterMappingService.getActiveMappingByConsumerNo(consumerNo);
                if(consumerMeterMapping == null){
                    logger.error(methodName+ "consumer meter mapping not found");
                    throw new Exception("some error in fetching  consumer meter mapping");
                }
                String meterIdentifier = consumerMeterMapping.getMeterIdentifier();
                MeterMasterInterface meterMaster = meterMasterService.getByIdentifier(meterIdentifier);
                if(meterMaster == null){
                    logger.error(methodName+ "meter master not found for meter identifier :"+meterIdentifier);
                    throw new Exception("some error in fetching consumer meter phase");
                }
                String meterPhase = meterMaster.getPhase();
                customTariffDetail.setMeterPhase(meterPhase);

                if (tariffDescriptionString.equals(TariffDescriptionInterface.TARIFF_DESCRIPTION_AGRICULTURE) || tariffDescriptionString.equals(TariffDescription.TARIFF_DESCRIPTION_DOMESTIC)) {
                    logger.info(methodName + " Consumer is of "+tariffDescriptionString +"  category");
                    logger.info(methodName + "fetching read master");
                    read = readService.getLatestByConsumerNoForPunchingFinalReading(consumerNo);
                    customTariffDetail.setRead(read);
                    // fetching read master for Agriculture/domestic metered consumers only
                }
            }

            if(consumerConnectionInformation.getIsXray()) {
                logger.info(methodName + "consumer is of Xray type,fetching details");
                xrayConnectionInformationInterfaces = xrayConnectionInformationService.getByConsumerNoAndStatus(consumerNo, XrayConnectionInformationInterface.STATUS_ACTIVE);
                if (xrayConnectionInformationInterfaces == null) {
                    logger.error(methodName + "some error in fetching Xray connection information");
                    throw new Exception("some error in fetching Xray connection information");
                }
                logger.info(methodName+ "fetched Xray details");
                xrayConnectionInformation = xrayConnectionInformationInterfaces.get(0);
                customTariffDetail.setXrayConnectionInformationInterface(xrayConnectionInformation);
            }

        }else {
            customTariffDetail.setTariffChangeDetailInterface(tariffChangeDetail);
            customTariffDetail.setIsTariffChange(true);
            if (tariffChangeDetail.getMeteringStatus().equals(MeteringTypeInterface.METERING_STATUS_METERED)){
                logger.info(methodName+" consumer is of metered type, fetching meter phase");
                ConsumerMeterMappingInterface consumerMeterMapping = consumerMeterMappingService.getActiveMappingByConsumerNo(consumerNo);
                if(consumerMeterMapping == null){
                    logger.error(methodName+ "consumer meter mapping not found");
                    throw new Exception("some error in fetching  consumer meter mapping");
                }
                String meterIdentifier = consumerMeterMapping.getMeterIdentifier();
                MeterMasterInterface meterMaster = meterMasterService.getByIdentifier(meterIdentifier);
                if(meterMaster == null){
                    logger.error(methodName+ "meter master not found for meter identifier :"+meterIdentifier);
                    throw new Exception("some error in fetching consumer meter phase");
                }
                String meterPhase = meterMaster.getPhase();
                customTariffDetail.setMeterPhase(meterPhase);

                if (tariffDescriptionString.equals(TariffDescriptionInterface.TARIFF_DESCRIPTION_AGRICULTURE) || tariffDescriptionString.equals(TariffDescription.TARIFF_DESCRIPTION_DOMESTIC)) {
                    logger.info(methodName + " Consumer is of "+tariffDescriptionString +"  category");
                    logger.info(methodName + "fetching read master");
                    read = readService.getLatestByConsumerNoForPunchingFinalReading(consumerNo);
                    customTariffDetail.setRead(read);
                    // fetching read master for Agriculture/domestic metered consumers only
                }
            }
            if(tariffChangeDetail.getIsXray()) {
                logger.info(methodName + "consumer is of Xray type,fetching details");
                xrayConnectionInformationInterfaces = xrayConnectionInformationService.getByConsumerNoAndStatus(consumerNo,XrayConnectionInformationInterface.STATUS_ACTIVE);
                if (xrayConnectionInformationInterfaces == null) {
                    logger.error(methodName + "some error in fetching Xray connection information");
                    throw new Exception("some error in fetching Xray connection information");
                }
                logger.info(methodName + "fetched Xray details"+xrayConnectionInformationInterfaces);
                xrayConnectionInformation = xrayConnectionInformationInterfaces.get(0);
                customTariffDetail.setXrayConnectionInformationInterface(xrayConnectionInformation);
            }
        }
        logger.info(methodName + "Fetching load Details for Consumer No : " + consumerNo);
        loadDetail = loadDetailService.getLatestLoadDetail(consumerNo);
        if (loadDetail == null) {
            logger.error(methodName + "Load Details not found for given consumer no :" + consumerNo);
            throw new Exception("Load Details not found for given consumer no");
        }
        logger.info(methodName + "Fetched load details for consumer no :" + consumerNo);
        customTariffDetail.setLoadDetailInterface(loadDetail);
        logger.info(methodName + "fetching subcategory details for subcategory code : "+tariffDetail.getSubcategoryCode());
        subCategory = subCategoryService.getCurrentSubcategory(tariffDetail.getSubcategoryCode());
        logger.info(methodName + "end contract demand for subcategory :"+subCategory.getEndContractDemandKW());
        // below validation checks whether end contract demand is zero for consumer's existing subcategory code if yes it set CD unit and CD to Null.
        if(subCategory.getEndContractDemandKW().compareTo(BigDecimal.ZERO) == 0){
            logger.info(methodName + "in consumer's subcategory, contract demand not required ");
            loadDetail.setCdUnit(null);
            loadDetail.setContractDemand(null);
//            LoadDetailInterface loadDetailNew = loadDetailService.getLatestLoadDetail(consumerNo);
//            logger.info(methodName + "new load details "+loadDetailNew);
            customTariffDetail.setLoadDetailInterface(loadDetail);
        }
        customTariffDetail.setIsLoadChange(false);
        customTariffDetail.setMinDateToTariffChange(tariffDetail.getEffectiveStartDate());

        Date dateOfConnection = consumerConnectionInformation.getConnectionDate();
        long connectionAge = GlobalResources.getDateDiffInDays(dateOfConnection, GlobalResources.getCurrentDate());
        long agreementPeriodInDays = configuratorService.getValueForCode(ConfiguratorInterface.AGREEMENT_PERIOD);
        List<LoadDetailInterface> allLoadDetails = loadDetailService.getByConsumerNo(consumerNo);

        // checking for load changed previously
        if(allLoadDetails.size() > 1 && connectionAge <= agreementPeriodInDays){
            customTariffDetail.setIsLoadChange(true);
        }

        if(tariffDetail.getEffectiveStartDate().after(loadDetail.getEffectiveStartDate())){
            customTariffDetail.setMinDateToTariffChangeWithLoad(tariffDetail.getEffectiveStartDate());
        }else {
            customTariffDetail.setMinDateToTariffChangeWithLoad(loadDetail.getEffectiveStartDate());
        }

        BillInterface latestBill = billService.getLatestBillByConsumerNo(consumerNo);
        if(latestBill != null){
            logger.info(methodName+ "fetched latest bill,comparing dates");
            Date billDate = latestBill.getBillDate();
            if(billDate.after(loadDetail.getEffectiveStartDate())) {
                customTariffDetail.setMinDateToTariffChange(billDate);
            }
            //comparing loadEffectiveStartDate, tariffEffectiveStartDate, bill date to set in  setMinDateToTraiffChangeWithLoad
            if(billDate.after(customTariffDetail.getMinDateToTariffChangeWithLoad())){
                customTariffDetail.setMinDateToTariffChangeWithLoad(billDate);
            }
        }

        if (tariffDescriptionString.equals(TariffDescriptionInterface.TARIFF_DESCRIPTION_DOMESTIC)) {
            return customTariffDetail;
            // If consumer belongs to Domestic Category , returns without checking Agreement period.
        } else {
            String phase = consumerConnectionInformation.getPhase();
            boolean isSeasonal = consumerConnectionInformation.getIsSeasonal();
            logger.info(methodName + "Checking Seasonal Flag for consumer :" + consumerNo);
            // checking isSeasonal flag in existing consumer connection information tariff detail.
            if (tariffChangeDetail == null && isSeasonal) {
                logger.info(methodName + "Fetching Seasonal Connection information for Consumer No:" + consumerNo);
                seasonalConnectionInformation = seasonalConnectionInformationService.getByConsumerNo(consumerNo);
                if (seasonalConnectionInformation == null) {
                    logger.error(methodName + "Seasonal Connection Information not found for Given Consumer no: " + consumerNo);
                    throw new Exception("Seasonal Connection Information not found");
                }
                logger.info(methodName + "Fetched Seasonal Connection Information for Consumer No " + consumerNo);
                customTariffDetail.setSeasonalConnectionInformationInterface(seasonalConnectionInformation);
            }else {
                logger.info(methodName + "seasonal flag is 'N' in consumer connection information");
                if(tariffChangeDetail != null){
                    logger.info(methodName +" Tariff change details exist for consumer: "+consumerNo);
                    // checking isSeasonal flag in existing consumer tariff change detail
                    boolean isSeasonalTariffChangeFlag = tariffChangeDetail.getIsSeasonal();
                    if(isSeasonalTariffChangeFlag){
                        logger.info(methodName + "Fetching Seasonal Connection information for Consumer No:" + consumerNo);
                        seasonalConnectionInformation = seasonalConnectionInformationService.getByConsumerNo(consumerNo);
                        if (seasonalConnectionInformation == null) {
                            logger.error(methodName + "Seasonal Connection Information not found for Given Consumer no: " + consumerNo);
                            throw new Exception("Seasonal Connection Information not found");
                        }
                        logger.info(methodName + "Fetched Seasonal Connection Information for Consumer No " + consumerNo);
                        customTariffDetail.setSeasonalConnectionInformationInterface(seasonalConnectionInformation);
                    }
                }
            }
            // Below condition checks if Phase is single no need to check for Agreement period.
            if (phase.equals(ConsumerConnectionInformationInterface.PHASE_SINGLE)) {
                return customTariffDetail;
            } else {
                /**
                 * checking if connection is more than 2 years old, i.e.consumer has completed agreement period
                 */
                logger.info(methodName + "Connection Age :" + connectionAge + " agreementPeriodInDays :" + agreementPeriodInDays);
                if (connectionAge < agreementPeriodInDays) {
                    logger.error(methodName + "consumer is under agreement period");
                    throw new Exception("consumer is under agreement period");
                }
            }
        }
        return customTariffDetail;
    }

    /**
     *
     * param tariffDetail
     * return
     * throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public TariffDetailInterface insertTariffChange (CustomTariffDetail customTariffDetail) throws Exception {
        String methodName = "insertTariffChange() :";
        logger.info(methodName+ "called");
        TariffChangeDetailInterface insertedTariffChangeDetail1 = null;
        TariffDetailInterface insertedTariffDetail1 = null;
        TariffDetailInterface updatedOldTariffDetail = null;
        TariffChangeDetailInterface tariffChangeDetail = null;
        LoadDetailInterface loadDetail = null;
        LoadDetailInterface insertedLoadDetail = null;
        SeasonalConnectionInformationInterface seasonalConnectionInformationToInsert = null;
        SeasonalConnectionInformationInterface insertedSeasonalConnectionInformation = null;
        XrayConnectionInformationInterface xrayConnectionInformationToInsert = null;
        XrayConnectionInformationInterface insertedXrayConnectionInformation = null;
        ConsumerMeterMappingInterface updatedConsumerMeterMapping = null;
        ReadMasterInterface readMaster = null;
        ScheduleInterface schedule = null;
        Read read = null;
        BillInterface bill = null;
        CustomTariffDetail oldCustomTariffDetail = null;
        ConsumerConnectionInformationInterface consumerConnectionInformation = null;
        if (customTariffDetail == null) {
            logger.error(methodName + "Given input request is Null");
            throw new Exception("Given input request is null");
        }
        if (customTariffDetail.getConsumerNo() == null || customTariffDetail.getTariffChangeDetail() == null) {
            logger.error(methodName + "Given input request is Null");
            throw new Exception("Given input request is null");
        }
        tariffChangeDetail = customTariffDetail.getTariffChangeDetail();
        String consumerNo = customTariffDetail.getConsumerNo();
        consumerNo = consumerNo.trim();
        logger.info(methodName + "fetching old tariff details for Consumer No" + consumerNo);
        oldCustomTariffDetail = getByConsumerNo(consumerNo);
        if (oldCustomTariffDetail == null) {
            logger.error(methodName + "old tariff details not found for Consumer ");
            throw new Exception("old tariff details not found for Consumer ");
        }
        Date existingTariffEffectiveStartDate = oldCustomTariffDetail.getEffectiveStartDate();
        Date existingTariffEffectiveEndDate = oldCustomTariffDetail.getEffectiveEndDate();
        customTariffDetail.setEffectiveEndDate(existingTariffEffectiveEndDate);
        Date effectiveStartDateToInsert = customTariffDetail.getEffectiveStartDate();
        Date existingTariffNewEffectiveEndDate = GlobalResources.addDaysInDate(-1, effectiveStartDateToInsert);
        oldCustomTariffDetail.setEffectiveEndDate(existingTariffNewEffectiveEndDate);

        TariffDetailInterface oldTariffDetail = new TariffDetail();
        oldTariffDetail.setId(oldCustomTariffDetail.getId());
        oldTariffDetail.setConsumerNo(oldCustomTariffDetail.getConsumerNo());
        oldTariffDetail.setTariffCode(oldCustomTariffDetail.getTariffCode());
        oldTariffDetail.setSubcategoryCode(oldCustomTariffDetail.getSubcategoryCode());
        oldTariffDetail.setEffectiveStartDate(oldCustomTariffDetail.getEffectiveStartDate());
        oldTariffDetail.setEffectiveEndDate(oldCustomTariffDetail.getEffectiveEndDate());
        oldTariffDetail.setBillMonth(oldCustomTariffDetail.getBillMonth());
        long dateComparisionValue = effectiveStartDateToInsert.compareTo(existingTariffEffectiveStartDate);
        logger.info(methodName + "Value of date Comparision "+dateComparisionValue);

        // checking effective start date to insert of new tariff and  effective start date of old tariff detail
        if(dateComparisionValue <= 0 ){
            logger.error(methodName + " Tariff Start Date Should be Greater than Previous tariff effective date");
            throw new Exception(" Tariff Start Date Should be Greater than Previous tariff effective date");
        }
        logger.info(methodName + "fetching latest bill for Consumer no:" + consumerNo);
        bill =billService.getLatestBillByConsumerNo(consumerNo);
        if(bill != null){
            logger.info(methodName + "fetched latest bill for Consumer no:" + consumerNo);
            Date billDate = bill.getBillDate();
            long billDateComparisionValue = effectiveStartDateToInsert.compareTo(billDate);
            logger.info(methodName + "checking effective start date to insert with bill date of latest bill");
            // checking effective start date to insert of new tariff and bill date of latest bill.
            if(billDateComparisionValue <= 0){
                logger.error(methodName + " Tariff Start Date Should be Greater than Bill Date");
                throw new Exception(" Tariff Start Date Should be Greater than Bill Date");
            }
        }
        logger.info(methodName + "updating existing tariff's end date");
        updatedOldTariffDetail = update(oldTariffDetail);
        if (updatedOldTariffDetail == null) {
            logger.error(methodName + "unable to update existing tariff details for Consumer no:" + consumerNo);
            throw new Exception("unable to update existing tariff details");
        }
        logger.info(methodName + "Updated old tariff details for Consumer no: " + consumerNo);

        TariffDetailInterface tariffDetail = new TariffDetail();
        tariffDetail.setConsumerNo(customTariffDetail.getConsumerNo());
        tariffDetail.setTariffCode(customTariffDetail.getTariffCode());
        tariffDetail.setSubcategoryCode(customTariffDetail.getSubcategoryCode());
        tariffDetail.setEffectiveStartDate(customTariffDetail.getEffectiveStartDate());
        tariffDetail.setEffectiveEndDate(existingTariffEffectiveEndDate);
        logger.info(methodName + "Inserting Tariff details for Consumer no " + consumerNo);
        insertedTariffDetail1 = insert(tariffDetail);
        if (insertedTariffDetail1 == null) {
            logger.error(methodName + "Unable to save tariff details for Consumer no: " + consumerNo);
            throw new Exception("Unable to save tariff details for Consumer ");
        }
        logger.info(methodName + "Inserted Tariff details for Consumer No :" + consumerNo);
        long subCategoryCodeToInsert = customTariffDetail.getSubcategoryCode();
        tariffChangeDetail.setTariffDetailId(insertedTariffDetail1.getId());
        logger.info(methodName + "Inserting tariff change details for Consumer no:" + consumerNo);
        insertedTariffChangeDetail1 = tariffChangeDetailService.insert(tariffChangeDetail);
        if (insertedTariffChangeDetail1 == null) {
            logger.error(methodName + "Some error in insertion of  tariff change detail ");
            throw new Exception("Some error in insertion of  tariff change detail");
        }
        logger.info(methodName + "Inserted tariff change detail for Consumer no:" + consumerNo);
        consumerConnectionInformation = consumerConnectionInformationService.getByConsumerNo(consumerNo);
        if(consumerConnectionInformation == null){
            logger.error(methodName + "Unable to fetch consumer connection information for consumer no:"+consumerNo);
            throw new Exception("Unable to fetch consumer connection information for consumer");
        }
        loadDetail = customTariffDetail.getLoadDetail();

        // checking whether load detail to insert
        if (loadDetail == null) {
            logger.info(methodName + "No load details found to update in Tariff Details ");
        } else {
            logger.info(methodName + "calling load service insert()");
            CustomLoadDetail customLoadDetail = new CustomLoadDetail();
            customLoadDetail.setIsTariffChange(customTariffDetail.getIsTariffChange());
            customLoadDetail.setLoadDetailInterface(loadDetail);
            CustomLoadDetail insertedCustomLoadDetail = loadDetailService.insertLoadChange(customLoadDetail,insertedTariffDetail1.getSubcategoryCode());   //changes requied
            if (insertedCustomLoadDetail == null || insertedCustomLoadDetail.getLoadDetail() == null) {
                logger.error(methodName + "Some error in Insertion of Load details , aborting request");
                throw new Exception("Some error in Insertion of Load details , aborting request");
            }
            insertedLoadDetail = customLoadDetail.getLoadDetail();
            logger.info(methodName + "Inserted Load Details ");
        }

        logger.info(methodName+ "preparing tariff load mapping to insert");
        TariffLoadMappingInterface tariffLoadMapping = new TariffLoadMapping();
        tariffLoadMapping.setTariffDetailId(insertedTariffDetail1.getId());

        tariffLoadMapping.setLoadChange(customTariffDetail.getIsLoadChange());
        tariffLoadMapping.setTariffChange(customTariffDetail.getIsTariffChange());

        if(insertedLoadDetail != null){
            tariffLoadMapping.setLoadDetailId(insertedLoadDetail.getId());
        }else {
            TariffLoadMappingInterface existingTariffLoadMapping = tariffLoadMappingService.getLatestTariffLoadMappingByTariffDetailId(updatedOldTariffDetail.getId());
            if (existingTariffLoadMapping == null) {
                logger.error(methodName + "tariff load mapping not found ");
                throw new Exception("tariff load mapping not found");
            }
            tariffLoadMapping.setLoadDetailId(existingTariffLoadMapping.getLoadDetailId());
        }
        TariffLoadMappingInterface insertedTariffLoadMapping = tariffLoadMappingService.insert(tariffLoadMapping);
        if(insertedTariffLoadMapping == null){
            logger.error(methodName+ "unable to insert tariff load mapping ");
            throw new Exception("unable to insert tariff load mapping");
        }

        logger.info(methodName+ "checking for x ray information to insert");
        xrayConnectionInformationToInsert = customTariffDetail.getXrayConnectionInformationInterface();
        if(xrayConnectionInformationToInsert == null){
            logger.info(methodName+ "No Xray details to insert");
            // old tariff Detail have Xray detail and no xray details exist for new Tariff detail then update old Xray detail to inactive status
            if(!insertedTariffChangeDetail1.getIsXray() && consumerConnectionInformation.getIsXray()){
                List<XrayConnectionInformationInterface> xrayConnectionInformationInterfaces = xrayConnectionInformationService.getByConsumerNoAndStatus(consumerNo, XrayConnectionInformation.STATUS_ACTIVE);
               if(xrayConnectionInformationInterfaces == null){
                   logger.error(methodName+ "some error in fetching xray details");
                   throw new Exception("some error in fetching xray details");
               }
                XrayConnectionInformationInterface xrayConnectionInformationInterface = xrayConnectionInformationInterfaces.get(0);
                if(xrayConnectionInformationInterface == null){
                    logger.error(methodName+ "some error in fetching xray details");
                    throw new Exception("some error in fetching xray details");
                }
                xrayConnectionInformationInterface.setStatus(ConsumerMeterMapping.STATUS_INACTIVE);                       //use variable from X-Ray connection information Bean
                XrayConnectionInformationInterface updatedXrayConnectionInformation = xrayConnectionInformationService.update(xrayConnectionInformationInterface);
                if(updatedXrayConnectionInformation == null){
                    logger.error(methodName+ "some error to update old xray details");
                    throw new Exception("some error to update old xray details");
                }
                logger.info(methodName+ "updated xray details for consumer no "+consumerNo);
            }
        }else {
            logger.info(methodName+ "got xray detail to insert");
            insertedXrayConnectionInformation = xrayConnectionInformationService.insert(xrayConnectionInformationToInsert);
            if(insertedXrayConnectionInformation == null){
                logger.error(methodName+ "some error in insertion of Xray details");
                throw new Exception("some error in insertion of Xray details");
            }
        }

        logger.info(methodName + "Checking for Seasonal information to insert");
        seasonalConnectionInformationToInsert = customTariffDetail.getSeasonalConnectionInformation();

        //Checking for Seasonal information to insert
        if (seasonalConnectionInformationToInsert == null) {
            logger.info(methodName + "No seasonal details to update");
        } else {
            logger.info(methodName + "fetching schedule");
            schedule = scheduleService.getLatestCompletedScheduleByConsumerNo(consumerNo);
            if(schedule == null){
                logger.error(methodName+ "No completed schedule found");
                throw new Exception("No completed schedule found");
            }
            logger.info(methodName+ "fetched schedule");
            // check for schedule bill month with seasonStartBillMonth and seasonEndBillMonth

            String seasonStartBillMonth = seasonalConnectionInformationToInsert.getSeasonStartBillMonth();
            String seasonEndBillMonth = seasonalConnectionInformationToInsert.getSeasonEndBillMonth();
            String scheduleBillMonth = schedule.getBillMonth();
            if(seasonStartBillMonth == null || seasonEndBillMonth == null || scheduleBillMonth == null){
                logger.error(methodName + "Given Season start bill month Or season End bill month is Null");
                throw new Exception("Given Season start bill month Or season End bill month is Null");
            }
            int comparisionValueForSeasonStartBillMonth =  GlobalResources.compareMonth(seasonStartBillMonth,scheduleBillMonth);
            if(comparisionValueForSeasonStartBillMonth <= 0){
                logger.error(methodName + "given Seasonal start bill month is invalid");
                throw new Exception("given Seasonal start bill month is invalid, billing already completed for "+scheduleBillMonth);
            }
            int comparisionValueForSeasonEndBillMonth = GlobalResources.compareMonth(seasonEndBillMonth,seasonStartBillMonth);
            if(comparisionValueForSeasonEndBillMonth < 0){
                logger.error(methodName + "given Seasonal end bill month is less than seasonal start bill month");
                throw new Exception("given Seasonal end bill month is less than seasonal start bill month");
            }
            String meteringStatus = insertedTariffChangeDetail1.getMeteringStatus();;
            String connectionType = consumerConnectionInformation.getConnectionType();
            String tariffCode = insertedTariffDetail1.getTariffCode();
            String tariffCategory = tariffCode.substring(0,3);
            TariffInterface tariff = null;
            List<TariffInterface> tariffs = null;
            BigDecimal sanctionedLoad;
            BigDecimal contractDemand;
            tariffs = tariffService.getByTariffCategoryAndMeteringStatusAndConnectionTypeAndTariffTypeAndEffectiveDate(tariffCategory,meteringStatus,connectionType,TariffDetailInterface.TARIFF_TYPE,null);
            if(tariffs == null){
                logger.error(methodName+ "unable to fetch tariffs");
                throw new Exception("unable to fetch tariffs");
            }
            logger.info(methodName + "tariffs"+tariffs);
            tariff = tariffs.get(0);
            long tariffId = tariff.getId();
            String premiseType = consumerConnectionInformation.getPremiseType();
            long purposeOfInstallationId = insertedTariffChangeDetail1.getPurposeOfInstallationId();
            String applicantType = "NORMAL";
            if(insertedLoadDetail != null) {
                sanctionedLoad = loadDetail.getSanctionedLoad();
                contractDemand = loadDetail.getContractDemand();
            }else {
                logger.info(methodName +" fetching latest load for consumer :"+consumerNo);
                loadDetail  = loadDetailService.getLatestLoadDetail(consumerNo);
                if(loadDetail == null){
                    logger.error(methodName+ "some error in fetching load detail");
                    throw new Exception("some error in fetching load detail");
                }
                sanctionedLoad = loadDetail.getSanctionedLoad();
                contractDemand = loadDetail.getContractDemand();
            }
            List<SubCategoryInterface> subCategoryInterfaces = null;
            SubCategoryInterface subCategory =null;
            logger.info(methodName+ "getByTariffId"+tariffId+"AndPremiseType"+premiseType+"AndPurposeOfInstallationId"+purposeOfInstallationId+"AndApplicantType"+applicantType+"AndConnectedLoad"+sanctionedLoad+"And ContractDemand"+contractDemand);
            subCategoryInterfaces = subCategoryService.getByTariffIdAndPremiseTypeAndApplicantTypeAndConnectedLoadAndContractDemand(tariffId,premiseType,applicantType,sanctionedLoad,contractDemand);
            if(subCategoryInterfaces == null){
                logger.error(methodName +"some error in fetching Subcategory for Seasonal connection");
                throw new Exception("some error in fetching Subcategory for Seasonal connection");
            }
            logger.info(methodName+ "subcategory"+subCategoryInterfaces);
            subCategory = subCategoryInterfaces.get(0);
            long subCategoryCode = subCategory.getCode();
            seasonalConnectionInformationToInsert.setSeasonSubCategory(subCategoryCode);
            logger.info(methodName + "inserting seasonal details for consumer no:" + consumerNo);
            insertedSeasonalConnectionInformation = seasonalConnectionInformationService.insert(seasonalConnectionInformationToInsert);
            if (insertedSeasonalConnectionInformation == null) {
                logger.error(methodName + "Unable to insert seasonal details for consumer no :" + consumerNo);
                throw new Exception("Unable to insert seasonal details given consumer ");
            }
            logger.info(methodName + "Inserted seasonal details for consumer no :" + consumerNo);
        }
        read = customTariffDetail.getRead();
        if(read == null){
            logger.info(methodName+"no read details to insert");
        }else {
            logger.info(methodName + "read details present, fetching read master ");
            readMaster = read.getReadMaster();
            if(readMaster == null){
                logger.info(methodName+"no read master details to insert");
            }else {
                logger.info(methodName + "fetched read details");
                logger.info(methodName + "reading is" + readMaster);
            }
        }
        // below validation checks metering status changed from metered to un-metered
        // if yes 1) insert final read 2) in-active Consumer meter mapping 3) if meter is of CTT type , then in-active MeterCtrMapping
        if (insertedTariffChangeDetail1.getMeteringStatus().equals(MeteringTypeInterface.METERING_STATUS_UNMETERED)){
            logger.info(methodName + "Tariff Change is of UN-METERED type, ");
            if (consumerConnectionInformation.getMeteringStatus().equals(MeteringTypeInterface.METERING_STATUS_METERED)) {
                logger.info(methodName + "metering status changed from metered to un metered :" + consumerNo);
                logger.info(methodName + "Inserting final read in Read Master");
                Read customRead = new Read();
                logger.info(methodName + "fetching MF for consumer no"+consumerNo);
                BigDecimal mf = consumerMeterMappingService.getMFByConsumerNoForActiveMeter(consumerNo);
                if(mf == BigDecimal.ZERO){
                    logger.error(methodName +" unable to fetch MF for consumer no");
                    throw new Exception("unable to fetch MF for consumer no");
                }
                logger.info(methodName+ "fetched MF for Consumer No"+consumerNo);
                customRead.setMf(mf);
                logger.info(methodName+ "fetching read type configurator for consumer No "+consumerNo);
                ReadTypeConfiguratorInterface readTypeConfigurator = readTypeConfiguratorService.getByTariffDetail(oldTariffDetail);
                if(readTypeConfigurator == null){
                    logger.error(methodName + "unable to fetch read type configurator for consumer no"+consumerNo);
                    throw new Exception("unable to fetch read type configurator");
                }
                logger.info(methodName+ "fetched read type configurator for consumer no  "+consumerNo);
                customRead.setReadTypeConfigurator(readTypeConfigurator);
                logger.info(methodName+"fetching read master for consumer no");
                ReadMasterInterface previousRead = readMasterService.getLatestReadingByConsumerNo(consumerNo);
                if(previousRead == null){
                    logger.error(methodName + "unable to fetch previous read for consumer no"+consumerNo);
                    throw new Exception("unable to fetch previous read");
                }
                logger.info(methodName + "fetched previous read for consumer no"+consumerNo);
                customRead.setReadMasterInterface(previousRead);
                ReadMasterInterface insertedReadMaster =  readMasterService.insertFinalRead(consumerNo,readMaster, customRead);
                if(insertedReadMaster == null){
                    logger.error(methodName + "unable to insert read master Kw ");
                    throw new Exception("unable to insert read master Kw");
                }
                logger.info(methodName + "inserted read master");
                logger.info(methodName+ "updating consumer meter mapping");
                updatedConsumerMeterMapping = consumerMeterMappingService.InActivateMappingByConsumerNo(consumerNo, readMaster);
                if (updatedConsumerMeterMapping == null) {
                    logger.error(methodName + "unable to update Consumer meter mapping for given consumer no :" + consumerNo);
                    throw new Exception("unable to update Consumer meter mapping ");
                }
                logger.info(methodName + "updated existing consumer meter mapping to inactive for consumer no :" + consumerNo);
            }
        }
        // below validation checks metering status change from Un-metered to metered type
        // if yes 1) insert active Consumer meter mapping 2) if meter is of CTT type , then insert active MeterCtrMapping 3) insert Start read
        if(insertedTariffChangeDetail1.getMeteringStatus().equals(MeteringTypeInterface.METERING_STATUS_METERED)) {
            logger.info(methodName + "Consumer is of metered  type ");
            if(consumerConnectionInformation.getMeteringStatus().equals(MeteringTypeInterface.METERING_STATUS_UNMETERED)){
                logger.info(methodName + " metering status change from Un-metered to metered type");
                updatedConsumerMeterMapping = consumerMeterMappingService.insertActiveMappingByConsumerNo(consumerNo,read);
                if(updatedConsumerMeterMapping == null){
                    logger.error(methodName + "unable to insert Consumer meter mapping for given consumer no :" + consumerNo);
                    throw new Exception("unable to insert Consumer meter mapping ");
                }else {
                    logger.info(methodName + "inserted consumer meter mapping "+updatedConsumerMeterMapping);
                    logger.info(methodName + "inserting  read master");
                    ReadMasterInterface insertedReadMaster = readMasterService.insertStartRead(readMaster);
                    if(insertedReadMaster == null){
                        logger.error(methodName + "unable to insert read master Kw ");
                        throw new Exception("unable to insert read master Kw");
                    }
                    logger.info(methodName + "inserted read master");
                }
            }
        }
        return insertedTariffDetail1;
    }



    /**
     * this method will return the custom bean tariff, with all latest info(Tariff detail, X-ray detail,seasonal info)
     *  param consumerNo
     * return
     * throws Exception
     */
    public CustomTariffDetail getLatestCustomTariffByConsumerNo(String consumerNo) throws Exception {
        String methodName = "getLatestCustomTariffByConsumerNo() : ";
        logger.info(methodName + "called");
        TariffDetailInterface tariffDetail = null;
        TariffChangeDetailInterface tariffChangeDetail = null;
        LoadDetailInterface loadDetail = null;
        SeasonalConnectionInformationInterface seasonalConnectionInformation = null;
        ConsumerConnectionInformationInterface consumerConnectionInformation = null;
        if (consumerNo == null) {
            logger.error(methodName + "Given Consumer No is Null" +consumerNo);
            throw new Exception("Given Consumer No is Null");
        }
        consumerNo = consumerNo.trim();
        logger.info(methodName + "Fetching Tariff Detail for Consumer no: " + consumerNo);
        tariffDetail = tariffDetailDAO.getTopByConsumerNoOrderByIdDesc(consumerNo);
        if (tariffDetail == null) {
            logger.error(methodName + "No tariff details found for Consumer no:" + consumerNo);
            throw new Exception("No tariff details found for Consumer");
        }
        CustomTariffDetail customTariffDetail  = new CustomTariffDetail();
        customTariffDetail.setId(tariffDetail.getId());
        customTariffDetail.setConsumerNo(tariffDetail.getConsumerNo());
        customTariffDetail.setTariffCode(tariffDetail.getTariffCode());
        customTariffDetail.setSubcategoryCode(tariffDetail.getSubcategoryCode());
        customTariffDetail.setEffectiveStartDate(tariffDetail.getEffectiveStartDate());
        customTariffDetail.setEffectiveEndDate(tariffDetail.getEffectiveEndDate());
        customTariffDetail.setBillMonth(tariffDetail.getBillMonth());

        consumerConnectionInformation = consumerConnectionInformationService.getByConsumerNo(consumerNo);
        if (consumerConnectionInformation == null) {
            logger.error(methodName + "Consumer connection information not found for consumer no :" + consumerNo);
            throw new Exception("Consumer connection information not found for consumer ");
        }
        tariffChangeDetail = tariffChangeDetailService.getByTariffDetailId(tariffDetail.getId());
        List<XrayConnectionInformationInterface> xrayConnectionInformationInterfaces = null;
        XrayConnectionInformationInterface xrayConnectionInformation = null;
        if (tariffChangeDetail == null) {
            logger.info(methodName + "Tariff not changed previously");
            if(consumerConnectionInformation.getIsXray()) {
                logger.info(methodName + "consumer is of Xray type,fetching details");
                xrayConnectionInformationInterfaces = xrayConnectionInformationService.getByConsumerNoAndStatus(consumerNo,XrayConnectionInformationInterface.STATUS_ACTIVE);
                if (xrayConnectionInformationInterfaces == null) {
                    logger.error(methodName + "some error in fetching Xray connection information");
                    throw new Exception("some error in fetching Xray connection information");
                }
                logger.info(methodName+ "fetched Xray details");
                xrayConnectionInformation = xrayConnectionInformationInterfaces.get(0);
                customTariffDetail.setXrayConnectionInformationInterface(xrayConnectionInformation);
            }
        }else {
            customTariffDetail.setTariffChangeDetailInterface(tariffChangeDetail);
            if(tariffChangeDetail.getIsXray()) {
                logger.info(methodName + "consumer is of Xray type,fetching details");
                xrayConnectionInformationInterfaces = xrayConnectionInformationService.getByConsumerNoAndStatus(consumerNo,XrayConnectionInformationInterface.STATUS_ACTIVE);
                if (xrayConnectionInformationInterfaces == null) {
                    logger.error(methodName + "some error in fetching Xray connection information");
                    throw new Exception("some error in fetching Xray connection information");
                }
                logger.info(methodName + "fetched Xray details"+xrayConnectionInformationInterfaces);
                xrayConnectionInformation = xrayConnectionInformationInterfaces.get(0);
                customTariffDetail.setXrayConnectionInformationInterface(xrayConnectionInformation);
            }
        }
        logger.info(methodName + "Fetching load Details for Consumer No : " + consumerNo);
        loadDetail = loadDetailService.getLatestLoadDetail(consumerNo);
        if (loadDetail == null) {
            logger.error(methodName + "Load Details not found for given consumer no :" + consumerNo);
            throw new Exception("Load Details not found for given consumer no");
        }
        logger.info(methodName + "Fetched load details for consumer no :" + consumerNo);
        customTariffDetail.setLoadDetailInterface(loadDetail);
        boolean isSeasonal = consumerConnectionInformation.getIsSeasonal();
        logger.info(methodName + "Checking Seasonal Flag for consumer :" + consumerNo);
        // checking isSeasonal flag in existing consumer connection information tariff detail.
        if (tariffChangeDetail== null && isSeasonal) {
            logger.info(methodName + "Fetching Seasonal Connection information for Consumer No:" + consumerNo);
            seasonalConnectionInformation = seasonalConnectionInformationService.getByConsumerNo(consumerNo);
            if (seasonalConnectionInformation == null) {
                logger.error(methodName + "Seasonal Connection Information not found for Given Consumer no: " + consumerNo);
                throw new Exception("Seasonal Connection Information not found");
            }
            logger.info(methodName + "Fetched Seasonal Connection Information for Consumer No " + consumerNo);
            customTariffDetail.setSeasonalConnectionInformationInterface(seasonalConnectionInformation);
        }else {
            logger.info(methodName + "seasonal flag is 'N' in consumer connection information");
            if(tariffChangeDetail != null){
                logger.info(methodName +" Tariff change details exist for consumer: "+consumerNo);
                // checking isSeasonal flag in existing consumer tariff change detail
                boolean isSeasonalTariffChangeFlag = tariffChangeDetail.getIsSeasonal();
                if(isSeasonalTariffChangeFlag){
                    logger.info(methodName + "Fetching Seasonal Connection information for Consumer No:" + consumerNo);
                    seasonalConnectionInformation = seasonalConnectionInformationService.getByConsumerNo(consumerNo);
                    if (seasonalConnectionInformation == null) {
                        logger.error(methodName + "Seasonal Connection Information not found for Given Consumer no: " + consumerNo);
                        throw new Exception("Seasonal Connection Information not found");
                    }
                    logger.info(methodName + "Fetched Seasonal Connection Information for Consumer No " + consumerNo);
                    customTariffDetail.setSeasonalConnectionInformationInterface(seasonalConnectionInformation);
                }
            }
        }
        return customTariffDetail;
    }


    public List<TariffDetailInterface> getByConsumerNoAndEffectiveStartDateAndEffectiveEndDate(String consumerNo, Date startDate, Date  endDate){
        String methodName = "getByConsumerNoAndEffectiveStartDateAndEffectiveEndDate() : ";
        List<TariffDetailInterface> tariffDetails = null;
        logger.info(methodName + "called ");
        if(consumerNo != null && startDate != null && endDate != null) {
            tariffDetails = tariffDetailDAO.getByConsumerNoAndEffectiveStartDateAndEffectiveEndDate(consumerNo,startDate,endDate);
        }
        return tariffDetails;
    }

    public TariffDetailInterface getLatestTariffByConsumerNo(String consumerNo){
        String methodName = "getLatestTariffByConsumerNo() : ";
        TariffDetailInterface tariffDetailInterface = null;
        logger.info(methodName + "called ");
        if(consumerNo != null) {
            tariffDetailInterface = tariffDetailDAO.getTopByConsumerNoOrderByIdDesc(consumerNo);
        }
        return tariffDetailInterface;
    }

    public TariffDetailInterface insert (TariffDetailInterface tariffDetail) {
        String methodName = "insert() : ";
        TariffDetailInterface insertedTariffDetail1 = null;
        logger.info(methodName + "called ");
        if(tariffDetail != null) {
            setAuditDetails(tariffDetail);
            insertedTariffDetail1 = tariffDetailDAO.add(tariffDetail);
        }
        return insertedTariffDetail1;
    }

    public TariffDetailInterface update (TariffDetailInterface tariffDetail) {
        String methodName = "update() : ";
        TariffDetailInterface updatedTariffDetail1 = null;
        logger.info(methodName + "called ");
        if(tariffDetail != null) {
            setUpdatedDetails(tariffDetail);
            updatedTariffDetail1 = tariffDetailDAO.update(tariffDetail);
        }
        return updatedTariffDetail1;
    }

    private void setAuditDetails(TariffDetailInterface tariffDetail){
        String methodName = "setAuditDetails()  :";
        if(tariffDetail != null) {
            Date date = GlobalResources.getCurrentDate();
            String user = GlobalResources.getLoggedInUser();
            tariffDetail.setUpdatedOn(date);
            tariffDetail.setUpdatedBy(user);
            tariffDetail.setCreatedOn(date);
            tariffDetail.setCreatedBy(user);
        }
        else {
            logger.error(methodName + "given input is null");
        }
    }
    private  void setUpdatedDetails(TariffDetailInterface tariffDetail){
        String methodName = "setUpdatedDetails()  :";
        if(tariffDetail != null) {
            tariffDetail.setUpdatedOn(GlobalResources.getCurrentDate());
            tariffDetail.setUpdatedBy(GlobalResources.getLoggedInUser());
        } else {
            logger.error(methodName + "given input is null");
        }
    }
}
