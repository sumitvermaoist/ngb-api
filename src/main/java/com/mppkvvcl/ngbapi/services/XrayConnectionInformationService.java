package com.mppkvvcl.ngbapi.services;

import com.mppkvvcl.ngbapi.utility.GlobalResources;
import com.mppkvvcl.ngbdao.daos.XrayConnectionInformationDAO;
import com.mppkvvcl.ngbinterface.interfaces.XrayConnectionInformationInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by SUMIT on 15-06-2017.
 */
@Service
public class XrayConnectionInformationService {
    /**
     * Asking Spring to inject XrayConnectionInformationRepository dependency in the variable
     * so that we get various handles for performing CRUD operation on xray connection information  table
     * at the backend Database
     */
    @Autowired
    private XrayConnectionInformationDAO xrayConnectionInformationDAO;

    /**
     * getting whole application logger object for performing logs. This logger will only be
     * used in this class methods only.
     */
    private static final Logger logger = GlobalResources.getLogger(XrayConnectionInformationService.class);

    /**
     * This insert method takes xrayConnectionInformation object and insert it into the XrayConnectionInformation table in the backend database.<br>
     * Return insertedXrayConnectionInformation object if successful else return null.<br><br>
     * param xrayConnectionInformation<br><br>
     * return insertedXrayConnectionInformation<br>
     */
    public XrayConnectionInformationInterface insert(XrayConnectionInformationInterface xrayConnectionInformation){
        String methodName = " insert : ";
        XrayConnectionInformationInterface insertedXrayConnectionInformation = null;
        logger.info(methodName + "Started insertion for XrayConnectionInformation ");
        if(xrayConnectionInformation != null){
            logger.info(methodName + " Calling XrayConnectionInformationRepository for inserting XrayConnectionInformation ");
            setAuditDetails(xrayConnectionInformation);
            insertedXrayConnectionInformation = xrayConnectionInformationDAO.add(xrayConnectionInformation);
            if (insertedXrayConnectionInformation != null){
                logger.info(methodName + "Successfully inserted one row for XrayConnectionInformation");
            }else {
                logger.error(methodName + "Unable to insert into XrayConnectionInformation. XrayConnectionInformationReppository returning null");
            }
        }else{
            logger.error(methodName + " Received XrayConnectionInformation object is null ");
        }
        return insertedXrayConnectionInformation;
    }

    public List<XrayConnectionInformationInterface> getByConsumerNoAndStatus(String consumerNo,String status){
        String methodName = "getLatestTariffByConsumerNo() :";
        List<XrayConnectionInformationInterface> xrayConnectionInformation = null;
        logger.info(methodName + "called ");
        if(consumerNo != null && status != null) {
            xrayConnectionInformation = xrayConnectionInformationDAO.getByConsumerNoAndStatus(consumerNo, status);
        }
        return xrayConnectionInformation;
    }

    private void setAuditDetails(XrayConnectionInformationInterface xrayConnectionInformation){
        String methodName = "setAuditDetails() : ";
        if(xrayConnectionInformation != null) {
            Date date = GlobalResources.getCurrentDate();
            String user = GlobalResources.getLoggedInUser();
            xrayConnectionInformation.setUpdatedOn(date);
            xrayConnectionInformation.setUpdatedBy(user);
            xrayConnectionInformation.setCreatedOn(date);
            xrayConnectionInformation.setCreatedBy(user);
        }
        else {
            logger.error(methodName + "given input is null");
        }
    }

    public XrayConnectionInformationInterface update (XrayConnectionInformationInterface xrayConnectionInformationInterface) {
        String methodName = "update() : ";
        logger.info(methodName + "called ");
        XrayConnectionInformationInterface updatedXrayConnectionInformationInterface = null;
        if(xrayConnectionInformationInterface != null) {
            setUpdatedDetails(xrayConnectionInformationInterface);
            updatedXrayConnectionInformationInterface = xrayConnectionInformationDAO.update(xrayConnectionInformationInterface);
        }
        return updatedXrayConnectionInformationInterface;
    }

    private  void setUpdatedDetails(XrayConnectionInformationInterface xrayConnectionInformationInterface){
        String methodName = "setUpdatedDetails()  : ";
        logger.info(methodName + "called ");
        if(xrayConnectionInformationInterface != null) {
            xrayConnectionInformationInterface.setUpdatedOn(GlobalResources.getCurrentDate());
            xrayConnectionInformationInterface.setUpdatedBy(GlobalResources.getLoggedInUser());
        }
    }
}
