package com.mppkvvcl.ngbapi.utility;

import org.slf4j.Logger;

import javax.persistence.Column;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Developed by Nitish on 18112017.
 * Below class provides various utility methods for helping
 * NGB-API development.
 * Understand the code & concept before changing anything. Some of the
 * functions in this class uses Java's reflection feature. Understand it
 * before changing anything.
 */
public class NGBAPIUtility {

    private static final Logger logger = GlobalResources.getLogger(NGBAPIUtility.class);

    /**
     * This function gives Java's Persistence Column Annotation class
     * if used on passed field.
     * @param field
     * @return
     */
    public static Column getColumnAnnotationByNameValue(Field field){
        final String methodName = "getColumnAnnotation() : ";
        logger.info(methodName + "called");
        Column columnAnnotation = null;
        if(field != null){
            Annotation[] annotations = field.getDeclaredAnnotations();
            for(Annotation annotation : annotations){
                if(annotation != null && annotation instanceof Column){
                    columnAnnotation = (Column)annotation;
                }
            }
        }
        return columnAnnotation;
    }

    /**
     * This method returns Field class object for provided fieldName
     * in provided object
     * @param fieldName
     * @param object
     * @return
     */
    public static Field getFieldByName(String fieldName, Object object){
        final String methodName = "getFieldByName() : ";
        logger.info(methodName + "called");
        Field field = null;
        if(fieldName != null && object != null){
            Field fields[] = object.getClass().getDeclaredFields();
            for (Field f : fields) {
                if(f.getName().equals(fieldName)){
                    field = f;
                    break;
                }
            }
        }
        return field;
    }

    /**
     * Function to get column name of backend database table
     * as per provided params
     * @param fieldName
     * @param object
     * @return
     */
    public static String getColumnNameByFieldName(String fieldName,Object object){
        final String methodName = "getColumnNameByFieldName() : ";
        logger.info(methodName + "called");
        String columnName = null;
        if(fieldName != null && object != null){
            Field field = getFieldByName(fieldName,object);
            Column column = getColumnAnnotationByNameValue(field);
            if(column != null){
                columnName = column.name();
            }
        }
        return columnName;
    }

    /**
     * This method returns Method class object for provided field Name
     * in provided object if there is a getter for provided field Name
     * @param field
     * @param object
     * @return
     */
    public static Method getGetterForField(String field, Object object){
        final String methodName = "getGetterForField() : ";
        logger.info(methodName + "called");
        Method method = null;
        if(field != null && object != null){
            Method methods[] = object.getClass().getMethods();
            for (Method m : methods) {
                if(isGetter(m) && m.getName().toLowerCase().endsWith(field.toLowerCase())){
                    method = m;
                    break;
                }
            }
        }
        return method;
    }

    /**
     * This method returns Method class object for provided field Name
     * in provided object if there is a setter for provided field Name
     * @param field
     * @param object
     * @return
     */
    public static Method getSetterForField(String field,Object object){
        final String methodName = "getSetterForField() : ";
        logger.info(methodName + "called");
        Method method = null;
        if(field != null && object != null){
            Method methods[] = object.getClass().getMethods();
            for (Method m : methods) {
                if(isSetter(m) && m.getName().toLowerCase().endsWith(field.toLowerCase())){
                    method = m;
                    break;
                }
            }
        }
        return method;
    }

    /**
     * This method checks whether the passed Method class object
     * is a getter method or not. returns True if method name starts with get,
     * returns a Type and takes 0 parameter
     * @param method
     * @return
     */
    public static boolean isGetter(Method method){
        final String methodName = "isGetter() : ";
        //logger.info(methodName + "called");
        if(!method.getName().startsWith("get"))      return false;
        if(method.getParameterTypes().length != 0)   return false;
        if(void.class.equals(method.getReturnType())) return false;
        return true;
    }

    /**
     * This method checks whether the passed Method class object
     * is a setter method or not. returns True if method name starts with set,
     * returns void and takes 1 parameter
     * @param method
     * @return
     */
    public static boolean isSetter(Method method){
        final String methodName = "isSetter() : ";
        //logger.info(methodName + "called");
        if(!method.getName().startsWith("set")) return false;
        if(method.getParameterTypes().length != 1) return false;
        return true;
    }

    public static final String SORT_ORDER_ASCENDING = "ASC";

    public static final String SORT_ORDER_DESCENDING = "DESC";

}
